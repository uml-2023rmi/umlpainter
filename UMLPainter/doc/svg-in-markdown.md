SVG в файле формата MarkDown
----------------------------

Файл в формате _Scalable Vector Graphics_ (расширение __*.svg__)  
может быть использован в файле _MarkDown_ (расширение __*.md__).  
Пример приводится ниже:  
 
![Примитивы рисования](uml-primitives.svg)  

Встраивание делает следующий код:

```
![Примитивы рисования](uml-primitives.svg)  
```