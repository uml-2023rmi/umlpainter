# Использование и реализация интерфейсов компонентами

Для каждой компоненты (заданного jar-файла) показать на диаграмме 
какие внешние интерфейсы реализуются классами в компоненте, 
и какие внешние интерфейсы используются компонентами.

Пример первой диаграммы (Саада):

![pic1](img/component-interfaces-1.png)

Пример второй диаграммы (Мелоян):

![pic2](img/component-interfaces-2.png)




