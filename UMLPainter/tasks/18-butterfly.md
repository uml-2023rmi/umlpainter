# Диаграмма-бабочка для метрик пакета

![package-metrics](img/package-metrics.png)

![package-metrics](img/package-metrics-screen.png)

+ **PP (Number of Provider Packages).**  
Number of package providers of a package. 

+ **CP (Number of Client Packages).**  
Number of packages that depend on a package. 

+ **RTP (Number of Class References To Other Packages).**  
Number of class references from classes in the measured package to classes in other packages.  

+ **RRTP (RelativeNumber of Class References To Other Packages).**  
RTP divided by the sum of RTP and the number of internal class references.

+ **RFP (Number of Class References From Other Packages).**  
Number of class references from classes belonging to other packages to classes belonging to the analyzed package. 

+ **RRFP (Relative Number of Class References From Other Packages).**  
RFP divided by the sum of RFP and the number of internal class references.

+ **PIIR (Number of Internal Inheritance Relationships).**  
Number of inheritance relationships existing between classes in the same package.

+ **RPII (Relative Number of Internal Inheritance Relationships).**  
PIIR divided by the sum of PIIR and EIP. 

+ **EIC (Number of External Inheritance as Client).**  
Number of inheritance relationships in which superclasses are in external packages. 

+ **EIP (Number of External Inheritance as Provider).**  
Number of inheritance relationships where the superclass is in the package being analyzed and
the subclass is in another package. 

+ **REIP (Relative Number of External Inheritance as Provider).**  
EIP divided by the sum of PIIR and EIP. 

+ **ASC (Number of Ancestor State as Client).**  
Number of accesses to instance variables defined in a superclass that belongs to another package.

+ **RASC (Relative Number of Ancestor State as Client).**  
ASC divided by the sum of ASC and ASCI.  
Where ASCI, Number of Ancestor State Client Internal to the Package 
is the ancestor state class dependencies internal to the package.  
We consider only dependencies from a class that is inside the package 
to other classes of the same package.

+ **ASP (Number of Ancestor State as Provider).**  
Number of times that instance variables of classes belonging to the analyzed package 
are accessed by classes belonging to other packages. 

+ **RASP (Relative Number of Ancestor State as Provider).  
ASP divided by the sum of ASP and the number of gives ancestor state dependencies
between classes when both classes belong to the package.

+ **CC (Number of Class Clients).**   
Number of external class dependencies that are clients of a package.  
Sum over the number of the class dependencies (ancestor state, class reference and inheritance) 
that refer to a package. 

+ **NCP (Number of Classes in a Package).**  
Number of classes in the package. NCP(P1)=2.
 