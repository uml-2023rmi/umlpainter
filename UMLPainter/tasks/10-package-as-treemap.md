# Диаграмма структуры пакета как карты-дерева

![pic1](img/package-as-tree-map.png)  
[link](img/package-as-tree-map.png)  

Представить структуру пакета в виде карты-дерева.

[NDependent](https://www.ndepend.com/docs/treemap-visualization-of-code-metrics)
  пример.
  