# Диаграмма зависимости компонент

![pic1](img/componentDependencies.svg)  
[link](img/componentDependencies.svg)  

Диаграмма *зависимости компонентов* должна быть построена по UML-модели 
для компоненты __Component.jar__
(малиновая в центре).  

Слева показаны компоненты _использующие_ компоненту __Component.jar__.
Они показаны синим фоном, внутри них пакеты.
Классы, расположенные в этих пакетах, _используют_ классы 
расположенные в компоненте __Component.jar__.

Справа показаны компоненты _используемые_ компонентой __Component.jar__.
Они зеленым фоном, внутри них пакеты.
Классы, расположенные в этих пакетах, _используются_ классами 
расположенными в компоненте __Component.jar__.

![pic1](img/artifact.png)  
[link](img/artifact.png)  

```java
    Set<Artifact> components = model.getOwnedElements()
			.stream()
			.filter(e -> e instanceof Artifact)
			.map( e -> (Artifact)e)
			.collect(Collectors.toSet());

    Set<Package> nestedPackages = component.getManifestations()
            .stream()
            .map(Manifestation::getUtilizedElement)
            .map(Element::getNearestPackage)
            .collect(Collectors.toSet());
```

  
  