# Диаграмма вложенности пакетов с помощью радиальной планировки

![pic1](img/packages-nesting-with-radial-layout.png)

```java
package samples;

import uml.diagram.svg.SVGDiagram;
import uml.diagram.svg.edges.AssociationEdge;
import uml.diagram.svg.nodes.*;

import java.lang.Math;
import java.io.File;

public class RadialLayout {
    private static final double CX = 200;
    private static final double CY = 200;

    public static void showRadialLayout(String diagramDir, String fileName) {
        new File(diagramDir).mkdirs();

        SVGDiagram diagram = new SVGDiagram("Packages nesting with radial layout");

        double xA = CX;
        double yA = CY;
        ClassNode center;
        center = new ClassNode(diagram, "org", xA, yA);
        center.setColor("yellow");
        center.setBackground("yellow");

        ClassNode level1;
        double offset1 = 50;
        double xB[] = {xA + offset1, xA - offset1};
        double yB[] = {yA + offset1, yA - offset1};
        for (int i = 0; i < xB.length; i++) {
            for (int j = 0; j < yB.length; j++) {
                level1 = new ClassNode(diagram, "java", xB[i], yB[j]);
                level1.setColor("yellow");
                level1.setBackground("maroon");
                new AssociationEdge(diagram, center, level1);
            }
        }

        double offset2 = 52;
        double r2 = 180;
        double xC[] = {-10, -10 - offset2, -10 - 2 * offset2, -10 - 3 * offset2,
                10, 10 + offset2, 10 + 2 * offset2, 10 + 3 * offset2};
        ClassNode level2;
        for (int i =0;i<xC.length;i++) {
            double yC1 = Math.sqrt(Math.pow(r2, 2) - Math.pow(xC[i], 2));
            double yC2 = -yC1;
            level2 = new ClassNode(diagram, "swing", xA + xC[i], yA + yC1);
            level2.setColor("yellow");
            level2.setBackground("yellow");

            level2 = new ClassNode(diagram, "swing", xA + xC[i], yA + yC2);
            level2.setColor("yellow");
            level2.setBackground("yellow");

        }

        diagram.save(diagramDir, fileName);

    }

    public static void main(String[] args) {
        RadialLayout.showRadialLayout("./test", "1.svg");
    }
}
```


