<a name="start" />

# Построение UML-диаграмм в формате SVG

## 

1. [Элементы модели на UML-диаграммах статической структуры и реализации](#diagram-nodes)
  + [Безымянные узлы-пиктограммы с всплывающей подсказкой](#icon-nodes-tiny)
  + [Узлы-пиктограммы для классификаторов и пакетов](#icon-nodes)
  + [Структурированные узлы для классификаторов и пакетов](#structured-nodes)
    + [Структурированные узлы в сжатом состоянии](#structured-nodes-collapsed)
    + [Структурированные узлы в раскрытом состоянии](#structured-nodes-expanded)
2. [Отношения на диаграммах UML](#relations)
  + [Якоря ребер для прикрепления к узлам](#anchors)
  + [Маршрутизаторы ребер](#routers)
    + [Маршрутизаторы для ребер из двух сегментов](#routers2)
    + [Маршрутизаторы для ребер из трех сегментов](#routers3)
3. [Примитивы для рисования диаграмм UML](#uml-primitives)
4. [Графические элементы](#uml-graphical)

В этом учебнике описывается инструмент для построения и генерации UML-диаграмм
в формате *Scalable Vector Graphics* (***SVG***).
 
<a name="diagram-nodes" />

## 1. Элементы модели на UML-диаграммах статической структуры и реализации
[->оглавление](#start)  

На *диаграммах статической структуры* показываются 
 *пакеты* и *классификаторы* (классы, интерфейсы и перечисления).  

На *диаграммах реализации* показываются *компоненты* (файлы)
и содержащиеся в них *пакеты* и *классификаторы*.  

<a name="icon-nodes-tiny" />

Для компактного представления на диаграммах пакетов, классификаторов и компонент
могут быть использованы узлы-крошки. 
Тест на этих узлах-пиктограммах показывается только как всплывающие подсказки.
Пример на рисунке ниже.

![pic1](TinyNodes.svg)  
[link](TinyNodes.svg)  

Программа рисующая эту диаграмму ниже:

```java
private static void genTinyNodes() {
    SVGDiagram diagram = new SVGDiagram("Безымянные пиктограммы с всплывающей подсказкой");
    Point p = new Point(5, 5);

    new PackageTinyNode(diagram, "Package tiny", p);
    p.x += 20;

    new InterfaceTinyNode(diagram, "Interface tiny",p);
    p.x += 20;

    new ClassTinyNode(diagram, "Class tiny", p);
    p.x += 20;

    new EnumerationTinyNode(diagram, "Package tiny", p);
    p.x += 20;

    new ComponentTinyNode(diagram, "Component tiny", p);

    diagram.save(dir, "TinyNodes.svg");
  }
```

### Безымянные узлы-пиктограммы с всплывающей подсказкой](#icon-nodes-tiny)
[->оглавление](#start)  

<a name="icon-nodes" />

### Узлы-пиктограммы для классификаторов и пакетов
[->оглавление](#start)  

![pic1](IconNodes.svg)  
[link](IconNodes.svg)  

```java
SVGDiagram diagram = new SVGDiagram("Узлы - пиктограммы");

Point p = new Point(5, 5);

SVGIconNode icon = new ClassIconNode(diagram, "Class", p);
p = icon.bounds.getRightBottom();

icon = new InterfaceIconNode(diagram, "Interface", p);
p = icon.bounds.getRightBottom();

icon = new EnumerationIconNode(diagram, "Enumeration", p);
p = icon.bounds.getRightBottom();

icon = new ComponentIconNode(diagram, "Component (swt.jar)", p);
p = icon.bounds.getRightBottom();

icon = new PackageIconNode(diagram, "Package", p);

diagram.save(dir, "IconNodes.svg");
```

<a name="structured-nodes" />

### Структурированные узлы для классификаторов и пакетов
[->оглавление](#start)  

Структурированные узлы состоят из заголовка (первая секция)
и нескольких секций расположенных ниже.  

Структурированные узлы UML-диаграммы могут находиться 
в сжатом и раскрытом состоянии.

<a name="structured-nodes-collapsed" />

### Структурированные узлы в сжатом состоянии
[->оглавление](#start)  

На рисунке ниже показаны узлы в сжатом состоянии. 
В этом состоянии показывается только заголовок узла. 

![pic1](StructuredNodesCollapsed.svg)  
[link](StructuredNodesCollapsed.svg)  

Пример кода для этой диаграммы показан ниже:

```java
SVGDiagram diagram = new SVGDiagram("Узлы - пиктограммы");

Point p = new Point(5, 5);

ClassifierNode node = new ClassNode(diagram, "Class", p);
p = node.bounds.getRightBottom();
p.x += 10;

node = new InterfaceNode(diagram, "Interface", p);
p = node.bounds.getRightBottom();
p.x += 10;

node = new EnumerationNode(diagram, "Enumeration", p);
p = node.bounds.getRightBottom();
p.x += 10;

node = new ComponentNode(diagram, "swt.jar", p);
p = node.bounds.getRightBottom();
p.x += 10;

new PackageNode(diagram, "Package", p);

diagram.save(dir, "StructuredNodesCollapsed.svg");
```

<a name="structured-nodes-expanded" />

### Структурированные узлы в раскрытом состоянии
[->оглавление](#start)  

На рисунке ниже показаны узлы в раскрытом состоянии. 
В этом состоянии показывается содержимое всех секций узла.

![pic1](StructuredNodesExpanded.svg)  
[link](StructuredNodesExpanded.svg)  

Пример кода для этой диаграммы показан ниже:

```java
    SVGDiagram diagram = new SVGDiagram("Раскрытые структурированные узлы");

    ClassifierNode node;

    Point p = new Point(5, 5);

    node = new EnumerationNode(diagram, "Color", p);
    node.createAttributeNode("white");
    node.createAttributeNode("black");
    node.createAttributeNode("red");
    node.createAttributeNode("green");
    node.createAttributeNode("blue");
    p = node.bounds.getBottomCenter();
    p.y += 10;

    node = new ClassNode(diagram, "Point", p);
    node.createAttributeNode("- x: Double");
    node.createAttributeNode("- y: Double");
    node.createOperationNode("+ distance(p: Point): double");
    p = node.bounds.getBottomCenter();
    p.y += 10;

    node = new InterfaceNode(diagram, "Layouter", p);
    node.createOperationNode("+ layout()");
    node.createClassIconNode("NoneLayouter");
    p = node.bounds.getBottomCenter();
    p.y += 10;

    ComponentNode cNode = new ComponentNode(diagram, "swt.jar", p);
    cNode.createPackageIconNode("widgets");
    cNode.createPackageIconNode("painting");
    cNode.createPackageIconNode("layout");
    p = cNode.bounds.getBottomCenter();
    p.y += 10;

    PackageNode pNode = new PackageNode(diagram, "diagram", p);
    pNode.createInterfaceIconNode("Shape");
    pNode.createClassIconNode("Circle");
    pNode.createClassIconNode("Rectangle");
    pNode.createPackageIconNode("nodes");
    pNode.createPackageIconNode("edges");
    pNode.createPackageIconNode("anchors");
    pNode.createPackageIconNode("routers");

    diagram.save(dir, "StructuredNodesExpanded.svg");
```



<a name="relations" />

## 2. Отношения между элементами UML-диаграмм
[->оглавление](#start)  

Отношения:  

+ *Наследования* между классами и интерфейсами
+ *Реализации* классами интерфейсов
+ *Ассоциации* между классификаторам 
(классами, интерфейсами, перечислениями)
+ *Зависимости* между элементами UML-модели

<a name="anchors" />

### Якоря на ребрах графа для прикрепления к узлам графа
[->оглавление](#start)  

 **Якорь (Anchor)** определяет место прикрепления ребра графа 
к узлу графа.  

+ Якорь **AnchorTop** прикрепляет ребро *к верхней стороне* узла.
+ Якорь **AnchorBottom** прикрепляет ребро *к нижней стороне* узла.
+ Якорь **AnchorLeft** прикрепляет ребро *к левой стороне* узла.
+ Якорь **AnchorRight** прикрепляет ребро *к правой стороне* узла.  
  
По умолчанию якорь прикрепляется к середине заданной стороны узла.  

+ Функция **setSourceAnchor()** определяет якорь для прикрепления 
к узлу *из которого выходит* ребро.
+ Функция **setTargetAnchor()** определяет якорь для прикрепления 
к узлу *в который входит* ребро.

![pic1](AnchorDefault.svg)  
[link](AnchorDefault.svg)  

Пример кода:  

```java
SVGDiagram diagram = new SVGDiagram("Умалчиваемые якоря ребер графа");

ClassNode grandFather = new ClassNode(diagram, "GrandFather", 10, 10);
ClassNode father = new ClassNode(diagram, "Father", 100, 100);
ClassNode son = new ClassNode(diagram, "Son", 250, 150);

SVGEdge edge2GrandFather = new InheritanceEdge(diagram, father, grandFather);
edge2GrandFather.setSourceAnchor(new AnchorTop());
edge2GrandFather.setTargetAnchor(new AnchorBottom());

SVGEdge edge2Father = new InheritanceEdge(diagram, son, father);
edge2Father.setSourceAnchor(new AnchorLeft());
edge2Father.setTargetAnchor(new AnchorRight());

diagram.save(dir, "AnchorDefault.svg");
```

<a name="routers" />

### Маршрутизаторы для ребер графа
[->оглавление](#start)  

 **Маршрутизаторы (Routers)** определяют направление сегментов 
(маршрут) ребра графа. 
Маршрутизаторы могут быть ребер из 2-х и 3-х сегментов.

<a name="routers2" />

#### Маршрутизаторы для ребер из двух сегментов
[->оглавление](#start)  

+ Маршрутизатор **RouterHV** создает ребро состоящее из двух сегментов.  
Первый сегмент *горизонтальный* (**H**), второй *вертикальный* (**V**). 
+ Маршрутизатор **RouterVH** создает ребро состоящее из двух сегментов.  
Первый сегмент *вертикальный* (**V**), второй *горизонтальный* (**H**).  
  
На приводимом ниже рисунке 
маршрут ребра из **Father** в **GrandFather** 
определяет маршрутизатор **RouterHV**.  
Маршрут ребра из **Son** в **Father** 
определяет маршрутизатор **RouterVH**.

![pic1](RouterHVandVH.svg)  
[link](RouterHVandVH.svg)  

Пример кода:  

```java
SVGDiagram diagram = new SVGDiagram("Маршрутизаторы RouterHV и RouterVH");

ClassNode grandFather = new ClassNode(diagram, "GrandFather", 10, 10);
ClassNode father = new ClassNode(diagram, "Father", 100, 100);
ClassNode son = new ClassNode(diagram, "Son", 250, 150);

SVGEdge edge2GrandFather = new InheritanceEdge(diagram, father, grandFather);
edge2GrandFather.setSourceAnchor(new AnchorLeft());
edge2GrandFather.setTargetAnchor(new AnchorBottom());
edge2GrandFather.setRouterHV();

SVGEdge edge2Father = new InheritanceEdge(diagram, son, father);
edge2Father.setSourceAnchor(new AnchorTop());
edge2Father.setTargetAnchor(new AnchorRight());
edge2Father.setRouterVH();

diagram.save(dir, "RouterHVandVH.svg");
```

<a name="routers3" />  

### Маршрутизаторы для ребер из трех сегментов
[->оглавление](#start)  

+ Маршрутизатор **RouterHVH** создает ребро состоящее из трех сегментов.  
Выходящий и входящий сегменты *горизонтальные* (**H**), 
средний сегмент *вертикальный* (**V**) и расположен на равном расстоянии
соединяемых узлов. 
+ Маршрутизатор **RouterVHV** создает ребро состоящее из двух сегментов.  
Выходящий и входящий сегменты *вертикальные* (**V**), 
средний сегмент *горизонтальный* (**H**) и расположен на равном расстоянии
соединяемых узлов.  
  
На приводимом ниже рисунке 
маршрут ребра из **Father** в **GrandFather** 
определяет маршрутизатор **RouterHVH**.  
Маршрут ребра из **Son** в **Father** 
определяет маршрутизатор **RouterVHV**.  

![pic1](RouterHVHandVHV.svg)  
[link](RouterHVHandVHV.svg)  

Пример кода:  

```java
SVGDiagram diagram = new SVGDiagram("Маршрутизаторы RouterHVH и RouterVHV");

ClassNode grandFather = new ClassNode(diagram, "GrandFather", 10, 10);
ClassNode father = new ClassNode(diagram, "Father", 100, 100);
ClassNode son = new ClassNode(diagram, "Son", 250, 150);

SVGEdge edge2GrandFather = new InheritanceEdge(diagram, father, grandFather);
edge2GrandFather.setSourceAnchor(new AnchorTop());
edge2GrandFather.setTargetAnchor(new AnchorBottom());
edge2GrandFather.setRouterHVH();

SVGEdge edge2Father = new InheritanceEdge(diagram, son, father);
edge2Father.setSourceAnchor(new AnchorLeft());
edge2Father.setTargetAnchor(new AnchorRight());
edge2Father.setRouterVHV();

diagram.save(dir, "RouterHVHandVHV.svg");
```

<a name="uml-primitives" />

## 3. Примитивы для рисования UML диаграмм
[->оглавление](#start)  

![pic1](uml-primitives.svg)  
[link](uml-primitives.svg)  

```java
public class UML2SVGDemo {
  private static final double DX = 30;
  private static final double DY = 30;

  private static final double X = DX;
  private static final double Y = DY;

  public static void showPrimitives(String diagramDir, String fileName) {
    new File(diagramDir).mkdirs();

    SVGDiagram diagram = new SVGDiagram("Примитивы для рисования UML диаграмм");

    double x = X;
    double y = Y;
    double columnWidth = 0;

    // ===================================
    // Узлы-пиктограммы на диаграммах UML.
    // ===================================
    SVGIconNode icon;

    // Класс - пиктограмма.
    icon = new ClassIconNode(diagram, "Class", x, y);
    icon.setX(x);
    icon.setY(y);
    columnWidth = max(columnWidth, icon.getWidth());
    y += (icon.getHeight() + DY);

    // Интерфейс - пиктограмма.
    icon = new InterfaceIconNode(diagram, "Interface", x, y);
    icon.setColor("Purple");
    icon.setBackground("Violet");
    icon.setColor("Purple");
    icon.setBackground("Violet");
    icon.setTextColor("Indigo");
    columnWidth = max(columnWidth, icon.getWidth());
    y += (icon.getHeight() + DY);

    // Перечисление - пиктограмма.
    icon = new EnumerationIconNode(diagram, "Enumeration", x, y);
    icon.setColor("maroon");
    icon.setBackground("yellow");
    icon.setTextColor("maroon");
    columnWidth = max(columnWidth, icon.getWidth());
    y += (icon.getHeight() + DY);

    // Пакет - пиктограмма.
    icon = new PackageIconNode(diagram, "Package", x, y);
    icon.setColor("green");
    icon.setBackground("SpringGreen");
    icon.setTextColor("green");
    columnWidth = max(columnWidth, icon.getWidth());
    y += (icon.getHeight() + DY);

    // Компонента - пиктограмма.
    icon = new ComponentIconNode(diagram, "swt.jar", x, y);
    icon.setColor("Blue");
    icon.setBackground("CornflowerBlue");
    icon.setTextColor("Blue");
    icon.setX(x);
    icon.setY(y);
    columnWidth = max(columnWidth, icon.getWidth());

    // =================================
    // Составные узлы на диаграммах UML.
    // =================================
    //
    // Новая колонка.
    // ==============
    x += (columnWidth + DX);
    y = Y;

    // ----- Составной пакет. -----
    PackageNode pn = new PackageNode(diagram, "eclipse.swt", x, y);

    // Интерфейс вложенный в пакет.
    InterfaceIconNode iMouse = pn.createInterfaceIconNode("MouseListener");
    iMouse.setBackground("Violet");
    iMouse.setColor("Purple");

    // Интерфейс вложенный в пакет.
    InterfaceIconNode iMouseListener = pn.createInterfaceIconNode("KeyboardListener");
    iMouseListener.setBackground("Violet");
    iMouseListener.setColor("Purple");

    // Класс вложенный в пакет.
    SVGIconNode in = pn.createClassIconNode("Event");
    in.setColor("blue");
    in.setTextColor("maroon");
    in.setItalic(true);
    y += (pn.getHeight() + DY);

    // Класс.
    ClassNode cn = new ClassNode(diagram, "PaintEvent", x, y);
    cn.header.setBackground("cyan");
    cn.header.setColor("blue");
    y += 50;

    // Интерфейс.
    InterfaceNode inn = new InterfaceNode(diagram, "PaintEvent", x, y);
    inn.iconNode.setBackground("Violet");
    inn.header.setColor("Indigo");
    inn.header.setBackground("Violet");
    inn.header.setColor("Indigo");
    y += 50;

    // Компонета.
    ComponentNode cmn = new ComponentNode(diagram, "ClassNode.jar", x, y);
    cmn.iconNode.setColor("Blue");
    cmn.iconNode.setTextColor("Blue");
    cmn.iconNode.setBackground("CornflowerBlue");
    cmn.header.setColor("DarkSlateBlue");
    cmn.header.setBackground("CornflowerBlue");

    // =========================================
    // Отношения на диаграммах UML (ребра графа).
    // =========================================
    //
    // Новая колонка.
    // ==============
    x += 200;
    y = Y;

    double L = 80;

    SVGEdge edge;

    // Отношение наследования.
    ClassifierNode subclass = new ClassNode(diagram, "Dog", x, y + L);
    ClassifierNode superclass = new ClassNode(diagram, "Animal", x, y);
    new InheritanceEdge(diagram, subclass, superclass);
    y += 150;

    // Отношение реализации.
    ClassNode implementor = new ClassNode(diagram, "Fish", x, y + L);
    InterfaceNode implemented = new InterfaceNode(diagram, "Swim", x, y);
    implemented.header.setColor("Purple");
    implemented.header.setBackground("Violet");
    implemented.iconNode.setColor("Purple");
    implemented.iconNode.setBackground("Violet");
    new ImplementationEdge(diagram, implementor, implemented);

    //
    // Новая колонка.
    // ==============
    x += 100;
    y = Y;

    // Отношение ассоциации.
    ClassNode owner = new ClassNode(diagram, "Tree", x, y + L);
    ClassNode owned = new ClassNode(diagram, "Leaf", x, y);
    new AssociationEdge(diagram, owner, owned);
    y += 150;

    // Отношение зависимости.
    SVGShape client = new PackageNode(diagram, "Chess", x, y);
    SVGShape supplier = new PackageNode(diagram, "Game", x, y + L);
    edge = new DependencyEdge(diagram, client, supplier);
    edge.setSourceAnchor(new AnchorBottom());
    edge.setTargetAnchor(new AnchorTop());

    diagram.save(diagramDir, fileName);
  }
```

<a name="uml-graphical" />

## 4. Графические элементы
[->оглавление](#start) 

Прямоугольник по умолчанию прозрачный.

![pic1](GraphicalElements.svg)  
[link](GraphicalElements.svg)  

```java
private static void genGraphicalElements(String dir) {
    SVGDiagram diagram = new SVGDiagram("Графические элементы");
    Point p = new Point(15, 15);

    Point pFigures = figuresDemo(diagram, p);
    p.x = pFigures.x;
    p.y = pFigures.y + 50;

    p = textDemo(diagram, p);
    p.x = pFigures.x;
    p.y += 50;

    drawAxis(diagram, p, 200);

    diagram.save(dir, "GraphicalElements.svg");
  }

  private static Point figuresDemo(SVGDiagram diagram, Point p) {
    Rectangle rectSolid = new SVGRectangle(diagram);
    rectSolid.setX(p.x);
    rectSolid.setY(p.y);
    rectSolid.setWidth(100);
    rectSolid.setHeight(100);
    rectSolid.setColor("navy");
    rectSolid.setBackground("SkyBlue");
    p = rectSolid.getBounds().getRightTop();
    p.x += 50;

    Rectangle rectGlass = new SVGRectangle(diagram);
    rectGlass.setX(p.x);
    rectGlass.setY(p.y);
    rectGlass.setWidth(100);
    rectGlass.setHeight(100);
    rectGlass.setColor("green");

    return rectSolid.getBounds().getLeftBottom();
  }

  private static Point textDemo(SVGDiagram diagram, Point p) {
    double lineLen = 100;
    p.x += 300;
    Line vertical = new SVGLine(diagram, p,
                                new Point(p.x, p.y + lineLen));
    vertical.setColor("Red");

    Text textLeft = new SVGText(diagram, "Text start (default)", p);
    textLeft.setColor("purple");

    p.y += 20;
    Text textMiddle = new SVGText(diagram, "setAlignment(AlignmentKind.center)", p);
    textMiddle.setAlignment(AlignmentKind.center);
    textMiddle.setColor("purple");

    p.y += 20;
    Text textRight = new SVGText(diagram, "setAlignment(AlignmentKind.end)", p);
    textRight.setAlignment(AlignmentKind.end);
    textRight.setColor("purple");

    p.y += textRight.getHeight() + 20;
    return p;
  }

  private static void drawAxis(SVGDiagram diagram, Point axisStart, double axisLen) {
    axisStart.y += axisLen;
    double k = 0.75;

    Point xAxisEnd = new Point(axisStart.x + axisLen, axisStart.y);
    Point yAxisEnd = new Point(axisStart.x, axisStart.y - axisLen);

    Line axisX = new SVGLine(diagram, axisStart, xAxisEnd);
    Line axisY = new SVGLine(diagram, axisStart, yAxisEnd);
    axisX.setColor("navy");
    axisY.setColor("navy");

    Line optimal = new SVGLine(diagram,
            new Point(axisStart.x + k * axisLen, axisStart.y),
            new Point(axisStart.x, axisStart.y - k * axisLen));
    optimal.setColor("red");
  }
```