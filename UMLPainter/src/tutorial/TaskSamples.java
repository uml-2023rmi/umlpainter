package tutorial;

import uml.dd.Point;
import uml.diagram.Painter;
import uml.diagram.Shape;
import uml.diagram.anchors.AnchorLeft;
import uml.diagram.anchors.AnchorRight;
import uml.diagram.svg.SVGDiagram;
import uml.diagram.svg.edges.DependencyEdge;
import uml.diagram.svg.nodes.ClassNode;
import uml.diagram.svg.nodes.ComponentNode;
import uml.diagram.svg.nodes.PackageNode;

import java.io.File;
import java.util.*;

public class TaskSamples {
    static Painter usedPainter = new Painter().pen("Green").brush("LightGreen").pencil("Green");
    static Painter usersPainter = new Painter().pen("Blue").brush("SkyBlue").pencil("Blue");
    static Painter centerPainter = new Painter().pen("Brown").brush("Plum").pencil("Blue");

        private static SVGDiagram diagram;

    public static void generate(String dir) {
        new File(dir).mkdirs();

        classDependencies(dir);
        componentDependencies(dir);
    }

    private static void classDependencies(String dir) {
        Map<String, List<String>> users = new HashMap<>();
        users.put("UserPackage1", Arrays.asList("UserClass1", "UserClass2", "UserClass3"));
        users.put("UserPackage2", Arrays.asList("UserClass1", "UserClass2"));

        Map<String, List<String>> used = new HashMap<>();
        used.put("UsedPackage1", Arrays.asList("UsedClass1", "UsedClass2"));
        used.put("UsedPackage2", Collections.singletonList("UsedClass3"));
        used.put("UsedPackage3", Arrays.asList("UsedClass4", "UsedClass5", "UsedClass6"));

        diagram = new SVGDiagram("Диаграмма зависимости класса");

        Point p = new Point(300, 5);
        ClassNode theNode = new ClassNode(diagram, "TheClass", p);
        centerPainter.paint(theNode);

        p.x = 5;

        users.keySet().stream()
                .map(name -> new PackageNode(diagram, name, p))
                .peek(usersPainter::paint)
                .peek(node -> nestClasses(node, users))
                .peek(node -> dependency(node, theNode))
                .reduce(TaskSamples::locateVertical);

        p.x = 600;

        used.keySet().stream()
                .map(name -> new PackageNode(diagram, name, p))
                .peek(usedPainter::paint)
                .peek(node -> nestClasses(node, used))
                .peek(node -> dependency(theNode, node))
                .reduce(TaskSamples::locateVertical);

        diagram.save(dir, "classDependencies.svg");
    }

    private static void componentDependencies(String dir) {
        Map<String, List<String>> users = new HashMap<>();
        users.put("UserComponent1.jar", Arrays.asList("UserPackage1", "UserPackage2", "UserPackage3"));
        users.put("UserComponent2.jar", Arrays.asList("UserPackage1", "UserPackage2"));

        Map<String, List<String>> used = new HashMap<>();
        used.put("UsedComponent1.jar", Arrays.asList("UsedPackage1", "UsedPackage2"));
        used.put("UsedComponent2.jar", Collections.singletonList("UsedPackage3"));
        used.put("UsedComponent3.jar", Arrays.asList("UsedPackage4", "UsedPackage5", "UsedPackage6"));

        diagram = new SVGDiagram("Диаграмма зависимости компонент");

        Point p = new Point(300, 5);
        ComponentNode theNode = new ComponentNode(diagram, "Component.jar", p);
        centerPainter.paint(theNode);

        p.x = 5;

        users.keySet().stream()
                .map(name -> new ComponentNode(diagram, name, p))
                .peek(usersPainter::paint)
                .peek(node -> nestPackages(node, users))
                .peek(node -> dependency(node, theNode))
                .reduce(TaskSamples::locateVertical);

        p.x = 600;

        used.keySet().stream()
                .map(name -> new ComponentNode(diagram, name, p))
                .peek(usedPainter::paint)
                .peek(node -> nestPackages(node, used))
                .peek(node -> dependency(theNode, node))
                .reduce(TaskSamples::locateVertical);

        diagram.save(dir, "componentDependencies.svg");
    }

    private static void nestClasses(PackageNode node, Map<String, List<String>> classes) {
        List<String> classesNames = classes.get(node.getName());
        classesNames.forEach(node::createClassIconNode);
    }

    private static void nestPackages(ComponentNode node, Map<String, List<String>> packages) {
        List<String> packagesNames = packages.get(node.getName());
        packagesNames.forEach(node::createPackageIconNode);
    }

    private static void dependency(Shape source, Shape target) {
        DependencyEdge e = new DependencyEdge(diagram, source, target);
        e.setSourceAnchor(new AnchorRight());
        e.setTargetAnchor(new AnchorLeft());
        e.setRouterHVH();
    }

    private static <T extends Shape> T locateVertical(T node, T nextNode) {
        nextNode.setY(node.getBottom() + 50);
        return nextNode;
    }
}
