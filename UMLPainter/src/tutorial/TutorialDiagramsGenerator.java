package tutorial;

import uml.dd.AlignmentKind;
import uml.dd.Bounds;
import uml.dd.Point;
import uml.diagram.*;
import uml.diagram.anchors.AnchorBottom;
import uml.diagram.anchors.AnchorLeft;
import uml.diagram.anchors.AnchorRight;
import uml.diagram.anchors.AnchorTop;
import uml.diagram.graphical.*;
import uml.diagram.svg.SVGDiagram;
import uml.diagram.svg.SVGIconNode;
import uml.diagram.svg.edges.InheritanceEdge;
import uml.diagram.svg.graphical.*;
import uml.diagram.svg.nodes.*;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class TutorialDiagramsGenerator {
  private static String dir;

  public static void generate(String tutorialPath) {
    dir = tutorialPath;
    new File(tutorialPath).mkdirs();

    genTinyNodes();
    genGraphicalElements(tutorialPath);
    genIconNodes();
    genStructuredNodesCollapsed();
    genStructuredNodesExpanded();
    genAnchorDefault();
    genRouter_HV_VH();
    genRouter_HVH_VHV();
  }

  private static void genGraphicalElements(String dir) {
    SVGDiagram diagram = new SVGDiagram("Графические элементы");
    Point p = new Point(15, 15);

    Point pFigures = rectanglesDemo(diagram, p);
    p.x = pFigures.x;
    p.y = pFigures.y + 50;

    p = textDemo(diagram, p);
    p.x = pFigures.x;
    p.y += 50;

    p = circleDemo(diagram, p);
    p.x = pFigures.x;
    p.y += 50;

    p = polylineDemo(diagram, p);
    p.x = pFigures.x;
    p.y += 50;

    drawAxis(diagram, p, 200);

    diagram.save(dir, "GraphicalElements.svg");
  }

  private static Point polylineDemo(SVGDiagram diagram, Point p) {
    double diamondSize = 50;

    Point diamondCenter = new Point(p.x + diamondSize, p.y + diamondSize * 2);
    List<Point> pointsGlass = getDiamond(diamondCenter, diamondSize);

    Polyline diamondGlass= new SVGPolyline(diagram, pointsGlass);
    diamondGlass.setColor("blue");

    diamondCenter.x += 3 * diamondSize;

    List<Point> pointsSolid = getDiamond(diamondCenter, diamondSize);
    Polyline diamondSolid= new SVGPolyline(diagram, pointsSolid);
    diamondSolid.setColor("blue");
    diamondSolid.setBackground("cyan");

    p.y = diamondGlass.getBottom();
    return p;
  }

  private static List<Point> getDiamond(Point pc, double size) {
    return Arrays.asList(
            new Point(pc.x - size, pc.y),
            new Point(pc.x, pc.y - size),
            new Point(pc.x + size, pc.y),
            new Point(pc.x, pc.y + size),
            new Point(pc.x - size, pc.y)
    );
  }

  private static Point circleDemo(SVGDiagram diagram, Point p) {
    double radius = 50;
    p.x += radius;
    p.y += radius;

    Circle circleSolid = new SVGCircle(diagram, p, radius);
    circleSolid.setColor("Teal");
    circleSolid.setBackground("LightSeaGreen");
    p = circleSolid.getBounds().getRightTop();
    p.x += radius * 2;
    p.y += radius;

    Circle circleGlass = new SVGCircle(diagram, p, radius);
    circleGlass.setColor("DarkGreen");

    return circleGlass.getBounds().getLeftBottom();
  }

  private static Point rectanglesDemo(SVGDiagram diagram, Point p) {
    Rectangle rectSolid = new SVGRectangle(diagram, new Bounds(p.x, p.y, 100, 100));
    rectSolid.setColor("navy");
    rectSolid.setBackground("SkyBlue");
    p = rectSolid.getBounds().getRightTop();
    p.x += 50;

    // Прозрачный прямоугольник.
    Rectangle rectGlass = new SVGRectangle(diagram, new Bounds(p.x, p.y, 100, 100));
    rectGlass.setColor("Indigo");
    p = rectGlass.getBounds().getRightTop();
    p.x += 50;

    Rectangle rectSolidR = new SVGRectangle(diagram, new Bounds(p.x, p.y, 100, 100), 20);
    rectSolidR.setColor("DarkGreen");
    rectSolidR.setBackground("Chartreuse");
    p = rectSolidR.getBounds().getRightTop();
    p.x += 50;

    // Прозрачный прямоугольник.
    Rectangle rectGlassR = new SVGRectangle(diagram, new Bounds(p.x, p.y, 100, 100), 20);
    rectGlassR.setColor("Crimson");

    return rectSolid.getBounds().getLeftBottom();
  }

  private static Point textDemo(SVGDiagram diagram, Point pStart) {
    double lineLen = 100;
    Point p = new Point(pStart.x  + 300, pStart.y);

    Point lineEnd = new Point(p.x, p.y + lineLen);
    Line vertical = new SVGLine(diagram, p, lineEnd);
    vertical.setColor("Red");

    Text textLeft = new SVGText(diagram, "Text start (default)", p);
    textLeft.setColor("purple");
    p.y += 20;

    Text textMiddle = new SVGText(diagram, "setAlignment(AlignmentKind.center)", p);
    textMiddle.setAlignment(AlignmentKind.center);
    textMiddle.setColor("purple");
    p.y += 20;

    Text textRight = new SVGText(diagram, "setAlignment(AlignmentKind.end)", p);
    textRight.setAlignment(AlignmentKind.end);
    textRight.setColor("purple");

    p.x = pStart.x;
    p.y = lineEnd.y;

    return p;
  }

  private static void drawAxis(SVGDiagram diagram, Point axisStart, double axisLen) {
    axisStart.y += axisLen;
    double k = 0.75;

    Point xAxisEnd = new Point(axisStart.x + axisLen, axisStart.y);
    Point yAxisEnd = new Point(axisStart.x, axisStart.y - axisLen);

    Line axisX = new SVGLine(diagram, axisStart, xAxisEnd);
    Line axisY = new SVGLine(diagram, axisStart, yAxisEnd);
    axisX.setColor("navy");
    axisY.setColor("navy");

    Line optimal = new SVGLine(diagram,
            new Point(axisStart.x + k * axisLen, axisStart.y),
            new Point(axisStart.x, axisStart.y - k * axisLen));
    optimal.setColor("red");
  }

  private static void genTinyNodes() {
    SVGDiagram diagram = new SVGDiagram("Безымянные пиктограммы с всплывающей подсказкой");
    Point p = new Point(5, 5);

    new PackageTinyNode(diagram, "Package tiny", p);
    p.x += 20;

    new InterfaceTinyNode(diagram, "Interface tiny",p);
    p.x += 20;

    new ClassTinyNode(diagram, "Class tiny", p);
    p.x += 20;

    new EnumerationTinyNode(diagram, "Package tiny", p);
    p.x += 20;

    new ComponentTinyNode(diagram, "Component tiny", p);

    diagram.save(dir, "TinyNodes.svg");
  }

  private static void genStructuredNodesExpanded() {
    SVGDiagram diagram = new SVGDiagram("Раскрытые структурированные узлы");

    ClassifierNode node;

    Point p = new Point(5, 5);

    node = new EnumerationNode(diagram, "Color", p);
    node.createAttributeNode("white");
    node.createAttributeNode("black");
    node.createAttributeNode("red");
    node.createAttributeNode("green");
    node.createAttributeNode("blue");
    p = node.getBounds().getBottomCenter();
    p.y += 10;

    node = new ClassNode(diagram, "Point", p);
    node.createAttributeNode("- x: Double");
    node.createAttributeNode("- y: Double");
    Text txt = node.createOperationNode("+ distance(p1: Point, p2: Point): double");
    txt.setUnderline();
    node.createOperationNode("+ distance(p: Point): double");
    p = node.getBounds().getBottomCenter();
    p.y += 10;

    node = new InterfaceNode(diagram, "Layouter", p);
    node.createOperationNode("+ layout()");
    node.createClassIconNode("NoneLayouter");
    p = node.getBounds().getBottomCenter();
    p.y += 10;

    ComponentNode cNode = new ComponentNode(diagram, "swt.jar", p);
    cNode.createPackageIconNode("widgets");
    cNode.createPackageIconNode("painting");
    cNode.createPackageIconNode("layout");
    p = cNode.getBounds().getBottomCenter();
    p.y += 10;

    PackageNode pNode = new PackageNode(diagram, "diagram", p);
    pNode.createInterfaceIconNode("Shape");
    pNode.createClassIconNode("Circle");
    pNode.createClassIconNode("Rectangle");
    pNode.createPackageIconNode("nodes");
    pNode.createPackageIconNode("edges");
    pNode.createPackageIconNode("anchors");
    pNode.createPackageIconNode("routers");

    diagram.save(dir, "StructuredNodesExpanded.svg");
  }

  private static void genStructuredNodesCollapsed() {
    SVGDiagram diagram = new SVGDiagram("Сжатые структурированные узлы");

    Point p = new Point(5, 5);

    ClassifierNode node = new ClassNode(diagram, "Class", p);
    p = node.getBounds().getRightBottom();
    p.x += 10;

    node = new InterfaceNode(diagram, "Interface", p);
    p = node.getBounds().getRightBottom();
    p.x += 10;

    node = new EnumerationNode(diagram, "Enumeration", p);
    p = node.getBounds().getRightBottom();
    p.x += 10;

    node = new ComponentNode(diagram, "swt.jar", p);
    p = node.getBounds().getRightBottom();
    p.x += 10;

    new PackageNode(diagram, "Package", p);

    diagram.save(dir, "StructuredNodesCollapsed.svg");
  }

  private static void genIconNodes() {
    SVGDiagram diagram = new SVGDiagram("Узлы - пиктограммы");

    Point p = new Point(5, 5);

    SVGIconNode icon = new ClassIconNode(diagram, "Class", p);
    p = icon.getBounds().getRightBottom();

    icon = new InterfaceIconNode(diagram, "Interface", p);
    p = icon.getBounds().getRightBottom();

    icon = new EnumerationIconNode(diagram, "Enumeration", p);
    p = icon.getBounds().getRightBottom();

    icon = new ComponentIconNode(diagram, "swt.jar", p);
    p = icon.getBounds().getRightBottom();

    new PackageIconNode(diagram, "Package", p);

    diagram.save(dir, "IconNodes.svg");
  }

  private static void genAnchorDefault() {
    SVGDiagram diagram = new SVGDiagram("Умалчиваемые якоря ребер графа");

    ClassNode grandFather = new ClassNode(diagram, "GrandFather", 10, 10);
    ClassNode father = new ClassNode(diagram, "Father", 100, 100);
    ClassNode son = new ClassNode(diagram, "Son", 250, 150);

    Edge edge2GrandFather = new InheritanceEdge(diagram, father, grandFather);
    edge2GrandFather.setSourceAnchor(new AnchorTop());
    edge2GrandFather.setTargetAnchor(new AnchorBottom());

    Edge edge2Father = new InheritanceEdge(diagram, son, father);
    edge2Father.setSourceAnchor(new AnchorLeft());
    edge2Father.setTargetAnchor(new AnchorRight());

    diagram.save(dir, "AnchorDefault.svg");
  }

  private static void genRouter_HV_VH() {
    SVGDiagram diagram = new SVGDiagram("Маршрутизаторы RouterHV и RouterVH");

    ClassNode grandFather = new ClassNode(diagram, "GrandFather", 10, 10);
    ClassNode father = new ClassNode(diagram, "Father", 100, 100);
    ClassNode son = new ClassNode(diagram, "Son", 250, 150);

    Edge edge2GrandFather = new InheritanceEdge(diagram, father, grandFather);
    edge2GrandFather.setSourceAnchor(new AnchorLeft());
    edge2GrandFather.setTargetAnchor(new AnchorBottom());
    edge2GrandFather.setRouterHV();

    Edge edge2Father = new InheritanceEdge(diagram, son, father);
    edge2Father.setSourceAnchor(new AnchorTop());
    edge2Father.setTargetAnchor(new AnchorRight());
    edge2Father.setRouterVH();

    diagram.save(dir, "RouterHVandVH.svg");
  }

  private static void genRouter_HVH_VHV() {
    SVGDiagram diagram = new SVGDiagram("Маршрутизаторы RouterHVH и RouterVHV");

    ClassNode grandFather = new ClassNode(diagram, "GrandFather", 10, 10);
    ClassNode father = new ClassNode(diagram, "Father", 100, 100);
    ClassNode son = new ClassNode(diagram, "Son", 250, 150);

    Edge edge2GrandFather = new InheritanceEdge(diagram, father, grandFather);
    edge2GrandFather.setSourceAnchor(new AnchorTop());
    edge2GrandFather.setTargetAnchor(new AnchorBottom());
    edge2GrandFather.setRouterVHV();

    Edge edge2Father = new InheritanceEdge(diagram, son, father);
    edge2Father.setSourceAnchor(new AnchorLeft());
    edge2Father.setTargetAnchor(new AnchorRight());
    edge2Father.setRouterHVH();

    diagram.save(dir, "RouterHVHandVHV.svg");
  }
}
