package uml.dd;

/**
 * Прямоугольные границы. 
 */
public class Bounds {
	public double x = 0.0;
	public double y = 0.0;
	public double width = 0.0;
	public double height = 0.0;

	public Bounds(double x, double y, double width, double height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public Bounds(double width, double height) {
		this.width = width;
		this.height = height;
	}

	public Bounds() {
	}

	public double getLeft() { return x; }
	public double getTop()  { return y; }
	public double getRight()  { return x + width; }
	public double getBottom() { return y + height; }

	public Point getLeftTop()     { return new Point(x, y); }
	public Point getLeftCenter()  { return new Point(x, y + 0.5 * height); }
	public Point getLeftBottom()  { return new Point(x, y + height); }
	public Point getRightTop()    { return new Point(x + width, y); } 
	public Point getRightCenter() { return new Point(x + width, y + 0.5 * height); } 
	public Point getRightBottom() { return new Point(x + width, y + height); }
	
	public Point getTopCenter() { return new Point(x + 0.5 * width, y); }
	public Point getBottomCenter() { return new Point(x + 0.5 * width, y + height); }
}
