package uml.dd;

public class Dimension {
	public final double width;
	public final double height;

	public Dimension(double width, double height) {
		this.width = width;
		this.height = height;
	}
}
