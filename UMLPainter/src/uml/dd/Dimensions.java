package uml.dd;

import static java.lang.Math.max;

public class Dimensions {
	static public Point point(Dimension d) {
		return new Point(d.width, d.height);
	}
	
	static public Dimension union(Dimension d1, Dimension d2) {
		return new Dimension(max(d1.width, d2.width), max(d1.height, d2.height));
	}
}
