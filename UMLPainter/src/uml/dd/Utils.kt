package uml.dd


infix fun Double.point(y: Double) = Point(this, y)