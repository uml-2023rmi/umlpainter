package uml.dd;

import static java.lang.String.format;

public class Color {
  public final int red;
  public final int green;
  public final int blue;

  public Color(int red, int green, int blue) {
    this.red = red;
    this.green = green;
    this.blue = blue;
  }

  public String getRGB() {
    return format("rgb(%d,%d,%d)", red, green, blue);
  }

  public String getRGBA(double alpha) {
    return format("rgba(%d,%d,%d,%f)", red, green, blue, alpha);
  }

  public static Color lighter(Color color, double ratio) {
    int r = (int) ((255 - color.red) * ratio + color.red);
    int g = (int) ((255 - color.green) * ratio + color.green);
    int b = (int) ((255 - color.blue) * ratio + color.blue);
    return new Color(r, g, b);
  }

  public static Color darker(Color color, double ratio) {
    int r = (int) (color.red - ratio * color.red);
    int g = (int) (color.green - ratio * color.green);
    int b = (int) (color.blue - ratio * color.blue);
    return new Color(r, g, b);
  }

  public static Color blend(Color color1, Color color2, double ratio) {
    int r = (int) ((color2.red - color1.red) * ratio + color1.red);
    int g = (int) ((color2.green - color1.green) * ratio + color1.green);
    int b = (int) ((color2.blue - color1.blue) * ratio + color1.blue);
    return new Color(r, g, b);
  }
}
