package uml.dd;

public enum AlignmentKind {
  start,
  end,
  center
}
