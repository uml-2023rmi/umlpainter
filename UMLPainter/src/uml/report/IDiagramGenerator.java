package uml.report;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import uml.diagram.svg.SVGDiagram;
import java.lang.Class;

import java.io.File;

/**
 * Интерфейс генераторов UML-диаграмм для элемента UML-модели.
 *
 * @author <a href="mailto:vladimir.romanov@gmail.com">Romanov V.Y.</a>
 */
public interface IDiagramGenerator {
  /**
   * Для какого элемента UML-модели генерируется UML-диаграмма.
   *
   * @return класс элемента UML-модели.
   */
  Class<? extends Element> getElementClass();

  /**
   * Получить имя вида диаграммы. Должно быть коротким и на английском языке,
   * поскольку используется для формирования имени файла для этой диаграммы.
   *
   * @return имя вида диаграммы
   */
  String getKind();

  /**
   * Получить имя диаграммы. Должно быть коротким и на русском языке,
   * поскольку используется в закладке и показывается пользователю.
   *
   * @return имя диаграммы.
   */
  String getName();

  /**
   * Получить заголовок диаграммы. Полное имя используемое в качестве
   * всплывающей подсказки для закладки. Должно быть полным и на русском
   * языке.
   *
   * @return заголовок диаграммы.
   */
  String getTooltip();

  /**
   * Сгенерировать диаграмму для элемента UML-модели
   * и встроить ее в элемент DOM модели.
   *
   * @param named элемент UML-модели
   * @param diagram диаграмма для рисования.
   */  void generate(NamedElement named, SVGDiagram diagram);

  /**
   * Сгенерировать диаграмму для элемента UML-модели
   * и встроить ее в элемент DOM модели.
   *
   * @param named элемент UML-модели
   * @param owner элемент DOM модели.
   */
  default void generate(NamedElement named, org.dom4j.Element owner) {
    SVGDiagram diagram = new SVGDiagram(owner);
    generate(named, diagram);
  }

  /**
   * Сгенерировать диаграмму для элемента UML-модели
   * и записать ее в файл в заданной папке.
   *
   * @param named    элемент UML-модели
   * @param dir      папка для диаграммы
   * @param fileName файл для диаграммы
   */
  default void generate(NamedElement named, String dir, String fileName) {
    new File(dir).mkdirs();

    SVGDiagram diagram = new SVGDiagram(getTooltip());

    generate(named, diagram);

    diagram.save(dir, fileName);
  }
}
