package uml.report;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.eclipse.uml2.uml.NamedElement;

import uml.util.Util;

public class ReportUtil {
	static 
	public Element getHtml(Document doc) {
		doc.addDocType("html", null, null);
		
		Element html = doc.addElement("html");
		html.addAttribute("lang", "ru");
		
		Element head = html.addElement("head");

		head.addElement("meta")
			.addAttribute("charset", "utf-8") 
			.addAttribute("http-equiv", "Content-Type")
			.addAttribute("content", "text/html; charset=UTF-8");
	
		head.addElement("link")
			.addAttribute("rel", "stylesheet")
			.addAttribute("type", "text/css")
			.addAttribute("href", "../css/report.css");
		return html;
	}

	static 
	public Element getBody(Document doc) {
		Element html = getHtml(doc);

		return html.addElement("body");
	}

	static 
    public Element addLink(Element tag, String linkFile, String text, String target) {
		return tag
				.addElement("a")
				.addAttribute("href", linkFile)
				.addAttribute("target", target)
				.addText(text);
	}

	/**
	 * Выдать url на файл-диаграмму для указанного элемента UML-модели.
	 * 
	 * @param elementName
	 *            - имя элемента модели для которого создана диаграмма.
	 * @param kindName
	 *            - вид диаграммы для этого элемента модели.
	 * @return имя файла с диаграммой указанного вида.
	 */
	static
	public String getDiagramURL(String elementName, String kindName) {
		return elementName + ".-" + kindName + "-.html";
	}

	/**
	 * Выдать url на файл-диаграмму для указанного элемента UML-модели.
	 * 
	 * @param ne
	 *            - элемент модели для которого создана диаграмма.
	 * @param kindName
	 *            - вид диаграммы для этого элемента модели.
	 * @return имя файла с диаграммой указанного вида.
	 */
	static
	public String getDiagramURL(NamedElement ne, String kindName) {
		String name = Util.javaName(ne);
		return getDiagramURL(name, kindName);
	}

	/**
	 * Имя файла для фрейма с закладками-типам диаграмм 
	 * для заданного элемента UML-модели.
	 * 
	 * @return имя файла с закладками-типам диаграмм для выбранного элемента
	 *         UML-модели.
	 */
	static 
	public String getDiagramFrameURL(NamedElement ne) {
		String eName = Util.javaName(ne);
		return eName  + ".--frame--.html";
	}

	static 
	public void save(Document document, String dir, String fileName) {
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(dir + "/" + fileName);
	
			OutputFormat format = OutputFormat.createPrettyPrint();
	
			XMLWriter writer = new XMLWriter(fos, format);
	
			writer.write(document);
	
			writer.flush();
		} catch (FileNotFoundException e) {
			System.out.format("File not found: %s %n", e);
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			System.out.format("UnsupportedEncodingException: %s %n", e);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}