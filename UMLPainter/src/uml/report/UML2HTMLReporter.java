package uml.report;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.*;
import resources.Resources;
import uml.report.controls.HTMLTabFolder;
import uml.report.controls.HTMLTabItem;
import uml.report.reporters.*;
import uml.util.Comparators;
import uml.util.Util;

import java.io.File;
import java.util.*;

import static java.lang.String.format;
import static java.util.stream.Collectors.*;
import static uml.report.DiagramGeneratorRegistry.generatorsMap;
import static uml.util.Util.javaName;

/**
 * Генератор отчетов для UML-модели в формате HTML5+SVG
 * 
 * @author <a href="mailto:vladimir.romanov@gmail.com">Romanov V.Y.</a>
 */
public class UML2HTMLReporter {
	/**
	 * Фрейм для "оглавления отчета" (содержимое UML-модели).
	 */
	private static final String tableOfContentFrame = "tableOfContentFrame";

	/**
	 * Фрейм с закладками (tabs) для всех видов UML-диаграмм.
	 */
	private static final String diagramsTabsFrame = "diagramsTabsFrame";

	/**
	 * Корневая папка для генерации отчета.
	 */
	private static String rootDir;

	/**
	 * Папка для генерации html-файлов.
	 */
	private static String htmlDir;
	private static List<Model> models;

	static {
		// Регистрация плагинов.
		DiagramGeneratorRegistry.register(new ClassAllParentsGenerator());
		DiagramGeneratorRegistry.register(new InterfaceParentsGenerator());
		DiagramGeneratorRegistry.register(new ComponentDependencies());
	}

	/**
	 * Сгенерировать отчет для UML-модели
	 * 
	 * @param models UML-модели
	 * @param dir   папка для отчета
	 */
	public static void generateReport(List<Model> models, String dir) {
		Model model = models.get(0);
		rootDir = format("%s/%s", dir, model.getName());
		htmlDir = rootDir + "/html";

		new File(rootDir).mkdirs();
		new File(htmlDir).mkdirs();

		Resources.copyResources(rootDir);
		generateFrame(models);
	}

	/**
	 * Сгенерировать файл <b>index.html</b> описывающий разделение окна браузера на
	 * 2 части (2 фрейма).
	 * 
	 * @param _models UML-модель для которой генерируется отчет.
	 */
	private static void generateFrame(List<Model> _models) {
		models = _models; 
		Model model = models.get(0);
		
		String packagesFileName = "_all-packages.html";
		String stubFileName = "_stub.html";

		Document document = DocumentFactory.getInstance().createDocument();
		document.addDocType("html", null, null);

		Element html = document.addElement("html");

		Element fs = html
				.addElement("frameset")
				.addAttribute("cols", "25%,75%");
		fs.addElement("frame")
				.addAttribute("src", "html/" + packagesFileName)
				.addAttribute("name", tableOfContentFrame)
				.addAttribute("title", "Table Of Content Tabs");
		fs.addElement("frame")
				.addAttribute("src", "html/" + stubFileName)
				.addAttribute("name", diagramsTabsFrame)
				.addAttribute("title", "Diagram tabs");

		ReportUtil.save(document, rootDir, "index.html");

		generateCover(htmlDir, stubFileName);
		generateTableOfContentFiles(model, htmlDir, packagesFileName);
	}

	/**
	 * Сгенерировать html-файл с датой и временем генерации отчета.
	 * 
	 * @param dir      папка файла
	 * @param fileName имя файла
	 */
	private static void generateCover(String dir, String fileName) {
		Document doc = DocumentFactory.getInstance().createDocument();

		Element body = ReportUtil.getBody(doc);
		body.addElement("p").addText("Report " + new java.util.Date());

		ReportUtil.save(doc, dir, fileName);
	}

	/**
	 * Сгенерировать файлы со списками гиперссылок на диаграммы.
	 * 
	 * @param model    UML-модель для которой генерируется оглавление.
	 * @param dir      папка для генерации оглавления.
	 * @param fileName файл для генерации оглавления.
	 */
	private static void generateTableOfContentFiles(Model model, String dir, String fileName) {
		Map<Package, List<Type>> map = new TreeMap<>(Comparators.modelComparator);

		collectContent(model, map);
		Set<Package> packages = map.keySet();
		Collection<List<Type>> types = map.values();

		// Заголовок для закладок.
		HTMLTabFolder folder = new HTMLTabFolder("Модель " + model.getName());

		//
		// Пакеты.
		//
		HTMLTabItem packageItem = new HTMLTabItem(folder);
		packageItem.setText("Пакеты");
		packageItem.setFile(fileName);
		packageItem.setTooltip("Пакеты в UML-модели");
		packages.forEach(p -> generateLinkAndDiagram(p, packageItem.content, dir));

		//
		// Классификаторы.
		//
		String classifiersFileName = "_all-classifiers.html";

		HTMLTabItem classifierItem = new HTMLTabItem(folder);
		classifierItem.setText("Классификаторы");
		classifierItem.setFile(classifiersFileName);
		classifierItem.setTooltip("Классификаторы в UML-модели");

		Set<Type> classifiers = new TreeSet<>(Comparators.modelComparator);
		types.forEach(classifiers::addAll);

		classifiers.stream().filter(it -> it instanceof Classifier).map(it -> (Classifier) it)
				.forEach(cls -> generateLinkAndDiagram(cls, classifierItem.content, dir));

		//
		// Компоненты.
		//
		String componentsFileName = "_all-components.html";

		HTMLTabItem componentItem = new HTMLTabItem(folder);
		componentItem.setText("Компоненты");
		componentItem.setFile(componentsFileName);
		componentItem.setTooltip("Компоненты в UML-модели");

		Set<Artifact> components = model.getOwnedElements().stream().filter(e -> e instanceof Artifact)
				.map(e -> (Artifact) e).collect(toSet());

		components.forEach(comp -> generateLinkAndDiagram(comp, componentItem.content, dir));

		// Запишем закладки в файлы.
		folder.write(dir);
	}

	/**
	 * Собрать рекурсивно пакеты и типы вложенные в заданный пакет root и во все
	 * пакеты вложенные в пакет root.
	 * 
	 * @param root пакет с которого начинаем сбор.
	 * @param map  список в который собираем.
	 */
	private static void collectContent(Package root, Map<Package, List<Type>> map) {
		map.put(root, root.getOwnedTypes());

		root.getNestedPackages().forEach(nested -> collectContent(nested, map));
	}

	/**
	 * Сгенерировать закладку-гиперссылку и файл с UML-диаграммой, который будет
	 * загружаться во фрейм диаграмм при нажатии на эту ссылку мышкой.
	 * 
	 * @param ne  именованный элемент модели для которого в файл будет сгенерирована
	 *            диаграмма.
	 * @param tag в какой тег будет вложена гиперссылка
	 * @param dir папка в которую будет генерироваться файл с UML диаграммой.
	 */
	private static void generateLinkAndDiagram(NamedElement ne, Element tag, String dir) {
		String linkText = javaName(ne); // Имя пакета - гиперссылка.

		String kindName = getInterfaceName(ne.getClass());

		List<IDiagramGenerator> generators = generatorsMap.get(kindName);
		if (generators == null)
			return;

		String namedName = javaName(ne);

		String title = "Элемент модели: " + namedName;
		HTMLTabFolder tabFolder = new HTMLTabFolder(title);

		for (IDiagramGenerator g : generators) {
			String kind = g.getKind();
			String name = g.getName();
			String tooltip = g.getTooltip();
			String fileName = format("%s.-%s-.html", namedName, kind);

			HTMLTabItem item = new HTMLTabItem(tabFolder);
			item.setText(name);
			item.setFile(fileName);
			item.setTooltip(tooltip);

			if (g instanceof IEvolutionGenerator)
				((IEvolutionGenerator) g).setModels(models);

			g.generate(ne, item.content);
		}

		addIcon(tag, ne);

		String linkFile = tabFolder.getFirstFile();
		Element link = addLink(tag, linkFile, linkText, diagramsTabsFrame);

		if (ne instanceof Classifier) {
			Classifier cls = (Classifier) ne;

			if (cls.isAbstract())
				link.addAttribute("class", "abstract");
		}

		if (Util.isExternal(ne))
			link.addAttribute("class", "external");

		tag.addElement("br");
		tag.addText("\r\n");

		tabFolder.write(dir);
	}

	/**
	 * Добавить к тегу пиктограмму именованного элемента модели.
	 * 
	 * @param tag тега в DOM модели.
	 * @param ne  именованный элемент.
	 */
	private static void addIcon(Element tag, NamedElement ne) {
		String file;
		if (ne instanceof Package)
			file = "Package.png";
		else if (ne instanceof Class)
			file = "Class.png";
		else if (ne instanceof Interface)
			file = "Interface.png";
		else if (ne instanceof Enumeration)
			file = "Enumeration.png";
		else
			return;

		tag.addElement("img").addAttribute("src", "../img/" + file);
	}

	/**
	 * Добавить гиперссылку в документ.
	 * 
	 * @param tag      тег-собственник гиперссылки
	 * @param linkFile файл, на который ссылается гиперссылка.
	 * @param text     текст гиперссылки (показывается в браузере пользователю)
	 * @param target   имя фрейма в котором открыть файл гиперссылки
	 * @return элемент - ссылка.
	 */
	private static Element addLink(Element tag, String linkFile, String text, String target) {
		return tag.addElement("a").addAttribute("href", linkFile).addAttribute("target", target).addText(text);
	}

	/**
	 * Получить из имени класса реализующего интерфейс, имя этого интерфейса.
	 *
	 * @param named имя класса - потомка класса NamedElement (именованный элемент
	 *              UML-модели).
	 * @return имя интерфейса который реализует класс.
	 */
	private static String getInterfaceName(java.lang.Class<? extends NamedElement> named) {
		String typeName = named.getSimpleName();

		if (typeName.endsWith("Impl"))
			typeName = typeName.substring(0, typeName.length() - 4);

		return typeName;
	}
}