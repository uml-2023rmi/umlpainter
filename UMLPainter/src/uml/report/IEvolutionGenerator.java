package uml.report;

import org.eclipse.uml2.uml.Model;

import java.util.List;

/**
 * Генераторы отчетов для которых требуется
 * последовательность моделей для разных версий
 * одной и той же системы.
 */
public interface IEvolutionGenerator {
    void setModels(List<Model> models);
}
