package uml.report.reporters;

import static uml.util.Util.javaName;

import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;

import uml.dd.Point;
import uml.diagram.Painter;
import uml.diagram.svg.SVGDiagram;
import uml.diagram.svg.nodes.ComponentNode;
import uml.report.IDiagramGenerator;

public class ComponentDependencies implements IDiagramGenerator {
  @Override
  public java.lang.Class<? extends Element> getElementClass() {
    return Artifact.class;
  }

  @Override
  public String getKind() {
    return "compDepends";
  }

  @Override
  public String getName() {
    return "Зависимости компоненты";
  }

  @Override
  public String getTooltip() {
    return "Какие компоненты используют компоненту. Какие компоненты использует компонента";
  }

  @Override
  public void generate(NamedElement named, SVGDiagram diagram) {
    Artifact c = (Artifact) named;

    Painter usedPainter   = new Painter().pen("Green").brush("LightGreen").pencil("Green");
    Painter usersPainter  = new Painter().pen("Blue").brush("SkyBlue").pencil("Blue");
    Painter centerPainter = new Painter().pen("Brown").brush("Plum").pencil("Blue");

    Point p = new Point(5, 5);
    ComponentNode theNode = new ComponentNode(diagram, javaName(c), p);
    centerPainter.paint(theNode);

    diagram.layout();
  }
}
