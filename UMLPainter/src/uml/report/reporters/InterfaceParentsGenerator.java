package uml.report.reporters;

import static uml.dd.Dimensions.union;
import static uml.util.UMLUtil.javaName;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;

import uml.dd.Dimension;
import uml.dd.Dimensions;
import uml.dd.Point;
import uml.dd.Points;
import uml.diagram.svg.SVGDiagram;
import uml.diagram.anchors.AnchorBottom;
import uml.diagram.anchors.AnchorLeft;
import uml.diagram.anchors.AnchorRight;
import uml.diagram.svg.edges.ImplementationEdge;
import uml.diagram.svg.edges.InheritanceEdge;
import uml.diagram.svg.nodes.ClassNode;
import uml.diagram.svg.nodes.InterfaceNode;
import uml.diagram.routers.RouterHVH;
import uml.relations.UMLRelations;
import uml.relations.UMLRelations.UMLClasses;
import uml.report.IDiagramGenerator;

public class InterfaceParentsGenerator implements IDiagramGenerator {
	private SVGDiagram diagram;

	@Override
	public java.lang.Class<? extends org.eclipse.uml2.uml.Element> getElementClass() {
		return Interface.class;
	}

	@Override
	public String getKind() {
		return "parents";
	}

	@Override
	public String getName() {
		return "Предки";
	}

	@Override
	public String getTooltip() {
		return "Предки интерфейса";
	}

	public void generate(NamedElement named, SVGDiagram diagram){
		this.diagram = diagram;
		Interface i = (Interface) named;
		
		InterfaceNode iNode = new InterfaceNode(diagram, javaName(i), 10, 10);
		iNode.setBackground("Yellow");
		iNode.setColor("Brown");
		
		Dimension parentsSize = drawParents(iNode, i);
		drawImplementors(iNode, i, Dimensions.point(parentsSize));
		diagram.layout();
	}

	private Dimension drawParents(InterfaceNode childNode, Classifier i) {
		Dimension size = Points.dimension(childNode.getBounds().getRightBottom());
		
		final int space = 20; // Промежутки между узлами.
		final int shift = 40; // Сдвиг что бы оставить место для входа в узел стрелки.
		
		Point pParent = new Point(childNode.getLeft() + shift, 
				                  childNode.getBottom() + space);
		
		for (Generalization g : i.getGeneralizations()) {
			Classifier parent = g.getGeneral();
			String pName = javaName(parent);
			
			InterfaceNode parentNode = new InterfaceNode(diagram, pName, pParent);
			parentNode.setBackground("Cyan");
			parentNode.setColor("Blue");
			
			connectInheritance(childNode, parentNode);

			Dimension parentsSize = drawParents(parentNode, parent);
			size = union(size, parentsSize);
			pParent.y = size.height + space;
		}
		
		return size;
	}

	private void drawImplementors(InterfaceNode iNode, Interface i, Point p) {
		UMLClasses implementors = UMLRelations.implementations.get(i);
		if (implementors == null) return;
		
		p.y = iNode.getTop();
		p.x += 50;
		
		for (Class implementor : implementors) {
			ClassNode cNode = new ClassNode(diagram, javaName(implementor), p);
			cNode.setBackground("Violet");
			
			connectImplementation(cNode, iNode);
			
			p.y = cNode.getBottom() + 10;
		}
	}

	private void connectInheritance(InterfaceNode child, InterfaceNode parent) {
		InheritanceEdge edge = new InheritanceEdge(diagram, child, parent);
		edge.setSourceAnchor(new AnchorBottom(10));
		edge.setTargetAnchor(new AnchorLeft());
		edge.setRouterVH();		
	}

	private void connectImplementation(ClassNode source, InterfaceNode target) {
		ImplementationEdge edge = new ImplementationEdge(diagram, source, target);
		edge.setSourceAnchor(new AnchorLeft());
		edge.setTargetAnchor(new AnchorRight());
		edge.setRouter(new RouterHVH(10));		
	}
}