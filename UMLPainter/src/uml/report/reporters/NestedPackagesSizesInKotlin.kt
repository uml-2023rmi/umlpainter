package uml.report.reporters

import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.Package
import uml.dd.point
import uml.diagram.svg.SVGDiagram
import uml.diagram.svg.nodes.PackageNode
import uml.diagram.svg.nodes.PackageTinyNode
import uml.report.IDiagramGenerator

/**
 * Диаграмма показывающая размер пакетов вложенных в заданный пакет.
 */
class NestedPackagesSizesInKotlin : IDiagramGenerator {
    override fun getElementClass() = Package::class.java
    override fun getKind() = "nestedSizes"
    override fun getName() = "Размеры пакетов"
    override fun getTooltip() = "Размеры пакетов вложенных в выбранный пакет"

    override fun generate(named: NamedElement, diagram: SVGDiagram) {
        val thePackage = named as Package

        val packageNode = PackageNode(diagram, thePackage.name, 10.0 point 10.0)

        val nested = thePackage.nestedPackages
            .sortedWith(compareBy { it.deepSize })
            .reversed()

//        val minSize = nested.map { it.deepSize }.minOrNull() ?: 0;
        val k = 1.0 // 16.0 + 2.0 / (1 + minSize.toDouble())

        nested.map {
            val pSize = it.deepSize.toDouble()
            val tooltip = "${it.name}[$pSize]"

            val tiny = PackageTinyNode(packageNode.content, tooltip, 0.0 point 0.0)
            tiny.width = pSize * k
            tiny.height = pSize * k
            tiny
        }

        packageNode.content.layout()
        packageNode.layout()
        diagram.layout()
    }
}

val Package.size
    get() = ownedTypes.size

val Package.deepSize: Int
    get() = size + nestedPackages.sumOf { it.deepSize }
