package uml.report.reporters;
import org.eclipse.uml2.uml.*;
import org.eclipse.uml2.uml.Class;
import uml.diagram.anchors.AnchorBottom;
import uml.diagram.anchors.AnchorRight;
import uml.diagram.anchors.AnchorTop;
import uml.diagram.svg.SVGDiagram;
import uml.diagram.svg.edges.ImplementationEdge;
import uml.diagram.svg.edges.InheritanceEdge;
import uml.diagram.svg.nodes.ClassNode;
import uml.diagram.svg.nodes.InterfaceNode;
import uml.report.IDiagramGenerator;

import static uml.util.UMLUtil.javaName;

/**
 * TODO Макаров, Идрисов
 */
public class ClassAllParentsGenerator implements IDiagramGenerator {
    private SVGDiagram diagram;

    @Override
    public java.lang.Class<? extends Element> getElementClass() {
        return Class.class;
    }

    @Override
    public String getKind() {
        return "cAllParents";
    }

    @Override
    public String getName() {
        return "Предки";
    }

    @Override
    public String getTooltip() {
        return "Все предки класса";
    }

    @Override
    public void generate(NamedElement named, SVGDiagram diagram) {
        this.diagram = diagram;

        Classifier child = (Classifier) named;

        ClassNode childNode = generateParents(child);

        childNode.setBackground("Yellow");
        childNode.setColor("Brown");

        diagram.layout();
    }

    private ClassNode generateParents(Classifier child) {
        final double shiftV = 70;

        ClassNode childNode;

        if (child.getGeneralizations().isEmpty()) {
            childNode = new ClassNode(diagram, javaName(child), 10, 10);
        } else {
            Classifier parent = child.getGeneralizations().get(0).getGeneral();
            ClassNode parentNode = generateParents(parent);

            final double x = parentNode.getLeft();
            final double y = parentNode.getBottom() + shiftV;

            childNode = new ClassNode(diagram, child.getName(), x, y);

            connectInheritance(childNode, parentNode);

            generateInterfaces(childNode, child);
        }

        childNode.setBackground("SpringGreen");
        childNode.setColor("Green");

        return childNode;
    }

    private void generateInterfaces(ClassNode childNode, Classifier child) {
        final double shiftH = 10;
        final double shiftV = 50;

        double x = childNode.getRight() + shiftH;
        final double y = childNode.getTop() - shiftV;

        Class childClass = (Class)child;

        final int shiftI = (int) Math.floor(childNode.getHeight() / (childClass.getImplementedInterfaces().size() + 1));

        int countI = 0;

        for (Interface intr : childClass.getImplementedInterfaces()) {
            InterfaceNode interfaceNode = new InterfaceNode(diagram, javaName(intr), x, y);

            interfaceNode.iconNode.setBackground("Cyan");
            interfaceNode.iconNode.setColor("Blue");
            interfaceNode.iconNode.setTextColor("Blue");

            interfaceNode.setBackground("Violet");
            interfaceNode.setColor("Blue");

            connectInterface(childNode, interfaceNode, shiftI * (countI + 1));

            x += interfaceNode.getWidth() + shiftH;

            countI += 1;
        }
    }

    private void connectInheritance(ClassNode child, ClassNode parent) {
        InheritanceEdge edge = new InheritanceEdge(diagram, child, parent);
        edge.setSourceAnchor(new AnchorTop(10));
        edge.setTargetAnchor(new AnchorBottom(10));
    }

    private void connectInterface(ClassNode child, InterfaceNode parent, int shift) {
        ImplementationEdge edge = new ImplementationEdge(diagram, child, parent);
        edge.setSourceAnchor(new AnchorRight(shift));
        edge.setTargetAnchor(new AnchorBottom());
        edge.setRouterHV();
    }
}
