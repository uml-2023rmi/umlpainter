package uml.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.uml2.uml.Element;

public class DiagramGeneratorRegistry {
	/**
	 * Ключ элемента карты - класс описывающий элемент UML-модели.
	 * Значение элемента карты - список генераторов UML-диаграмм 
	 * для этого класса элементов UML-модели.
	 */
	static final Map<String,	List<IDiagramGenerator>> generatorsMap	= new HashMap<>();

	/**
	 * Регистрация генератора UML-диаграмм для заданного 
	 * в генераторе диаграмм класса элементов UML-модели.
	 * 
	 * @param generator - регистрируемый генератор диаграмм.
	 */
	public static void register(IDiagramGenerator generator) {
		Class<? extends Element> elementClass = generator.getElementClass();
		
		String typeName = elementClass.getSimpleName();
	
		List<IDiagramGenerator> generators;
		if (!generatorsMap.containsKey(typeName)) {
			// Для этого класса элементов UML-модели генераторов еще нет.
			generators = new ArrayList<>();
			generatorsMap.put(typeName, generators);
		} else
			generators = generatorsMap.get(typeName);
	
		generators.add(generator);
	}
}