package uml.report.controls;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import uml.report.ReportUtil;

public class HTMLTabFolder {
	final List<HTMLTabItem> tabs;
	
	private final String title;

	public HTMLTabFolder(String title) {
		this.title = title;
		tabs = new ArrayList<>();
	}

	public void add(HTMLTabItem item) {
		tabs.add(item);
	}

	public void write(String dir) {
		new File(dir).mkdirs();
		
		tabs.forEach(it -> it.gen(dir));
	}
	
	void generate(Element owner, HTMLTabItem selectedTab) {
		owner.addElement("p").addElement("b").addText(title);
		
		for (HTMLTabItem tab : tabs) {
			String tabText  = tab.text;
			String tooltip  = tab.tooltip;
			String linkFile = tab.fileName;
			String style    = (tab != selectedTab ? "tab" : "tab-selected");

			String target = "_self";
			Element link = ReportUtil.addLink(owner, linkFile, tabText, target);
			link.addAttribute("class", style);
			link.addAttribute("title", tooltip);
		}
	}

	/**
	 * @return имя файла для первой закладки.
	 */
	public String getFirstFile() {
		return tabs.get(0).fileName;
	}
}