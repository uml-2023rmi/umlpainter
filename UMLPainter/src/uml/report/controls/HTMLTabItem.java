package uml.report.controls;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;

import uml.report.ReportUtil;

public class HTMLTabItem {
	public String text;
	public String tooltip;
	public String fileName;
	
	private final Document doc;
	private final HTMLTabFolder folder;

	final Element tabs;
	public final Element content;

	public HTMLTabItem(HTMLTabFolder folder) {
		this.folder = folder;
		folder.add(this);
		
		doc  = DocumentFactory.getInstance().createDocument();
		Element html = ReportUtil.getHtml(doc);
		Element body = html.addElement("body");
		tabs = body.addElement("div");

		content = body.addElement("div");
		content.addAttribute("class", "tab-content");
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setFile(String fileName) {
		this.fileName = fileName;
	}

	public void setTooltip(String tooltip) {
		this.tooltip = tooltip;
	}

	public void gen(String dir) {
		folder.generate(tabs, this);
		
		ReportUtil.save(doc, dir, fileName);
	}
}