package uml.java.reverser;

public interface ProgressTracing {
	void begin(String message, int size);
	void step(int scale);
	void outMessage(String message);
	void done();
}
