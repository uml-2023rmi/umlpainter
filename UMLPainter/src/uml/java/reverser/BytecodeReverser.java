package uml.java.reverser;

import org.eclipse.uml2.uml.Model;

public interface BytecodeReverser {
	void reverseFolder(String jarsDirectory, Model model);
	void reverseJarFile(String jarFile, Model model);
	void reverseJarFileCollection(Iterable<String> files, Model model, ProgressTracing pt);
}
