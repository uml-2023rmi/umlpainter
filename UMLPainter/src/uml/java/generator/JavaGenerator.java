package uml.java.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.VisibilityKind;

/**
 * Генератор текста на языке Java из UML модели программы.
 */
public class JavaGenerator {
	/**
	 * Генерировать текст на языке Java для пакета из UML-модели.
	 * @param package_ пакет в UML-модели.
	 * @param packageDir корневая папка для генерации текстов пакета 
	 */
	public static void generatePackage(Package package_, String packageDir) {
		//
		// Генерируем классификаторы текущего пакета.
		//
		for (NamedElement m : package_.getOwnedMembers()) {
			// Внешний тип неизвестного вида (класс, интерфейс, перечисление?)
			if (m.hasKeyword("unknown"))
				continue; 

			if (m instanceof Class)  
				generateClass((Class) m, packageDir);
			else 
			if (m instanceof Interface)  
				generateInterface((Interface) m, packageDir);
			else 
			if (m instanceof Enumeration)  
				generateEnumeration((Enumeration) m, packageDir);
			else
			if (m instanceof Package)
				generatePackage((Package) m, packageDir + "/" + m.getName());
		} 
	}

	private static void makePackageDir(String packageDir) {
		File d = new File(packageDir);
		if (!d.exists())
			d.mkdirs();
	}

	/**
	 * Генерировать текст на языке Java для класса из UML-модели.
	 * @param class_ класс в UML-модели.
	 * @param packageDir корневая папка для генерации текстов класса 
	 */
	public static void generateClass(Class class_, String packageDir) {
		makePackageDir(packageDir);

		PrintStream ps = null;
		try {
			ps = new PrintStream(packageDir + "/" + class_.getName() + ".java");
		} catch (FileNotFoundException e) {
			return;
		}

		String modifiers = visibilityToJava(class_.getVisibility());

		if (class_.isAbstract())
			modifiers += "abstract ";

		if (class_.isLeaf())
			modifiers += "final ";
		
		
		genPackage(class_, ps);

		ps.format("%sclass %s ", modifiers, class_.getName());
		ps.format("{ %n");
		ps.format("} %n");

		ps.close();
	}
	/**
	 * Генерировать текст на языке Java для интерфейса из UML-модели.
	 * @param interface_ интерфейс в UML-модели.
	 * @param packageDir корневая папка для генерации текстов интерфейса 
	 */
	public static void generateInterface(Interface interface_, String packageDir) {
		makePackageDir(packageDir);

		PrintStream ps = null;
		try {
			ps = new PrintStream(packageDir + "/" + interface_.getName() + ".java");
		} catch (FileNotFoundException e) {
			return;
		}

		String modifiers = visibilityToJava(interface_.getVisibility());

		if (interface_.isLeaf())
			modifiers += " final ";

		genPackage(interface_, ps);

		ps.format("%sinterface %s ", modifiers, interface_.getName());
		ps.format("{ %n");
		ps.format("} %n");

		ps.close();
	}

	/**
	 * @param enum_ перечисление
	 * @param packageDir папка для генерации файла.
	 */
	public static void generateEnumeration(Enumeration enum_, String packageDir) {
		makePackageDir(packageDir);

		PrintStream ps = null;
		try {
			ps = new PrintStream(packageDir + "/" + enum_.getName() + ".java");
		} catch (FileNotFoundException e) {
			return;
		}

		String modifiers = visibilityToJava(enum_.getVisibility());

		genPackage(enum_, ps);

		ps.format("%senum %s ", modifiers, enum_.getName());
		ps.format("{ %n");
		ps.format("} %n");

		ps.close();
	}

	/**
	 * Генерировать текст на Java для пакета содержащего классификатор:
	 * класса, интерфейса, перечисления.
	 * <pre>
	 *     <b>package</b> имя-пакета;
	 * </pre>  
	 * 
	 * @param pe пакетируемый элемент (то, что можно вложить в пакет:
	 * класс, интерфейс, перечисление)
	 * 
	 * @param ps форматируемый поток вывода.
	 */
	private static void genPackage(PackageableElement pe, PrintStream ps) {
		Package ownerPackage = pe.getNearestPackage();

		Model model = ownerPackage.getModel();
		String modelName = model.getName();
		
		int shift = ownerPackage == model ? 0 : 1;
		String qPackName = ownerPackage.getQualifiedName()
				                       .replace("::", ".")
				                       .substring(modelName.length() + shift);
		
		ps.format("package %s; %n%n", qPackName);
	}

	/**
	 * Получить ключевое слово Java для представления видимости в модели UML.
	 * @param visibility значение видимости
	 * @return ключевое слово Java
	 */
	public static String visibilityToJava(VisibilityKind visibility) {
		return visibility == VisibilityKind.PACKAGE_LITERAL ? "" : visibility.getName() + ' ';
	}
}
