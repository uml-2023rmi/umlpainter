package uml.bpmn

import uml.diagram.svg.SVGDiagramElement
import uml.diagram.svg.SVGEdge
import uml.diagram.svg.SVGShape

class TransitionEdge(owner: SVGDiagramElement, source: SVGShape, target: SVGShape) : SVGEdge(owner, source, target) {
    init {
        setColor("navy")
        setBackground("white")
        tag.addAttribute("marker-end", "url(#arrowAdorn)")
        tag.addAttribute("stroke-width", "1")
    }
}
