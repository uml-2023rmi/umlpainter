package uml.bpmn

import uml.diagram.anchors.AnchorLeft
import uml.diagram.anchors.AnchorRight
import uml.diagram.svg.SVGDiagramElement
import uml.diagram.svg.SVGShape

infix fun SVGShape.transition(next: SVGShape): SVGShape {
    val edge = TransitionEdge(owningElement as SVGDiagramElement, this, next)

    next.x = bounds.right + 100
    next.y = bounds.rightCenter.y - 0.5 * next.height // По центру.

    edge.setSourceAnchor(AnchorRight())
    edge.setTargetAnchor(AnchorLeft())
    edge.layout()

    return next
}
