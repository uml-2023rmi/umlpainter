package uml.bpmn

import uml.diagram.svg.SVGDiagramElement
import uml.diagram.svg.SVGSection
import uml.diagram.svg.graphical.SVGText

class ActivityNode(owner: SVGDiagramElement, text: String)
    : SVGSection(owner) {
    init {
        paper.color = "black"
        paper.background = "white"
        paper.cornerRadius = 5.0

        val txt = SVGText(this, text)

        paper.width = txt.width
        paper.height = 40.0
        bounds = txt.bounds
        height = 40.0
    }
}
