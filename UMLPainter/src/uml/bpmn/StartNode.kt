package uml.bpmn

import uml.dd.Point
import uml.diagram.svg.SVGDiagramElement
import uml.diagram.svg.graphical.SVGCircle

class StartNode(owner: SVGDiagramElement, p: Point = Point(20.0,20.0))
    : SVGCircle( owner, Point(p.x + 10, p.y + 10), 10.0) {
        init {
            setBackground("black")
        }
    }