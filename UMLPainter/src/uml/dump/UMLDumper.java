package uml.dump;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;

import static java.lang.System.out;

/**
 * Дамп для содержимого UML-модели.
 */
public class UMLDumper {
	public static int prefixLen;

	public static void dump(Element element, int level) {
		String blanks = String.format("%" + 5*level + "s" , " ");

		for (Element owned : element.getOwnedElements()) {
			out.println(blanks + owned);
			
			if (!owned.getOwnedElements().isEmpty())
				dump(owned, level + 1);
		}
	}

	public static void dumpNamed(Element element, int level) {
		if (!(element instanceof NamedElement)) return;
		
		NamedElement ne = (NamedElement) element;
		
		String blanks = String.format("%" + 5*level + "s" , " ");
		String qName = ne.getQualifiedName();
		out.println(blanks + "Element: " + qName);

		for (Element owned : ne.getOwnedElements()) {
			if (!(owned instanceof NamedElement)) 
				continue;
			
			if (!owned.getOwnedElements().isEmpty())
				dumpNamed(owned, level + 1);
		}
	}

	public static void dumpImport(Element element, int level) {
		if (!(element instanceof NamedElement)) return;
		
		NamedElement ne = (NamedElement) element;
		
		String blanks = String.format("%" + 5*level + "s" , " ");
		String qName = ne.getName();
		String kind = "???";
		
		if (ne instanceof Class)
			kind = "class";
	
		if (ne instanceof Interface)
			kind = "interface ";

		if (ne instanceof Enumeration)
			kind = "enum ";

		if (ne instanceof Package)
			kind = "package ";

		if (ne instanceof Model)
			kind = "model ";

		if (ne instanceof Artifact)
			kind = "artifact ";
		
		out.printf("%s %s %s %n", blanks, kind, qName);

		for (Element owned : ne.getOwnedElements()) {
			if (!(owned instanceof NamedElement)) 
				continue;
			
			if (!owned.getOwnedElements().isEmpty())
				dumpImport(owned, level + 1);
			
			if (owned instanceof Operation) 
				continue;
			
			if (!(owned instanceof Namespace)) 
				continue;
			
			Namespace ns = (Namespace) owned;
			for (ElementImport im : ns.getElementImports()) {
				out.printf("%s import %s; [=%s]%n",
						blanks,
						im.getImportedElement().getQualifiedName(),
						im.getAlias());
				 im.getVisibility();
			}
		}
	}
	
	public static void dumpPackage(Package package_, PrintStream out) {
		String neName = "";
		String tagValues = "";
		String stereotype = "";
		String parents = "";
		String interfaces = "";
		String attributes = "";
		String methods = "";

		EList<NamedElement> members = package_.getOwnedMembers();
		
		for (NamedElement m : members) {
			neName = m.getQualifiedName().substring(prefixLen);
			tagValues = "";
			stereotype = "";
			parents = "";
			interfaces = "";
			attributes = "";
			methods = "";
			
			if (m instanceof Package)
				continue;	
	
			String vis = m.getVisibility().toString();
			if (!vis.equals("public"))
				tagValues += vis + " ";
			
			if (m instanceof Class) {	
				stereotype = "<<class>>";
				
				Class cls = (Class) m;
				
				if (cls.isAbstract()) {
					if (!tagValues.isEmpty())
						tagValues += ", ";

					tagValues += "abstract";
				}

				if (cls.isLeaf()) {
					if (!tagValues.isEmpty())
						tagValues += ", ";

					tagValues += "leaf";
				}

				parents = getParents(cls);
				interfaces = getInterfaces(cls);
				attributes = getAttributes(cls);
				methods = getMethods(cls);
				
			} else if (m instanceof Interface) {
				Interface inter = (Interface) m;
				
				stereotype = (!inter.hasKeyword("unknown") 
						? "<<interface>>"
						: "<<interface?>>");
				
				parents    = getParents(inter);
				attributes = getAttributes(inter);
				methods    = getMethods(inter);
			} else if (m instanceof Enumeration) {
				stereotype = "<<enum>>";
			} // if

			if (!tagValues.isEmpty())
				tagValues = "{ " + tagValues + " }";

			out.format("%14s %s %s %s %14s %s %s%n", 
					stereotype, neName, tagValues, 
					parents, interfaces, 
					attributes, methods
					);
		} // for

		for (NamedElement m : members) {
			if (!(m instanceof Package))
				continue;
			
			Package p = (Package) m;
			
			if(p.getName().equals("types"))
				continue;
			
			dumpPackage(p, out);
		} // for
	}

	private static String getMethods(Classifier cls) {
		return "";
	}

	private static String getAttributes(Classifier cls) {
		return "";
	}

	private static String getParents(Classifier cls) {
		return "";
	}


	private static String getInterfaces(Classifier cls) {
		return "";
	}

	public static void dumpModel(Model model, String dumpPath) {
		new File(dumpPath).mkdirs();

		PrintStream ps = null;
		try {
			ps = new PrintStream(dumpPath + "/" + model.getName() + ".dump.txt");
		} catch (FileNotFoundException e) {
			return;
		}
		
		dumpPackage(model, ps);

		ps.close();
	}
}
