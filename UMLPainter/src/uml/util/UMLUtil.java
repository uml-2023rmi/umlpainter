package uml.util;

import java.util.function.Consumer;
import java.util.function.Predicate;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;

/**
 * Класс для вспомогательных методов, которые упрощают работу с UML-моделью.
 * 
 * @author <a href="mailto:vladimir.romanov@gmail.com">Romanov V.Y.</a>
 */
public class UMLUtil {
	/**
	 * Количество типов в пакете thePackage и в пакетах вложенных в пакет
	 * thePackage.
	 * 
	 * @param thePackage
	 *            - измеряемый пакет.
	 * @return размер пакета.
	 */
	static 
	public long getSize(Package thePackage) {
		int n = thePackage.getOwnedTypes().size();
		
		for (Package p : thePackage.getNestedPackages())
			n += getSize(p);
		
		return n;
	}

	/**
	 * Пройти по пакету и выполнить заданное в параметре действие над элементом
	 * UML-модели.
	 * 
	 * @param p
	 *            - пакет, вложенные элементы которого проходим, выполняя над ними
	 *            действия.
	 * @param vizitFilter
	 *            - фильтр, определяющий какие вложенные пакеты пропускать.
	 * @param actionFilter
	 *            - фильтр, определяющий над какими элементами модели действия не
	 *            выполнять.
	 * @param action
	 *            - действие, которое надо выполнить над заданным элементом модели.
	 */
	static 
	public void walkPackage(Package p, 
			Predicate<Package> vizitFilter, 
			Predicate<NamedElement> actionFilter,
			Consumer<? super NamedElement> action) 
	{
		p.getMembers()
		 .stream()
		 .filter(actionFilter)
		 .forEach(action);
	
		p.getNestedPackages()
		 .stream()
		 .filter(vizitFilter)
		 .forEach(nested -> walkPackage(nested, vizitFilter, actionFilter, action));
	}

	/**
	 * Пройти рекурсивно по всем элементами которыми владеет заданный элемент модели
	 * и выполнить заданное в параметре действие над элементом UML-модели.
	 * 
	 * @param e            - заданный элемент модели, собственные элементы которого проходим,
	 *                     выполняя над ними действия.
	 * @param vizitFilter  - фильтр, определяющий какие собственные элементы модели
	 *                     не посещать (рекурсивно).
	 * @param actionFilter - фильтр, определяющий над какими элементами модели
	 *                     действия не выполнять.
	 * @param action       - действие, которое надо выполнить над заданным элементом
	 *                     модели.
	 */
	static 
	public void walkAll(Element e, 
			Predicate<Element> vizitFilter, 
			Predicate<Element> actionFilter,
			Consumer<? super Element> action) 
	{
		e.getOwnedElements()
		 .stream()
		 .filter(actionFilter)
		 .forEach(action);
	
		e.getOwnedElements()
		 .stream()
		 .filter(vizitFilter)
		 .forEach(owned -> walkAll(owned, vizitFilter, actionFilter, action));
	}

	/**
	 * Укороченное имя элемента UML-модели (без имени самой модели в начале).
	 * 
	 * @param named
	 *            - именованный элемент модели.
	 * @return укороченное квалифицированное имя элемента модели разделенное ::.
	 */
	static 
	public String getShortName(NamedElement named) {
		Model model = named.getModel();
	
		String mName = model.getName();
		String pName = named.getQualifiedName();
	
		int shift = (named == model ? 0 : 2);
		if (!mName.isEmpty())
			pName = pName.substring(mName.length() + shift);
	
		return pName;
	}
	
	static 
	public String javaName(NamedElement named) {
		return getShortName(named).replace("::", ".");
	}
	
	/**
	 * Укороченное имя элемента UML-модели (без имени самой модели в начале).
	 * 
	 * @param named
	 *            - именованный элемент модели.
	 * @return укороченное квалифицированное имя элемента модели разделенное точками.
	 */
	static 
	public String getJavaName(NamedElement named) {
		return getShortName(named).replace("::", ".");
	}
	
	/**
	 * Является ли элемент UML-модели внешним по отношению модели? При построении
	 * модели не встретилось объявление этих элементов, а лишь ссылки на них.
	 * 
	 * @param e
	 *            - элемент UML-модели.
	 * @return внешний или нет.
	 */
	static 
	public boolean isExternal(Element e) {
		if (!(e instanceof Package))
			return e.hasKeyword("external");

		Package p = (Package) e;
		for (NamedElement owned : p.getOwnedMembers())
			if (!isExternal(owned))
				return false;

		return true;
	}

	/**
	 * Является ли внешний элемент UML-модели неизвестного вида. 
	 * Неизвестно что это - класс, интерфейс, перечислениеы?
	 * 
	 * @param e
	 *            - элемент UML-модели.
	 * @return неизвестного вида или нет.
	 */
	static 
	public boolean isUnknown(Element e) {
		return e.hasKeyword("unknown");
	}
	
	/**
	 * Является ли пакет <b>mayBeExternal</b> внешним по отношению к пакету
	 * <b>base</b>?
	 * 
	 * @param base
	 *            - базовый пакет.
	 * @param mayBeExternal
	 *            -проверяемфй пакет.
	 * @return внешний или нет?
	 */
	static
	public boolean isPackageExternal(Package base, Package mayBeExternal) {
		String pQName = mayBeExternal.getQualifiedName();
		String baseQName = base.getQualifiedName();
		
		return !pQName.startsWith(baseQName);
	}

	/**
	 * Вложен ли пакет <b>compared</b> в пакет <b>p</b>
	 * 
	 * @param p - охватывающий пакет
	 * @param compared - вложенный пакет
	 * @return есть ли вложенность
	 */
	public static boolean isNested(Package p, Package compared) {
		if (p != compared)
			return false;
		
		String pQName = p.getQualifiedName();
		String comparedQName = compared.getQualifiedName();
		
		return comparedQName.startsWith(pQName);
	}
	
	
	/**
	 * Расположен ли один именованный элемент внутри другого именованного элемента.
	 * 
	 * @param tested - проверяемый элемент
	 * @param ne     в каком элементе может находиться.
	 * @return элемент <b>tested</b> расположен внутри элемента <b>ne</b>
	 */
	public static boolean isInside(NamedElement tested, NamedElement ne) {
		return tested.getQualifiedName().startsWith(ne.getQualifiedName()) && (tested != ne);
	}

	/**
	 * Расположен ли один именованный элемент вне другого именованного элемента.
	 * 
	 * @param tested - проверяемый элемент
	 * @param ne     вне какого элементе может находиться.
	 * @return элемент <b>tested</b> расположен вне элемента <b>ne</b>
	 */
	public static boolean isOutside(NamedElement tested, NamedElement ne) {
		return !tested.getQualifiedName().startsWith(ne.getQualifiedName());
	}
}