package uml.util;

import java.util.Comparator;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;

/**
 * Алгоритмы сортировки элементов UML-модели.
 */
public class Comparators {
	/**
	 * Сравнение по квалифицированному имени именованного элемента модели.
	 */
	public static final Comparator<? super NamedElement> qNameComparator =
					Comparator.comparing(NamedElement::getQualifiedName);

	public static final Comparator<? super NamedElement> modelComparator =
		(ne1, ne2) -> {
			int v1 = getWeight(ne1);
			int v2 = getWeight(ne2);
			
			if (v1 == v2)
				return qNameComparator.compare(ne1, ne2);
			
			return v1 - v2;
		};

	public static int getWeight(Element ne) {
		int base = Util.isExternal(ne)? 100 : 0;
			
		if (ne instanceof Interface)
			return base + 1;

		if (ne instanceof Enumeration)
			return base + 2;

		if (ne instanceof Class) {
			Class cls = (Class) ne;
			return base + (cls.isAbstract() ? 3 : 4);
		}
		if (ne instanceof Package)
			return base + 5;

		return base;
	}
}