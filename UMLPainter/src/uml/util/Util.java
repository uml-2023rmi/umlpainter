package uml.util;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;

public class Util {
	/**
	 * Является ли элемент UML-модели внешним по отношению модели?
	 * @param e - элемент UML-модели. 
	 * @return внешний или нет.
	 */
	public static boolean isExternal(Element e) {
		if (!(e instanceof Package))
			return e.hasKeyword("external");

		Package p = (Package) e;
		for (NamedElement owned : p.getOwnedMembers())
			if (!isExternal(owned))
				return false;

		return true;
	}

	/**
	 * Неизвестно чем является внешний элемент UML-модели: 
	 * классом, интерфейсом, перечислением?
	 * 
	 * @param e
	 *            - элемент UML-модели.
	 * @return неизвестного вида или нет.
	 */
	public static boolean isUnknown(Element e) {
		return e.hasKeyword("unknown");
	}
	
	/**
	 * Является ли пакет <b>mayBeExternal</b> внешним по отношению пакета <b>base</b>?
	 * @param base базовый пакет относительно которого выполняется проверка.
	 * @param mayBeExternal проверяемый пакето
	 * @return проверяемый пакет внешний или нет.
	 */
	static
	public boolean isPackageExternal(Package base, Package mayBeExternal) {
		String pQName = mayBeExternal.getQualifiedName();
		String baseQName = base.getQualifiedName();
		
		return !pQName.startsWith(baseQName);
	}
	
	/**
	 * Получить короткое квалифицированное имя элемента UML-модели.
	 * 
	 * @param ne
	 *            именованный элемент UML-модели.
	 * @return имя элемента UML-модели без имени модели в начале.
	 */
	static
	public String javaName(NamedElement ne) {
		String neName = ne.getQualifiedName();
		int k = neName.indexOf("::");
		return (k < 0) ? neName : neName.substring(k+2).replace("::", ".");
	}
}
