package uml.relations;

import static java.util.stream.Collectors.joining;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.PackageableElement;

import uml.util.UMLUtil;

public class DSM4Jars {
	static final List<Artifact> jars = new ArrayList<>();
	
	public static DSM dsm = new DSM(jars);

	public static void load(Model model) {
		Consumer<? super Element> action = e -> jars.add((Artifact) e);
		UMLUtil.walkAll(model, e -> true, e -> e instanceof Artifact, action);
		
		dsm = new DSM(jars);
		dsm.buildJars();
	}
	
	/**
	 * Для заданного классификатора выдать атрифакт (jar-файл)
	 * в котором он содержится.
	 * 
	 * TODO Жданов
	 * @param cls - классификатор.
	 * @return найденный артифакт или null
	 */
	public static Artifact getArtifact(Classifier cls) {
		return null;
	}

	public static void dump(String dumpPath) {
		new File(dumpPath).mkdirs();

		PrintStream ps = null;
		try {
			ps = new PrintStream(dumpPath + "/_jar-files.txt");
		} catch (FileNotFoundException e) {
			return;
		}

		String jarNames = jars
				.stream()
				.map(UMLUtil::getJavaName)
				.sorted()
				.collect( joining(",\n   ", "\n   ", "\n") );

		ps.format("jars: %s%n", jarNames);
		ps.close();
		
		dumpArtifactPackages(dumpPath);
	}
	
	public static void dumpArtifactPackages(String dumpPath) {
		new File(dumpPath).mkdirs();

		PrintStream ps = null;
		try {
			ps = new PrintStream(dumpPath + "/_jar-content.txt");
		} catch (FileNotFoundException e) {
			return;
		}
	
		for (Artifact jar : jars) {
			String content = jar.getManifestations()
			   .stream()
			   .map(Manifestation::getUtilizedElement)
		       .map(PackageableElement::getNearestPackage)
		       .distinct()
		       .map(UMLUtil::getJavaName)
		       .sorted()
		       .collect( joining(",\n   ", "\n   ", "\n") );

			ps.format("jar: %s %s%n", jar.getName(), content);
		}
		
		ps.close();
	}
}