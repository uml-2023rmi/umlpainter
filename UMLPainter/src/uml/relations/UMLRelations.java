package uml.relations;

import static java.util.stream.Collectors.joining;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.function.Predicate;

import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Model;

import uml.util.UMLUtil;

public class UMLRelations {
	@SuppressWarnings("serial")
	static public class UMLClassifiers extends HashSet<Classifier> {}
	
	@SuppressWarnings("serial")
	static public class UMLClasses extends HashSet<Class> {}

	public static final Map<Interface, UMLClasses> implementations = new HashMap<>();
	public static final Map<Classifier, UMLClassifiers> generalizations = new HashMap<>();

	public static void load(Model model) {
		loadGeneralizations(model);
		loadImplementations(model);
	}

	private static void loadImplementations(Model model) {
		Predicate<Element> vizitFilter = e -> true;
		Predicate<Element> actionFilter = e -> e instanceof InterfaceRealization;
		
		Consumer<? super Element> action = e -> {
			InterfaceRealization r = (InterfaceRealization) e;
			
			Interface implemented  = r.getContract();
			Class implementor = (Class) r.getImplementingClassifier();
			
			addImplementor(implemented, implementor);
		};
		
		UMLUtil.walkAll(model, vizitFilter, actionFilter, action);
	}

	private static void loadGeneralizations(Model model) {
		Predicate<Element> vizitFilter = e -> true;
		Predicate<Element> actionFilter = e -> e instanceof Generalization;
		
		Consumer<? super Element> action = e -> {
			Generalization g = (Generalization) e;
			
			Classifier parent = g.getGeneral();
			Classifier child  = g.getSpecific();
			
			addChild(parent, child);
		};
		
		UMLUtil.walkAll(model, vizitFilter, actionFilter, action);
	}

	private static void addChild(Classifier parent, Classifier child) {
		UMLClassifiers childs = generalizations.get(parent);
		if (childs == null) {
			childs = new UMLClassifiers();
			generalizations.put(parent, childs);
		}
		
		childs.add(child);
	}

	private static void addImplementor(Interface implemented, Class implementor) {
		UMLClasses implementors = implementations.get(implemented);
		if (implementors == null) {
			implementors = new UMLClasses();
			implementations.put(implemented, implementors);
		}
		
		implementors.add(implementor);
	}

	public static void dump(String dumpPath, int level) {
		dumpInheritance(dumpPath);
		dumpImplementations(dumpPath);
	}

	private static void dumpInheritance(String dumpPath) {
		new File(dumpPath).mkdirs();

		PrintStream ps = null;
		try {
			ps = new PrintStream(dumpPath + "/_inheritance.txt");
		} catch (FileNotFoundException e) {
			return;
		}
		
		for (Entry<Classifier, UMLClassifiers> g : generalizations.entrySet()) {
			Classifier parent = g.getKey();
			
			String parentName = UMLUtil.getJavaName(parent);
			String parentKind = "";
			if (parent instanceof Class)
				parentKind = "class";
			else
			if (parent instanceof Interface)
				parentKind = "interface";
			
			String childsNames = g.getValue()
				.stream()
				.map(UMLUtil::getJavaName)
				.sorted()
				.collect( joining(",\n   ", "\n   ", "\n") );
			
			ps.format("%s %s%n  isParentFor%s %n", parentKind, parentName, childsNames);
		}

		ps.close();
	}

	private static void dumpImplementations(String dumpPath) {
		new File(dumpPath).mkdirs();

		PrintStream ps = null;
		try {
			ps = new PrintStream(dumpPath + "/_implementations.txt");
		} catch (FileNotFoundException e) {
			return;
		}
		
		for (Entry<Interface, UMLClasses> g : implementations.entrySet()) {
			Interface implemented = g.getKey();
			
			String implementedName = UMLUtil.getJavaName(implemented);
			
			String implementorsNames = g.getValue()
				.stream()
				.map(UMLUtil::getJavaName)
				.sorted()
				.collect( joining(",\n   ", "\n   ", "\n") );
			
			ps.format("interface %s%n  isImplementedBy classes%s %n", implementedName, implementorsNames);
		}

		ps.close();
	}
}