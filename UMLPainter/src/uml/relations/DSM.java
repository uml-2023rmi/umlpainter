package uml.relations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;

import uml.util.UMLUtil;

/**
 * Матрица структурной зависимости между пакетами.
 * Зависимость возникает вследствии импорта элементов модели 
 * из одного пакета в другой пакет.
 * 
 * <a href="http://dsmweb.org">DSM - Dependency Structure Matrix</a>
 */
public class DSM {
	/**
	 * Размер квадратной матрицы.
	 */
	public final int size;
	
	/**
	 * Пакеты для который строится матрица.
	 */
	private final List<? extends Namespace> packages;

	/**
	 * Матрица ячейки которой содержат множество импортов
	 * из пакета в колонке матрицы в пакет в строке матрицы.
	 */
	public final DSMCell[][] matrix;

	@SuppressWarnings("serial")
	public static class DSMCell extends HashSet<ElementImport> {
		
		public List<PackageableElement> getImported() {
			return stream()
			    .map(ElementImport::getImportedElement)
			    .distinct() // Убрали повторяющиеся элементы.
			    .collect( Collectors.toList());
		}

		public long getAmount() {
			return stream()
			    .map(ElementImport::getImportedElement)
			    .distinct() // Убрали повторяющиеся элементы.
			    .count();
		}
	}

	/**
	 * Создать матрицу структурной зависимости между пакетами.
	 * 
	 * @param packages пакеты для которых создается матрица.
	 */
	public DSM(List<? extends Namespace> packages) {
		this.packages = new ArrayList<>(packages);
		size = packages.size();

		matrix = new DSMCell[size][size];
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				matrix[i][j] = new DSMCell();

		build();
	}
	
	public List<? extends Namespace> getElements() {
		return packages;
	}

	/**
	 * Вычислить значения ячеек матрицы.
	 */
	private void build() {
		if (size == 0)
			return;

		Predicate<Element> vizitFilter = e -> true;
		Predicate<Element> actionFilter = e -> e instanceof ElementImport;
		Consumer<? super Element> action = e -> {
			// Действие для найденного в модели импорта.
			ElementImport elementImport = (ElementImport) e;

			// Кто импортирует и что импортируется.
			final Namespace importer = elementImport.getImportingNamespace();
			final PackageableElement imported = elementImport.getImportedElement();
			
			// Пакеты для элементов которых выполняется импорт.
			final Package importerPackage = importer.getNearestPackage();
			final Package exporterPackage = imported.getNearestPackage();
			
			// Пакеты для которых строится матрица.
			Namespace importerOwner = findOwner(importerPackage);
			Namespace exporterOwner = findOwner(exporterPackage);
			if ((importerOwner == null) || (exporterOwner == null))
				return;
			
			DSMCell imports = getCell(importerOwner, exporterOwner);
			imports.add(elementImport);
		};

		Model model = packages.get(0).getModel();
		UMLUtil.walkAll(model, vizitFilter, actionFilter, action);
	}
	
	/**
	 * Вычислить значения ячеек матрицы.
	 */
	public void buildJars() {
		if (size == 0)
			return;

		Predicate<Element> vizitFilter = e -> true;
		Predicate<Element> actionFilter = e -> e instanceof ElementImport;
		Consumer<? super Element> action = e -> {
			// Действие для найденного в модели импорта.
			ElementImport elementImport = (ElementImport) e;

			// Кто импортирует и что импортируется.
			final Namespace importer = elementImport.getImportingNamespace();
			final PackageableElement imported = elementImport.getImportedElement();
			
			// Артифакты для которых строится матрица.
			Artifact importerOwner = DSM4Jars.getArtifact((Classifier) importer);
			Artifact exporterOwner = DSM4Jars.getArtifact((Classifier) imported);
			if ((importerOwner == null) || (exporterOwner == null))
				return;
			
			DSMCell imports = getCell(importerOwner, exporterOwner);
			imports.add(elementImport);
		};

		Model model = packages.get(0).getModel();
		UMLUtil.walkAll(model, vizitFilter, actionFilter, action);
	}

	/**
	 * Выдать все импорты элементов из одного пакета в другой пакет.
	 * @param importer пакет который импортирует элементы модели.
	 * @param exporter пакет из которого импортируются элементы модели.
	 * @return список импортов.
	 */
	public <T extends Namespace> DSMCell getCell(T importer, T exporter) {
		int kExporter = packages.indexOf(exporter);

		if (kExporter == -1) {
			System.out.format("exporter not found %s %n  in ", UMLUtil.getShortName(exporter));
			packages.forEach(p -> System.out.format(" %s", UMLUtil.getShortName(p)) );
			System.out.println();
			return null;
		}

		int kImporter = packages.indexOf(importer);
		
		if (kImporter == -1) {
			System.out.format("importer not found %s %n  in ", UMLUtil.getShortName(importer));
			packages.forEach(p -> System.out.format(" %s", UMLUtil.getShortName(p)) );
			System.out.println();
			return null;
		}

		return matrix[kImporter][kExporter];
	}

	public DSMCell getCell(int cellV, int cellH) {
		return matrix[cellV][cellH];
	}

	/**
	 * Выдать количество импортируемых элементов.
	 * @param importer - пакет в который импортируются элементы.
	 * @param exporter - пакет из которого импортируютс элементы.
	 * @return количество импортируемых элементов.
	 */
	public long getCellAmount(Package importer, Package exporter) {
		return getCell(importer, exporter).getAmount();
	}

	/**
	 * Среди пакетов для которых строится матрица DSM 
	 * найти пакет содержащий заданный пакет
	 * или совпадающий с заданным пакетом.
	 * @param searchPackage пакет для которого ищем собственника.
	 * @return найденный пакет-собственник или null
	 */
	private <T extends Namespace> Namespace findOwner(T searchPackage) {
		Optional<? extends Namespace> found = packages.stream()
		    .filter(p -> !UMLUtil.isOutside(searchPackage, p))
		    .findAny();
		 return found.orElse(null);
	}
	
	/**
	 * Если ли циклическая зависимость между двумя пакетами?
	 * @param row - первый пакет
	 * @param col - второй пакет
	 * @return есть ли циклическая зависимость.
	 */
	public boolean isCircle(Package row, Package col) {
		// Пока отслеживаем простой цикл из двух элементов.
		long v1 = getCellAmount(row, col);
		long v2 = getCellAmount(col, row);
		
		return (v1 != 0) && (v2 != 0);
	}
}