package uml.relations;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;

import uml.util.UMLUtil;

public class PackageButterfly {
	@SuppressWarnings("serial")
	static
	public class Metrics extends TreeMap<String, Float> {
		{
			// NCP (Number of Classes in a Package).
			put("NCP", 0f);
			
			// ASP (Number of Ancestor State as Provider/Client).
			put("ASP", 0f);
			put("ASC", 0f);
			
			// RTP (Number of Class References To/From Other Packages).
			put("RTP", 0f);
			put("RFP", 0f);
	
			// EIP (Number of External Inheritance as Provider/Client).
			put("EIP", 0f);
			put("EIC", 0f);
		}
	}
	
	@SuppressWarnings("serial")
	static
	public class PackageMetrics extends HashMap<Package, Metrics> {
		
	}
	
	static 
	public final PackageMetrics packageMetrics = new PackageMetrics();
	
	public static void load(Model model) {
		Predicate<Package> vizitFilter = e -> true;
		Predicate<NamedElement> actionFilter = e -> e instanceof Package;
		
		Consumer<? super NamedElement> action = e -> {
			Package p = (Package) e;
			
			// TODO Белоусова. Вычислить метрики EIP/EIC
			packageMetrics.put(p, new Metrics());
		};
		
		UMLUtil.walkPackage(model, vizitFilter, actionFilter, action);
	}
	
	public static void dump(String dumpPath) {
		new File(dumpPath).mkdirs();

		PrintStream ps = null;
		try {
			ps = new PrintStream(dumpPath + "/_package-metrics.txt");
		} catch (FileNotFoundException e) {
			return;
		}
		
		for (Entry<Package, Metrics> g : packageMetrics.entrySet()) {
			Package p = g.getKey();
			
			String pName = UMLUtil.getShortName(p).replace("::", ".");
			
			String metrics = g.getValue().entrySet()
				.stream()
				.map(m -> m.getKey() + "=" + m.getValue())
				.sorted()
				.collect( Collectors.joining(", ") );
			
			ps.format("Package %s has metrics:%n   %s %n%n", pName, metrics);
		}

		ps.close();
	}
}