package uml.diagram;

public enum Direction {
  left(-1.0, 0.0),
  right(1.0, 0.0),
  down(0.0, 1.0),
  up(0.0, -1.0);

  public final double dx;
    public final double dy;

  Direction(double dx, double dy) {
    this.dx = dx;
    this.dy = dy;
  }

  public Direction back() {
    switch (this) {
      case up:
        return down;
      case down:
        return up;
      case left:
        return right;
      case right:
        return left;
    }
    return null;
  }

  public Direction clock() {
    switch (this) {
      case up:
        return right;
      case down:
        return left;
      case left:
        return up;
      case right:
        return down;
    }
    return null;
  }
}
