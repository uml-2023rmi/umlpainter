package uml.diagram.routers;

import uml.dd.Point;
import uml.diagram.Edge;

import java.util.ArrayList;

public class RouterVH extends Router {
  private Edge edge;

  @Override
  public void route() {
    Point sourceP = edge.getWaypoints().get(0);
    Point middleP = edge.getWaypoints().get(1);
    Point targetP = edge.getWaypoints().get(2);
    middleP.x = sourceP.x;
    middleP.y = targetP.y;
  }

  @Override
  public void setEdge(Edge edge) {
    this.edge = edge;
    this.edge.setWaypoints(new ArrayList<>());
    this.edge.getWaypoints().add(new Point(0, 0));
    this.edge.getWaypoints().add(new Point(0, 0));
    this.edge.getWaypoints().add(new Point(0, 0));
  }
}
