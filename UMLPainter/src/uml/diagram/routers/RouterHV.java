package uml.diagram.routers;

import uml.dd.Point;
import uml.diagram.Edge;

import java.util.ArrayList;

public class RouterHV extends Router {
  private Edge edge;

  public void setEdge(Edge edge) {
    this.edge = edge;
    this.edge.setWaypoints(new ArrayList<>());
    this.edge.getWaypoints().add(new Point(0, 0));
    this.edge.getWaypoints().add(new Point(0, 0));
    this.edge.getWaypoints().add(new Point(0, 0));
  }

  @Override
  public void route() {
    Point sourceP = edge.getWaypoints().get(0);
    Point middleP = edge.getWaypoints().get(1);
    Point targetP = edge.getWaypoints().get(2);

    middleP.x = targetP.x;
    middleP.y = sourceP.y;
  }
}
