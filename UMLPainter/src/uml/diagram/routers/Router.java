package uml.diagram.routers;

import uml.diagram.Edge;

abstract public class Router {
  public abstract void setEdge(Edge edge);

  public abstract void route();

  static public class NoRouter extends Router {
    @Override
    public void setEdge(Edge edge) {
    }

    @Override
    public void route() {
    }
  }
}
