package uml.diagram.routers;

import uml.dd.Point;
import uml.diagram.Edge;

import java.util.ArrayList;

import static java.lang.Math.signum;

public class RouterVHV extends Router {
  private Edge edge;
  private int shift;
  private final double k;

  public RouterVHV(int shift) {
    this.shift = shift;
    k = -1;
  }

  public RouterVHV() {
    k = 0.5;
  }

  @Override
  public void route() {
    Point sourceP = edge.getWaypoints().get(0);
    Point PointH1 = edge.getWaypoints().get(1);
    Point PointH2 = edge.getWaypoints().get(2);
    Point targetP = edge.getWaypoints().get(3);

    double y = k > 0.0 ? k * (sourceP.y + targetP.y)
            : sourceP.y + signum(targetP.y - sourceP.y) * shift;

    PointH1.x = sourceP.x;
    PointH1.y = y;
    PointH2.x = targetP.x;
    PointH2.y = y;
  }

  @Override
  public void setEdge(Edge edge) {
    this.edge = edge;
    this.edge.setWaypoints(new ArrayList<>());
    this.edge.getWaypoints().add(new Point(0, 0));
    this.edge.getWaypoints().add(new Point(0, 0));
    this.edge.getWaypoints().add(new Point(0, 0));
    this.edge.getWaypoints().add(new Point(0, 0));
  }
}
