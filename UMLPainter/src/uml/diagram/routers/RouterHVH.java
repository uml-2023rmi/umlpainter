package uml.diagram.routers;

import uml.dd.Point;
import uml.diagram.Edge;

import java.util.ArrayList;

import static java.lang.Math.signum;

public class RouterHVH extends Router {
  private Edge edge;
  private int shift;
  private final double k;

  public RouterHVH(int shift) {
    this.shift = shift;
    k = -1;
  }

  public RouterHVH() {
    k = 0.5;
  }

  @Override
  public void route() {
    Point sourceP = edge.getWaypoints().get(0);
    Point PointV1 = edge.getWaypoints().get(1);
    Point PointV2 = edge.getWaypoints().get(2);
    Point targetP = edge.getWaypoints().get(3);

    double x = k > 0.0 ? k * (sourceP.x + targetP.x)
            : sourceP.x + signum(targetP.x - sourceP.x) * shift;

    PointV1.x = x;
    PointV1.y = sourceP.y;
    PointV2.x = x;
    PointV2.y = targetP.y;
  }

  @Override
  public void setEdge(Edge edge) {
    this.edge = edge;
    this.edge.setWaypoints(new ArrayList<>());
    this.edge.getWaypoints().add(new Point(0, 0));
    this.edge.getWaypoints().add(new Point(0, 0));
    this.edge.getWaypoints().add(new Point(0, 0));
    this.edge.getWaypoints().add(new Point(0, 0));
  }
}
