package uml.diagram;

public class Painter {
  private String penColor = "";
  private String brushColor = "";
  private String pencilColor = "";
  private String promptText = "";

  public Painter pen(String color) {
    penColor = color;
    return this;
  }

  public Painter brush(String color) {
    brushColor = color;
    return this;
  }

  public Painter pencil(String color) {
    pencilColor = color;
    return this;
  }

  public Painter prompt(String promptText) {
    this.promptText = promptText;
    return this;
  }

  public void paint(DiagramElement element) {
    if (!penColor.isEmpty())
      element.setColor(penColor);

    if (!brushColor.isEmpty())
      element.setBackground(brushColor);

    if (!pencilColor.isEmpty())
      element.setTextColor(pencilColor);
  }
}
