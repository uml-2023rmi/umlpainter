package uml.diagram.graphical;

import uml.dd.Point;

public interface Circle extends GraphicalElement {
  Point getCenter();
  void setCenter(Point center);

  void setRadius(double r);
  double getRadius();
}
