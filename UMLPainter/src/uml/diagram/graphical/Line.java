package uml.diagram.graphical;

import uml.dd.Point;

public interface Line extends GraphicalElement {
  void setStart(Point start);

  Point getStart();

  void setEnd(Point end);

  Point getEnd();
}
