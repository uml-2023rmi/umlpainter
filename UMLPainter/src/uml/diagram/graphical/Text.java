package uml.diagram.graphical;

import uml.dd.AlignmentKind;

public interface Text extends GraphicalElement {
  void setBold();

  void setUnderline();

  void setItalic(boolean isItalic);

  void setNormalText();

  void setColor(String color);

  void setBackground(String color);

  void setX(double x);

  void setY(double y);

  void setWidth(double width);

  double getWidth();

  void setHeight(double height);

  double getHeight();

  void setData(String data);

  String getData();

  AlignmentKind getAlignment();

  void setAlignment(AlignmentKind alignment);

  void setTooltip(String text);
}
