package uml.diagram.graphical;

import uml.dd.Point;

import java.util.List;

public interface Polygon extends GraphicalElement {
  List<Point> getPoints();
  void setPoints(List<Point> points);
}
