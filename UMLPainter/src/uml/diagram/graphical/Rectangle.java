package uml.diagram.graphical;

import uml.dd.Bounds;

public interface Rectangle extends GraphicalElement {
  void setX(double x);
  void setY(double y);

  void setWidth(double width);
  void setHeight(double height);

  void setBounds(Bounds bounds);
  Bounds getBounds();

  void setCornerRadius(double radius);
  double getCornerRadius();

  void setColor(String color);
  String getColor();

  void setBackground(String background);
  String getBackground();
}
