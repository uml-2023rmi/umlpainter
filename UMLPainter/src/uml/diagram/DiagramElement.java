package uml.diagram;

import org.eclipse.uml2.uml.Element;
import uml.diagram.layout.Layouter;

import java.util.ArrayList;

public interface DiagramElement {
  void setColor(String color);

  void setTextColor(String color);

  void setBackground(String color);

  void setTooltip(String tooltip);

  Layouter getLayouter();

  void setLayouter(Layouter layouter);

  void layout();

  ArrayList<DiagramElement> getOwnedElements();

  void add(DiagramElement owned);

  DiagramElement getOwningElement();

  void setOwningElement(DiagramElement owningElement);

  void setModelElement(Element e);
  Element getModelElement();
}
