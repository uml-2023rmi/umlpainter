package uml.diagram;

import uml.dd.Bounds;

public interface Shape extends DiagramElement {
  Bounds getBounds();

  void setX(double x);

  void setY(double y);

  double getX();

  double getY();

  double getRight();

  double getBottom();

  double getCenterX();

  double getCenterY();

  double getLeft();

  double getTop();

  void setWidth(double width);

  void setHeight(double height);

  double getWidth();

  double getHeight();

  String toString();
}
