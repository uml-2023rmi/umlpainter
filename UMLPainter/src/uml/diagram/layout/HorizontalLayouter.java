package uml.diagram.layout;

import uml.diagram.DiagramElement;
import uml.diagram.Shape;

import java.util.ArrayList;

public class HorizontalLayouter implements Layouter {
  private final Shape shape;
  private final int padding;
  private final int spacing;

  public HorizontalLayouter(Shape shape, int padding, int spacing) {
    this.shape = shape;
    this.padding = padding;
    this.spacing = spacing;
  }

  @Override
  public void layout() {
    ArrayList<DiagramElement> owned = shape.getOwnedElements();

    if (owned.isEmpty()) {
      shape.setWidth(0);
      shape.setHeight(0);
      return;
    }

    double maxH = owned
            .stream()
            .filter(e -> e instanceof Shape)
            .map(e -> (Shape) e)
            .map(Shape::getHeight)
            .reduce(0.0, Math::max);

    owned.stream()
            .filter(e -> e instanceof Shape)
            .map(e -> (Shape) e)
            .forEach(n -> n.setHeight(maxH));

    shape.setWidth(padding + maxH + padding);

    double sumW = owned
            .stream()
            .filter(e -> e instanceof Shape)
            .map(e -> (Shape) e)
            .map(Shape::getWidth)
            .reduce(0.0, Double::sum);

    if (!owned.isEmpty())
      sumW += (owned.size() - 1) * spacing;

    shape.setWidth(padding + sumW + padding);

    double X = shape.getX() + padding;
    double Y = shape.getY() + padding;

    for (DiagramElement n : owned) {
      if (!(n instanceof Shape))
        continue;

      Shape shape = (Shape) n;
      shape.setX(X);
      shape.setY(Y);
      X += (shape.getWidth() + spacing);
    }
  }
}
