package uml.diagram.layout;

public interface Layouter {
  class NoneLayouter implements Layouter {
    @Override
    public void layout() {
    }
  }

  void layout();
}
