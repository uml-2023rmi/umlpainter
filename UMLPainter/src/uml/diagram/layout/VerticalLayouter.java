package uml.diagram.layout;

import uml.diagram.DiagramElement;
import uml.diagram.Shape;

import java.util.List;

public class VerticalLayouter implements Layouter {
  private final Shape shape;
  private final double padding;
  private final double spacing;
  public int startElement = 0;

  public VerticalLayouter(Shape shape, double padding, double spacing, boolean sameSize) {
    this.shape = shape;
    this.padding = padding;
    this.spacing = spacing;
  }

  @Override
  public void layout() {
    List<DiagramElement> owned = shape.getOwnedElements();
    if (startElement == 1 && owned.size() == 1)
      return;

    List<DiagramElement> used = owned.subList(startElement, owned.size());

    if (used.isEmpty()) {
      shape.setWidth(0);
      shape.setHeight(0);
      return;
    }

    double maxW = used
            .stream()
            .filter(e -> e instanceof Shape)
            .map(e -> (Shape) e)
            .map(Shape::getWidth)
            .reduce(0.0, Math::max);

    shape.setWidth(padding + maxW + padding);

    double sumH = used
            .stream()
            .filter(e -> e instanceof Shape)
            .map(e -> (Shape) e)
            .map(Shape::getHeight)
            .reduce(0.0, Double::sum);

    if (!used.isEmpty())
      sumH += (used.size() - 1) * spacing;

    shape.setHeight(padding + sumH + padding);

    double X = shape.getX() + padding;
    double Y = shape.getY() + padding;

    for (DiagramElement n : used) {
      if (!(n instanceof Shape))
        continue;
      Shape nShape = (Shape) n;

      nShape.setX(X);
      nShape.setY(Y);
      Y += (nShape.getHeight() + spacing);
    }

    used.stream()
            .filter(e -> e instanceof Shape)
            .map(e -> (Shape) e)
            .forEach(n -> n.setWidth(maxW));
  }
}