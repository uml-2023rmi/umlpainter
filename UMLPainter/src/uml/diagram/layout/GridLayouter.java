package uml.diagram.layout;

import uml.diagram.DiagramElement;
import uml.diagram.Shape;

import static java.lang.Math.max;

public class GridLayouter implements Layouter {
  private final int nColumns;
  private final Shape shape;

  public GridLayouter(Shape shape, int nColumns) {
    this.shape = shape;
    this.nColumns = nColumns;
  }

  @Override
  public void layout() {
    double padding = 2.0;
    double spacing = 2.0;

    double x = shape.getX() + padding;
    double y = shape.getY() + padding;

    int k = 0;
    boolean isFirst = true;

    for (DiagramElement de : shape.getOwnedElements()) {
      if (isFirst) {
        isFirst = false;
        continue;
      }

      Shape nested = (Shape) de;
      nested.setX(x);
      nested.setY(y);

      if ((k+1) % nColumns == 0) {
        // Последняя колонка в сетке.
        // Переход на следующую строку.
        x = shape.getX() + padding;
        y = nested.getBottom() + padding;
      } else x = nested.getRight() + spacing;

      shape.setWidth(max(shape.getWidth(), nested.getRight() + padding - shape.getX()));
      shape.setHeight(max(shape.getHeight(), nested.getBottom() - shape.getY()));

      k++;
    }
    shape.setHeight(shape.getHeight() + padding);
  }
}

