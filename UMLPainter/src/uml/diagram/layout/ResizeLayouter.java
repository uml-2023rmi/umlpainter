package uml.diagram.layout;

import uml.diagram.Shape;

public class ResizeLayouter implements Layouter {
  private final Shape shape;
  private final double padding;

  public ResizeLayouter(Shape shape, double padding) {
    this.shape = shape;
    this.padding = padding;
  }

  @Override
  public void layout() {
    double maxW = shape.getOwnedElements()
            .stream()
            .filter(e -> e instanceof Shape)
            .map(e -> (Shape) e)
            .map(Shape::getRight)
            .reduce(Math::max)
            .orElse(padding);
    shape.setWidth(maxW + padding);

    double maxH = shape.getOwnedElements()
            .stream()
            .filter(e -> e instanceof Shape)
            .map(e -> (Shape) e)
            .map(Shape::getBottom)
            .reduce(Math::max)
            .orElse(padding);
    shape.setHeight(maxH + padding);
  }
}
