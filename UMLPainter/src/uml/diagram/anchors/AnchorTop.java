package uml.diagram.anchors;

import uml.dd.Point;

public class AnchorTop extends Anchor {
  private int shift;
  private double k;

  public AnchorTop(int shift) {
    this.shift = shift;
  }

  public AnchorTop() {
    k = 0.5;
  }

  public AnchorTop(double k) {
    this.k = k;
  }

  @Override
  public Point get() {
    double delta = k > 0.0 ? k * shape.getWidth() : shift;

    return new Point(shape.getLeft() + delta, shape.getTop());
  }
}
