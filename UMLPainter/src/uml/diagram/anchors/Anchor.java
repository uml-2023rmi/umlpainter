package uml.diagram.anchors;

import uml.dd.Point;
import uml.diagram.Shape;

abstract public class Anchor {
  protected Shape shape;

  public void setShape(Shape shape) {
    this.shape = shape;
  }

  abstract public Point get();
}