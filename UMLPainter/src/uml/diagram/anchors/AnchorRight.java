package uml.diagram.anchors;

import uml.dd.Point;

public class AnchorRight extends Anchor {
  private int shift;
  private final double k;

  public AnchorRight(int shift) {
    this.shift = shift;
    k = -1;
  }

  public AnchorRight() {
    k = 0.5;
  }

  public AnchorRight(double k) {
    this.k = k;
  }

  @Override
  public Point get() {
    double delta = k > 0.0 ? 0.5 * shape.getHeight() : shift;
    return new Point(shape.getRight(), shape.getTop() + delta);
  }
}
