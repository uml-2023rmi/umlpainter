package uml.diagram.anchors;

import uml.dd.Point;

public class AnchorLeft extends Anchor {
  private int shift;
  private double k;

  public AnchorLeft(int shift) {
    this.shift = shift;
  }

  public AnchorLeft() {
    k = 0.5;
  }

  public AnchorLeft(double k) {
    this.k = k;
  }

  @Override
  public Point get() {
    double delta = k > 0.0 ? 0.5 * shape.getHeight() : shift;
    return new Point(shape.getLeft(), shape.getTop() + delta);
  }
}
