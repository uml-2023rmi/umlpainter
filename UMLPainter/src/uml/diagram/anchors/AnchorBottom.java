package uml.diagram.anchors;

import uml.dd.Point;

public class AnchorBottom extends Anchor {
  final double k;
  private int shift = 0;

  public AnchorBottom(int shift) {
    this.shift = shift;
    k = -1;
  }

  public AnchorBottom() {
    k = 0.5;
  }

  public AnchorBottom(double k) {
    this.k = k;
  }

  @Override
  public Point get() {
    double delta = k > 0.0 ? k * shape.getWidth() : shift;
    return new Point(shape.getLeft() + delta, shape.getBottom());
  }
}
