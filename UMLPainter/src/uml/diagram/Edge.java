package uml.diagram;

import uml.dd.Point;
import uml.diagram.anchors.Anchor;
import uml.diagram.routers.Router;

import java.util.List;

public interface Edge extends DiagramElement {
  void setSourceAnchor(Anchor anchor);

  void setTargetAnchor(Anchor anchor);

  void setRouter(Router router);

  void setRouterHV();

  void setRouterVH();

  void setRouterHVH();

  void setRouterVHV();

  void layout();

  List<Point> getWaypoints();

  void setWaypoints(List<Point> waypoints);

  Shape getSource();

  void setSource(Shape source);

  Shape getTarget();

  void setTarget(Shape target);

  String toString();
}
