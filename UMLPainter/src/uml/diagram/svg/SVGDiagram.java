package uml.diagram.svg;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import uml.diagram.DiagramElement;
import uml.diagram.layout.ResizeLayouter;
import uml.report.ReportUtil;

public class SVGDiagram extends SVGSection implements uml.diagram.Diagram {
  private static Element svgTag;

  private String name;

  private static Document document = DocumentFactory.getInstance().createDocument();

  {
    setLayouter(new ResizeLayouter(this, 5));
    setColor("Maroon");
    setBackground("LightYellow");
  }

  public SVGDiagram(Element owner) {
    super(svgTag = SVG.svgHead(owner));
  }

  public SVGDiagram(String name) {
    super(svgTag = SVG.svgHead(document = DocumentFactory.getInstance().createDocument()));

    document.addDocType("svg",
            "-//W3C//DTD SVG 1.1//EN",
            "https://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd");

    this.setName(name);
  }

  @Override
  public void layout() {
    getLayouter().layout();

    getOwnedElements()
            .stream()
            .filter(e -> e instanceof SVGEdge)
            .forEach(DiagramElement::layout);

    svgTag.addAttribute("width", "" + (int) getWidth());
    svgTag.addAttribute("height", "" + (int) getHeight());

//		svgTag.addAttribute("width",  "500");
//		svgTag.addAttribute("height", "500");
  }

  @Override
  public void save(String dir, String file) {
    layout();
    ReportUtil.save(document, dir, file);
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }
}
