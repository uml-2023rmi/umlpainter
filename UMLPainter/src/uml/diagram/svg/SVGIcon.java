package uml.diagram.svg;

import org.dom4j.Element;
import org.dom4j.QName;
import uml.dd.Point;

public class SVGIcon extends SVGShape {

  private final Element title;

  public SVGIcon(SVGDiagramElement owner, String iconName, double x, double y) {
    super(owner.tag.addElement("use"));

    tag.addAttribute(new QName("href", SVG.nsXLink), "#" + iconName);
    title = tag.addElement("title");
    title.addText(" ");

    setX(x);
    setY(y);
    setWidth(16);
    setHeight(16);
  }

  public SVGIcon(SVGDiagramElement owner, String iconName, Point p) { this(owner, iconName, p.x, p.y); }

  @Override
  public void setTooltip(String tooltip) {
    title.addText(tooltip);
  }

  public void setX(double x) {
    super.setX(x);
    tag.addAttribute("x", "" + (int) x);
  }

  public void setY(double y) {
    super.setY(y);
    tag.addAttribute("y", "" + (int) y);
  }

  public void setWidth(double width) {
    super.setWidth(width);
    tag.addAttribute("width", "" + (int) width + "px");
  }

  public void setHeight(double height) {
    super.setHeight(height);
    tag.addAttribute("height", "" + (int) height + "px");
  }
}
