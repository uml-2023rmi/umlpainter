package uml.diagram.svg.adorns;

import org.dom4j.Element;
import uml.diagram.svg.SVGDiagramElement;

import static java.lang.String.format;

public class ComponentIcon extends SVGDiagramElement {
  final static int size = 32;

  public ComponentIcon(Element owner) {
    super(owner.addElement("symbol"));

    int w4 = size / 4;
    int h4 = size / 4;
    int dy = 5;

    tag.addAttribute("id", "componentIcon");
    tag.addAttribute("viewBox", format("0 0 %d %d", size, size));

    String bigRect = format("M %d 1 h %d v %d h -%d z ",
            w4, size - w4, size - 2, size - w4);
    String smRect1 = format("M 1 %d h %d v %d h -%d z ",
            dy, 2 * w4, h4, 2 * w4);
    String smRect2 = format("M 1 %d h %d v %d h -%d z ",
            2 * dy + h4, 2 * w4, h4, 2 * w4);

    Element bigRectPath = tag.addElement("path");
    bigRectPath.addAttribute("d", bigRect);
    bigRectPath.addAttribute("stroke-width", "1");

    Element smRect1Path = tag.addElement("path");
    smRect1Path.addAttribute("d", smRect1);
    smRect1Path.addAttribute("stroke-width", "1");

    Element smRect2Path = tag.addElement("path");
    smRect2Path.addAttribute("d", smRect2);
    smRect2Path.addAttribute("stroke-width", "1");
  }
}
