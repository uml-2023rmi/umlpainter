package uml.diagram.svg.adorns;

import org.dom4j.Element;
import uml.diagram.svg.SVGDiagramElement;

import static java.lang.String.format;

public class PackageIcon extends SVGDiagramElement {
  final static int size = 32;

  public PackageIcon(Element owner) {
    super(owner.addElement("symbol"));

    tag.addAttribute("id", "packageIcon");
    tag.addAttribute("viewBox", format("0 0 %d %d", size, size));

    Element symbolPath;
    String coordinates;

    coordinates = format("M 1 1 h %d v %d h -%d v -%d z",
            size - 2, size - 2, size - 2, size - 2);

    symbolPath = tag.addElement("path");
    symbolPath.addAttribute("d", coordinates);
    symbolPath.addAttribute("stroke-width", "1");

    coordinates = format("M 1 %d h %d l %d %d",
            size / 3, 2 * size / 3,
            size / 3-1, -size / 3+1);

    symbolPath = tag.addElement("path");
    symbolPath.addAttribute("d", coordinates);
    symbolPath.addAttribute("stroke-width", "1");
  }
}
