package uml.diagram.svg.adorns;

import org.dom4j.Element;
import uml.diagram.svg.SVGDiagramElement;

import static java.lang.String.format;

public class InterfaceIcon extends SVGDiagramElement {
  final static int size = 32;

  public InterfaceIcon(Element owner) {
    super(owner.addElement("symbol"));

    tag.addAttribute("id", "interfaceIcon");
    tag.addAttribute("viewBox", format("0 0 %d %d", size, size));

    Element symbolPath = tag.addElement("circle");
    symbolPath.addAttribute("cx", "" + size / 2);
    symbolPath.addAttribute("cy", "" + size / 2);
    symbolPath.addAttribute("r", "" + (size / 2 - 2));
    symbolPath.addAttribute("stroke-width", "1");
  }
}