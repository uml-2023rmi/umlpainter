package uml.diagram.svg.adorns;

import org.dom4j.Element;
import uml.diagram.svg.SVGDiagramElement;

import static java.lang.String.format;

public class EnumIcon extends SVGDiagramElement {
  final static int size = 32;

  public EnumIcon(Element owner) {
    super(owner.addElement("symbol"));

    tag.addAttribute("id", "enumIcon");
    tag.addAttribute("viewBox", format("0 0 %d %d", size, size));

    String coordinates;
    Element symbolPath;

    coordinates = format("M 1 1 h %d v %d h -%d v -%d z",
            size - 2, size - 2, size - 2, size - 2);

    symbolPath = tag.addElement("path");
    symbolPath.addAttribute("d", coordinates);
    symbolPath.addAttribute("stroke-width", "1");

    coordinates = format("M 1 %d h %d", size / 3, size - 2);

    symbolPath = tag.addElement("path");
    symbolPath.addAttribute("d", coordinates);
    symbolPath.addAttribute("stroke-width", "1");
  }
}
