package uml.diagram.svg.adorns;

import org.dom4j.Element;
import uml.diagram.svg.SVGDiagramElement;

import static java.lang.String.format;

public class ClassIcon extends SVGDiagramElement {
  final static int size = 32;

  public ClassIcon(Element owner) {
    super(owner.addElement("symbol"));

    tag.addAttribute("id", "classIcon");
    tag.addAttribute("viewBox", format("0 0 %d %d", size, size));

    String coordinates = format("M 1 1 h %d v %d h -%d v -%d z M 1 %d h %d M 1 %d h %d",
            size - 2, size - 2, size - 2, size - 2,
            size / 3, size - 2,
            2 * size / 3, size - 2
    );
    Element symbolPath = tag.addElement("path");
    symbolPath.addAttribute("d", coordinates);
    symbolPath.addAttribute("stroke-width", "1");
  }
}
