package uml.diagram.svg.adorns;

import org.dom4j.Element;
import uml.diagram.svg.SVGDiagramElement;

public class ArrowAdorn extends SVGDiagramElement {
  public ArrowAdorn(Element owner) {
    super(owner.addElement("marker"));

    tag.addAttribute("id", "arrowAdorn");
    tag.addAttribute("viewBox", "0 0 16 16");
    tag.addAttribute("refX", "16");
    tag.addAttribute("refY", "8");
//		svg.addAttribute("markerUnits", "strokeWidth");
    tag.addAttribute("markerWidth", "16");
    tag.addAttribute("markerHeight", "16");
    tag.addAttribute("orient", "auto");
    tag.addAttribute("stroke-width", "1");

    Element markerPath = tag.addElement("path");
    markerPath.addAttribute("stroke", "navy");
    markerPath.addAttribute("fill", "none");
    markerPath.addAttribute("d", "M 0 0 L 16 8 L 0 16");
  }
}
