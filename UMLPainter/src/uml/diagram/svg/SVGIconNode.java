package uml.diagram.svg;

import uml.diagram.graphical.Text;
import uml.diagram.layout.HorizontalLayouter;
import uml.diagram.svg.graphical.SVGText;

public class SVGIconNode extends SVGShape {
  public Text nodeText;
  public SVGIcon nodeIcon;

  private static final int imageWidth = 18;
  private static final int imageHeight = 18;
  private String text = "";

  public SVGIconNode(SVGDiagramElement owner, String iconName, String text, double x, double y) {
    super(owner.tag.addElement("g"));

    initNode(x, y, text, iconName);

    owner.add(this);
    layout();
  }

  private void initNode(double x, double y, String text, String iconName) {
    this.text = text;

    setLayouter(new HorizontalLayouter(this, 0, 5));

    nodeIcon = new SVGIcon(this, iconName, x, y);
    nodeIcon.setWidth(imageWidth);
    nodeIcon.setHeight(imageHeight);

    nodeText = new SVGText(this, text, x + 1 + imageWidth, y + imageHeight);
    nodeText.setBold();

    setX(x);
    setY(y);
    setHeight(imageHeight);
    setWidth(nodeIcon.getWidth() + 1 + nodeText.getWidth());

    setColor("black");
    setTextColor("black");
    setBackground("white");
  }

  public void layout() {
    nodeIcon.setX(getX());
    nodeIcon.setY(getY());

    nodeText.setX(nodeIcon.getRight() + 1);
    nodeText.setY(getY());
    nodeText.setHeight(imageHeight);

    setHeight(imageHeight);
    setWidth(nodeIcon.getWidth() + 1 + nodeText.getWidth());
  }

  public void setX(double x) {
    super.setX(x);
    layout();
  }

  public void setY(double y) {
    super.setY(y);
    layout();
  }

  public double getWidth() {
    return SVG.IWIDTH + nodeText.getWidth();
  }

  public double getHeight() {
    return SVG.IHEIGHT + 1;
  }

  public void setColor(String color) {
    nodeIcon.setColor(color);
  }

  public void setBackground(String color) {
    nodeIcon.setBackground(color);
  }

  public void setTextColor(String color) {
    nodeText.setColor(color);
  }

  public void setNormalText() {
    nodeText.setNormalText();
  }

  public void setItalic(boolean isItalic) {
    nodeText.setItalic(isItalic);
  }
}
