package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;

public class EnumerationNode extends ClassifierNode {

  public EnumerationNode(SVGDiagramElement owner, String enumName, double x, double y) {
    super(owner, x, y);

    iconNode = new EnumerationIconNode(header, enumName, x, y);
    setColor("maroon");
    setTextColor("maroon");
    setBackground("yellow");
    addHeader(iconNode);
    layout();
  }

  public EnumerationNode(SVGDiagramElement diagram, String enumName, Point p) {
    this(diagram, enumName, p.x, p.y);
  }
}
