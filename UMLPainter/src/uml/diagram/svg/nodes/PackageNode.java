package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.DiagramElement;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGSection;
import uml.diagram.svg.SVGSectionalNode;

public class PackageNode extends SVGSectionalNode {
  public final SVGSection header;
  public final SVGSection content;
  public PackageIconNode iconNode;

  PackageNode(SVGDiagramElement owner) {
    super(owner);
    owner.add(this);

    header = new SVGSection(this);
    content = new SVGSection(this);
    layout();
  }

  public PackageNode(SVGDiagramElement owner, String packageName, double x, double y) {
    this(owner);

    setX(x);
    setY(y);

    header.setColor("green");
    header.setBackground("SpringGreen");

    content.setColor("green");
    content.setBackground("SpringGreen");

    iconNode = new PackageIconNode(header, packageName, x, y);
    iconNode.setBackground("lime");
    iconNode.setColor("green");
    iconNode.setTextColor("green");
    addHeader(iconNode);
  }

  public PackageNode(SVGDiagramElement owner, String packageName, Point p) {
    this(owner, packageName, p.x, p.y);
  }

  public void addHeader(DiagramElement e) {
    iconNode.layout();
    header.layout();
    layout();
  }

  public void addContent(DiagramElement e) {
    content.layout();
    layout();
  }

  public ClassIconNode createClassIconNode(String name) {
    ClassIconNode in = new ClassIconNode(content, name, 0, 0);
    addContent(in);
    layout();

    return in;
  }

  public InterfaceIconNode createInterfaceIconNode(String name) {
    InterfaceIconNode in = new InterfaceIconNode(content, name, 0, 0);
    addContent(in);
    layout();
    return in;
  }

  public EnumerationIconNode createEnumerationIconNode(String name) {
    EnumerationIconNode in = new EnumerationIconNode(content, name, 0, 0);
    addContent(in);
    layout();
    return in;
  }

  public PackageIconNode createPackageIconNode(String name) {
    PackageIconNode in = new PackageIconNode(content, name, 0, 0);
    addContent(in);
    iconNode.layout();
    header.layout();
    layout();
    return in;
  }

  public String getName() {
    return iconNode.nodeText.getData();
  }

  public void setColor(String color) {
    header.setColor(color);
    content.setColor(color);
  }

  public void setBackground(String color) {
    header.setBackground(color);
    content.setBackground(color);
  }
}
