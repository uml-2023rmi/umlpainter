package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIconNode;

public class EnumerationIconNode extends SVGIconNode {
  public EnumerationIconNode(SVGDiagramElement owner, String text, double x, double y) {
    super(owner, "enumIcon", text, x, y);

    setColor("maroon");
    setTextColor("maroon");
    setBackground("yellow");
  }

  public EnumerationIconNode(SVGDiagramElement owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }

  public EnumerationIconNode(SVGDiagramElement owner, String text) {
    this(owner, text, 0, 0);
  }
}
