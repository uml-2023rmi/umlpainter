package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIcon;

public class EnumerationTinyNode extends SVGIcon {
  public EnumerationTinyNode(SVGDiagramElement owner, String text, double x, double y) {
    super(owner, "enumIcon", x, y);
    owner.add(this);

    setTooltip(text);
    setColor("maroon");
    setBackground("yellow");
  }

  public EnumerationTinyNode(SVGDiagramElement owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }
}
