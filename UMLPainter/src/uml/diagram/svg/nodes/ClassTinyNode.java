package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIcon;

public class ClassTinyNode extends SVGIcon {
  public ClassTinyNode(SVGDiagramElement owner, String text, double x, double y) {
    super(owner, "classIcon", x, y);
    owner.add(this);

    setTooltip(text);
    setColor("blue");
    setBackground("cyan");
  }

  public ClassTinyNode(SVGDiagramElement owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }
}
