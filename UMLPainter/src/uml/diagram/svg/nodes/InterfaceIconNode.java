package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIconNode;

public class InterfaceIconNode extends SVGIconNode {
  public InterfaceIconNode(SVGDiagramElement owner, String text, double x, double y) {
    super(owner, "interfaceIcon", text, x, y);
    setItalic(true);
    setColor("Purple");
    setTextColor("Purple");
    setBackground("Violet");
  }

  public InterfaceIconNode(SVGDiagramElement owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }

  public InterfaceIconNode(SVGDiagramElement owner, String text) {
    this(owner, text, 0, 0);
  }
}
