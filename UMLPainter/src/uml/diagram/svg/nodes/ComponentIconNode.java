package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIconNode;

public class ComponentIconNode extends SVGIconNode {
  public ComponentIconNode(SVGDiagramElement owner, String text, double x, double y) {
    super(owner, "componentIcon", text, x, y);
    setColor("Blue");
    setBackground("CornflowerBlue");
    setTextColor("Blue");
  }

  public ComponentIconNode(SVGDiagramElement owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }

  public ComponentIconNode(SVGDiagramElement owner, String text) {
    this(owner, text, 0, 0);
  }
}
