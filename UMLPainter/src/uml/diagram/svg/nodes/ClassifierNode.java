package uml.diagram.svg.nodes;

import uml.diagram.DiagramElement;
import uml.diagram.graphical.Text;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIconNode;
import uml.diagram.svg.SVGSection;
import uml.diagram.svg.SVGSectionalNode;
import uml.diagram.svg.graphical.SVGText;

public class ClassifierNode extends SVGSectionalNode {
  public final SVGSection header;
  public final SVGSection attributes;
  public final SVGSection operations;
  public final SVGSection classifiers;
  public SVGIconNode iconNode;

  public ClassifierNode(SVGDiagramElement owner, double x, double y) {
    this(owner);

    setX(x);
    setY(y);
    layout();
  }

  ClassifierNode(SVGDiagramElement owner) {
    super(owner);

    header = new SVGSection(this);
    attributes = new SVGSection(this);
    operations = new SVGSection(this);
    classifiers = new SVGSection(this);

    setColor("blue");
    setBackground("cyan");
  }

  public String getName() {
    return iconNode.nodeText.getData();
  }

  public void setColor(String color) {
    header.setColor(color);
    attributes.setColor(color);
    operations.setColor(color);
    classifiers.setColor(color);
  }

  public void setBackground(String color) {
    header.setBackground(color);
    attributes.setBackground(color);
    operations.setBackground(color);
    classifiers.setBackground(color);
  }

  public void addHeader(DiagramElement e) {
    iconNode.layout();
    header.layout();
    layout();
  }

  public Text createAttributeNode(String text) {
    SVGText node = new SVGText(attributes, text);
    node.setColor("blue");

    attributes.layout();
    layout();
    return node;
  }

  public Text createOperationNode(String text) {
    SVGText node = new SVGText(operations, text);
    node.setColor("blue");

    operations.layout();
    layout();
    return node;
  }

  public ClassIconNode createClassIconNode(String name) {
    ClassIconNode in = new ClassIconNode(classifiers, name, 0, 0);
    in.setBackground("cyan");
    in.setColor("blue");
    in.setTextColor("blue");

    classifiers.layout();
    layout();
    return in;
  }

  public InterfaceIconNode createInterfaceIconNode(String name) {
    InterfaceIconNode in = new InterfaceIconNode(classifiers, name, 0, 0);
    in.setBackground("cyan");
    in.setColor("blue");
    in.setTextColor("blue");

    classifiers.layout();
    layout();
    return in;
  }

  public EnumerationIconNode createEnumerationIconNode(String name) {
    EnumerationIconNode in = new EnumerationIconNode(classifiers, name, 0, 0);
    in.setBackground("cyan");
    in.setColor("blue");
    in.setTextColor("blue");

    classifiers.layout();
    layout();
    return in;
  }
}
