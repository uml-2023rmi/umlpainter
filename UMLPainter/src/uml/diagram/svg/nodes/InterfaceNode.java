package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;

public class InterfaceNode extends ClassifierNode {

  public InterfaceNode(SVGDiagramElement owner, String interfaceName, double x, double y) {
    super(owner, x, y);

    iconNode = new InterfaceIconNode(header, interfaceName, x, y);
    setColor("Purple");
    setTextColor("Purple");
    setBackground("Violet");
    addHeader(iconNode);
    layout();
  }

  public InterfaceNode(SVGDiagramElement diagram, String interfaceName, Point p) {
    this(diagram, interfaceName, p.x, p.y);
  }
}
