package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIcon;

public class InterfaceTinyNode extends SVGIcon {
  public InterfaceTinyNode(SVGDiagramElement owner, String text, double x, double y) {
    super(owner, "interfaceIcon", x, y);
    owner.add(this);

    setTooltip(text);
    setColor("Purple");
    setBackground("Violet");
  }

  public InterfaceTinyNode(SVGDiagramElement owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }
}
