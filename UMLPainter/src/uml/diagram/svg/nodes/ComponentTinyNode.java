package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIcon;

public class ComponentTinyNode extends SVGIcon {
  public ComponentTinyNode(SVGDiagramElement owner, String text, double x, double y) {
    super(owner, "componentIcon", x, y);
    owner.add(this);

    setTooltip(text);
    setColor("Blue");
    setBackground("CornflowerBlue");
  }

  public ComponentTinyNode(SVGDiagramElement owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }
}
