package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;

public class ComponentNode extends ClassifierNode {

  public ComponentNode(SVGDiagramElement owner, String componentName, double x, double y) {
    super(owner, x, y);

    iconNode = new ComponentIconNode(header, componentName, x, y);
    setColor("Blue");
    setBackground("CornflowerBlue");
    setTextColor("Blue");
    addHeader(iconNode);
  }

  public ComponentNode(SVGDiagramElement owner, String componentName, Point p) {
    this(owner, componentName, p.x, p.y);
  }

  public PackageIconNode createPackageIconNode(String name) {
    PackageIconNode in = new PackageIconNode(classifiers, name);
    in.setBackground("lime");
    in.setColor("green");
    in.setTextColor("blue");
    classifiers.layout();
    layout();

    return in;
  }
}
