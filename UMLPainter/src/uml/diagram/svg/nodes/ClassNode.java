package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;

public class ClassNode extends ClassifierNode {

  public ClassNode(SVGDiagramElement owner, String className, double x, double y) {
    super(owner, x, y);

    iconNode = new ClassIconNode(header, className, x, y);
    iconNode.setBackground("cyan");
    iconNode.setColor("blue");
    iconNode.setTextColor("blue");
    addHeader(iconNode);

  }

  public ClassNode(SVGDiagramElement owner, String className, Point p) {
    this(owner, className, p.x, p.y);
  }
}
