package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIconNode;

public class PackageIconNode extends SVGIconNode {
  public PackageIconNode(SVGDiagramElement owner, String text, double x, double y) {
    super(owner, "packageIcon", text, x, y);
    setItalic(false);
    setColor("DarkGreen");
    setTextColor("DarkGreen");
    setBackground("SpringGreen");
  }

  public PackageIconNode(SVGDiagramElement owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }

  public PackageIconNode(SVGDiagramElement owner, String text) {
    this(owner, text, 0, 0);
  }
}