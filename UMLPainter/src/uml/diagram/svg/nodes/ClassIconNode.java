package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIconNode;

public class ClassIconNode extends SVGIconNode {
  public ClassIconNode(SVGDiagramElement owner, String text, double x, double y) {
    super(owner, "classIcon", text, x, y);
    setColor("Blue");
    setTextColor("Blue");
    setBackground("Cyan");
    layout();
  }

  public ClassIconNode(SVGDiagramElement owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }

  public ClassIconNode(SVGDiagramElement owner, String text) {
    this(owner, text, 0, 0);
  }
}
