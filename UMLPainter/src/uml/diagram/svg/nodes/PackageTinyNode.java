package uml.diagram.svg.nodes;

import uml.dd.Point;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGIcon;

public class PackageTinyNode extends SVGIcon {
  public PackageTinyNode(SVGDiagramElement owner, String text, double x, double y) {
    super(owner, "packageIcon", x, y);
    owner.add(this);

    setTooltip(text);
    setColor("DarkGreen");
    setBackground("SpringGreen");
  }

  public PackageTinyNode(SVGDiagramElement owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }
}
