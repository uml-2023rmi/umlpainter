package uml.diagram.svg;

import org.dom4j.Element;
import uml.diagram.DiagramElement;
import uml.diagram.layout.Layouter;

import java.util.ArrayList;

public class SVGDiagramElement implements DiagramElement {
  public Element tag;

  private Layouter layouter = new Layouter.NoneLayouter();

  private final ArrayList<DiagramElement> ownedElements = new ArrayList<>();
  private DiagramElement owningElement;
  private org.eclipse.uml2.uml.Element modelElement;

  public SVGDiagramElement(Element svg) {
    this.tag = svg;
  }

  public SVGDiagramElement(DiagramElement owningElement) {
    this.setOwningElement(owningElement);
  }

  @Override
  public void setColor(String color) {
    tag.addAttribute("stroke", color);
  }

  @Override
  public void setTextColor(String color) {
  }

  @Override
  public void setBackground(String color) {
    tag.addAttribute("fill", color);
  }

  @Override
  public void setTooltip(String tooltip) {
  }

  @Override
  public void add(DiagramElement element) {
    ownedElements.add(element);
    element.setOwningElement(this);
    owningElement = this;
  }

  @Override
  public void layout() {
    layouter.layout();
  }

  @Override
  public Layouter getLayouter() {
    return layouter;
  }

  @Override
  public void setLayouter(Layouter layouter) {
    this.layouter = layouter;
  }

  @Override
  public ArrayList<DiagramElement> getOwnedElements() {
    return ownedElements;
  }

  @Override
  public DiagramElement getOwningElement() {
    return owningElement;
  }

  @Override
  public void setOwningElement(DiagramElement owningElement) {
    this.owningElement = owningElement;
  }

  @Override
  public void setModelElement(org.eclipse.uml2.uml.Element e) {
    modelElement = e;
  }

  @Override
  public org.eclipse.uml2.uml.Element getModelElement() {
    return modelElement;
  }
}
