package uml.diagram.svg.graphical;

import uml.dd.Point;
import uml.diagram.graphical.Line;
import uml.diagram.svg.SVGDiagramElement;

import static java.lang.Math.abs;

public class SVGLine extends SVGGraphicalElement implements Line {
  private final Point start;
  private final Point end;

  public SVGLine(SVGDiagramElement owner, Point start, Point end) {
    super(owner.tag.addElement("line"));
    owner.add(this);

    this.start = start;
    this.end = end;

    setStart(start);
    setEnd(end);

    setColor("black");
    setBackground("none");

    bounds.x = getX();
    bounds.y = getY();

    bounds.width = getWidth();
    bounds.height = getHeight();
  }

  @Override
  public void setStart(Point start) {
    tag.addAttribute("x1", "" + (int) start.x);
    tag.addAttribute("y1", "" + (int) start.y);
  }

  @Override
  public Point getStart() {
    return start;
  }

  @Override
  public void setEnd(Point end) {
    tag.addAttribute("x2", "" + (int) end.x);
    tag.addAttribute("y2", "" + (int) end.y);
  }

  @Override
  public Point getEnd() {
    return end;
  }

  @Override
  public double getX() {
    return Math.min(start.x, end.x);
  }

  @Override
  public double getY() {
    return Math.min(start.y, end.y);
  }

  @Override
  public double getWidth() {
    return abs(start.x - end.x);
  }

  @Override
  public double getHeight() {
    return abs(start.y - end.y);
  }
}
