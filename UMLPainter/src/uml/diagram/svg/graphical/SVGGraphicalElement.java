package uml.diagram.svg.graphical;

import org.dom4j.Element;
import uml.diagram.DiagramElement;
import uml.diagram.svg.SVGShape;

public class SVGGraphicalElement extends SVGShape {
  private final Element title;

  public SVGGraphicalElement(DiagramElement owner) {
    super(owner);

    title = tag.addElement("title");
    title.addText(" ");
  }

  public SVGGraphicalElement(Element owner) {
    super(owner);

    title = tag.addElement("title");
    title.addText(" ");
  }

  @Override
  public void setTooltip(String tooltip) {
    title.addText(tooltip);
  }
}
