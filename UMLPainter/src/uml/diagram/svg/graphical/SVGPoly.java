package uml.diagram.svg.graphical;

import org.dom4j.Element;
import uml.dd.Point;

import java.util.List;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class SVGPoly extends SVGGraphicalElement {
  private List<Point> points;

  protected final String color = "black";
  protected final String background = "none";

  public SVGPoly(Element owner) {
    super(owner);
  }

  public List<Point> getPoints() {
    return points;
  }

  public void setPoints(List<Point> points) {
    this.points = points;

    String pointsTxt = points
            .stream()
            .map(p -> format("%d,%d", (int) p.x, (int) p.y))
            .collect(joining(" "));
    tag.addAttribute("points", pointsTxt);
  }

  protected void init(List<Point> points) {
    setPoints(points);
    setColor(color);
    setBackground(background);

    Double minX = points.stream().map(p -> p.x).reduce(0.0, Double::min);
    Double minY = points.stream().map(p -> p.y).reduce(0.0, Double::min);
    Double maxX = points.stream().map(p -> p.x).reduce(0.0, Double::max);
    Double maxY = points.stream().map(p -> p.y).reduce(0.0, Double::max);

    setX(minX);
    setY(minY);
    setWidth(maxX - minX);
    setHeight(maxY - minY);

    tag.addAttribute("stroke-width", "3");
  }
}
