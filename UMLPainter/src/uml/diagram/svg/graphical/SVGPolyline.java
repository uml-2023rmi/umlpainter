package uml.diagram.svg.graphical;

import uml.dd.Point;
import uml.diagram.graphical.Polyline;
import uml.diagram.svg.SVGDiagramElement;

import java.util.List;

public class SVGPolyline extends SVGPoly implements Polyline {

  public SVGPolyline(SVGDiagramElement owner, List<Point> points) {
    super(owner.tag.addElement("polyline"));
    owner.add(this);

    init(points);
  }

}
