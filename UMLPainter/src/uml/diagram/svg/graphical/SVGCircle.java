package uml.diagram.svg.graphical;

import uml.dd.Point;
import uml.diagram.graphical.Circle;
import uml.diagram.svg.SVGDiagramElement;

public class SVGCircle extends SVGGraphicalElement implements Circle {
  private final String color = "black";

  /**
   * По умолчанию прямоугольник прозрачный.
   */
  private final String background = "none";

  private double radius;
  private final Point center;

  public SVGCircle(SVGDiagramElement owner, Point center, double radius) {
    super(owner.tag.addElement("circle"));
    owner.add(this);

    this.center = center;
    setRadius(radius);
    setCenter(center.x, center.y);
    setWidth(2 * radius);
    setHeight(2 * radius);

    setColor(color);
    setBackground(background);
  }

  @Override
  public void setX(double x) {
    bounds.x = x;
    center.x = x + radius;
    setCenter(center.x, center.y);
  }

  @Override
  public void setY(double y) {
    bounds.y = y;
    center.y = y + radius;
    setCenter(center.x, center.y);
  }

  public void setCenter(double x, double y) {
    center.x = x;
    center.y = y;

    super.setX(x - radius);
    super.setY(y - radius);
    tag.addAttribute("cx", "" + (int) x);
    tag.addAttribute("cy", "" + (int) y);
  }

  @Override
  public Point getCenter() {
    return center;
  }

  @Override
  public void setCenter(Point center) {
    setCenter(center.x, center.y);
  }

  @Override
  public void setRadius(double r) {
    radius = r;
    setWidth(2 * r);
    setHeight(2 * r);
    tag.addAttribute("r", "" + (int) r);
  }

  @Override
  public double getRadius() {
    return radius;
  }
}
