package uml.diagram.svg.graphical;

import uml.dd.AlignmentKind;
import uml.dd.Point;
import uml.diagram.graphical.Text;
import uml.diagram.svg.SVG;
import uml.diagram.svg.SVGDiagram;
import uml.diagram.svg.SVGDiagramElement;

public class SVGText extends SVGGraphicalElement implements Text {
  public static final double SymbolW = 10; // Character width.
  public static final double SymbolH = 16; // Character height.

  private String data;

  private AlignmentKind alignment;

  public SVGText(SVGDiagramElement owner, String data, double x, double y) {
    super(owner.tag.addElement("text"));
    owner.add(this);

    setData(data);

    setX(x);
    setY(y);
    setWidth(SymbolW * data.length());
    setHeight(SymbolH);
    setColor("black");
    setBackground("none");

    tag.addAttribute("font-size", "" + (int) SymbolH + "px");
    tag.addAttribute("font-family", "Courier");
  }

  public SVGText(SVGDiagramElement owner, String text) {
    this(owner, text, 0, 0);
  }

  public SVGText(SVGDiagram owner, String text, Point p) {
    this(owner, text, p.x, p.y);
  }

  @Override
  public void setBold() {
    tag.addAttribute("font-weight", "bold");
  }

  @Override
  public void setUnderline() {
    tag.addAttribute("text-decoration", "underline");
  }

  @Override
  public void setNormalText() {
    tag.addAttribute("font-style", "normal");
  }

  @Override
  public void setItalic(boolean isItalic) {
    if (isItalic)
      tag.addAttribute("font-style", "italic");
  }

  @Override
  public void setColor(String color) {
    tag.addAttribute("fill", color);
  }

  @Override
  public void setBackground(String color) {
    tag.addAttribute("stroke", color);
  }

  @Override
  public void setX(double x) {
    super.setX(x);
    tag.addAttribute("x", "" + (int) x);
  }

  @Override
  public void setY(double y) {
    super.setY(y);
    tag.addAttribute("y", "" + (int) (y + SVG.IHEIGHT));
  }

  @Override
  public void setWidth(double width) {
    super.setWidth(width);
    tag.addAttribute("width", "" + (int) width + "px");
  }

  @Override
  public void setHeight(double height) {
    super.setHeight(height);
    tag.addAttribute("height", "" + (int) height + "px");
  }

  @Override
  public double getHeight() {
    return SymbolH;
  }

  @Override
  public double getWidth() {
    return SymbolW * data.length();
  }

  @Override
  public void setTooltip(String text) {
    // tooltip.setText(text);
  }

  @Override
  public String getData() {
    return data;
  }

  @Override
  public void setData(String data) {
    this.data = data;
    tag.addText(data);
  }

  @Override
  public AlignmentKind getAlignment() {
    return alignment;
  }

  @Override
  public void setAlignment(AlignmentKind alignment) {
    this.alignment = alignment;

    switch (alignment) {
      case center:
        tag.addAttribute("text-anchor", "middle");
        break;
      case end:
        tag.addAttribute("text-anchor", "end");
        break;
	default:
		break;
    }
  }
}
