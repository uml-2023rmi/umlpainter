package uml.diagram.svg.graphical;

import java.util.List;

import uml.dd.Point;
import uml.diagram.graphical.Polygon;
import uml.diagram.svg.SVGDiagramElement;

public class SVGPolygon extends SVGPoly implements Polygon {
  public SVGPolygon(SVGDiagramElement owner, List<Point> points) {
    super(owner.tag.addElement("polygon"));
    owner.add(this);

    init(points);
  }
}
