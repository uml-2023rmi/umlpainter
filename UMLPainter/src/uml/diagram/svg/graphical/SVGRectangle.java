package uml.diagram.svg.graphical;

import uml.dd.Bounds;
import uml.diagram.graphical.Rectangle;
import uml.diagram.svg.SVGDiagramElement;

public class SVGRectangle extends SVGGraphicalElement implements Rectangle {
	private String color = "black";

	/**
	 * По умолчанию прямоугольник прозрачный.
	 */
	private String background = "none";
	private double cornerRadius;

	public SVGRectangle(SVGDiagramElement owner, Bounds bounds) {
		this(owner, bounds, 0.0);
	}

	public SVGRectangle(SVGDiagramElement owner, Bounds bounds, double cornerRadius) {
		super(owner.tag.addElement("rect"));
		owner.add(this);

		super.setBounds(bounds);
		setColor(color);
		setBackground(background);

		this.cornerRadius = cornerRadius;

		if (cornerRadius != 0.0)
			;
		setCornerRadius(cornerRadius);
	}

	@Override
	public void setX(double x) {
		super.setX(x);
		tag.addAttribute("x", "" + (int) x + "px");
	}

	@Override
	public void setY(double y) {
		super.setY(y);
		tag.addAttribute("y", "" + (int) y + "px");
	}

	@Override
	public void setWidth(double width) {
		super.setWidth(width);
		tag.addAttribute("width", "" + (int) width + "px");
	}

	@Override
	public void setHeight(double height) {
		super.setHeight(height);
		tag.addAttribute("height", "" + (int) height + "px");
	}

	@Override
	public void setBounds(Bounds bounds) {
		this.bounds = bounds;
	}

	@Override
	public Bounds getBounds() {
		return bounds;
	}

	@Override
	public void setCornerRadius(double cornerRadius) {
		this.cornerRadius = cornerRadius;
		tag.addAttribute("rx", "" + (int) cornerRadius + "px");
		tag.addAttribute("ry", "" + (int) cornerRadius + "px");
	}

	@Override
	public double getCornerRadius() {
		return cornerRadius;
	}

	@Override
	public String getBackground() {
		return background;
	}

	@Override
	public void setBackground(String background) {
		this.background = background;
		tag.addAttribute("fill", background);
	}

	@Override
	public void setColor(String color) {
		this.color = color;
		tag.addAttribute("stroke", color);
	}

	@Override
	public String getColor() {
		return color;
	}
}
