package uml.diagram.svg;

import org.dom4j.Element;
import uml.dd.Point;
import uml.diagram.Shape;
import uml.diagram.anchors.Anchor;
import uml.diagram.anchors.AnchorBottom;
import uml.diagram.anchors.AnchorTop;
import uml.diagram.routers.*;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class SVGEdge extends SVGDiagramElement implements uml.diagram.Edge {
  private List<Point> waypoints;

  private Shape source;
  private Shape target;

  private Anchor sourceAnchor;
  private Anchor targetAnchor;

  private Router router;

  {
    setWaypoints(new ArrayList<>());
    waypoints.add(new Point(0, 0));
    waypoints.add(new Point(0, 0));

    setColor("black");
    setBackground("white");
  }

  public SVGEdge(SVGDiagramElement owner, Shape source, Shape target) {
    super(owner.tag.addElement("path"));
    this.source = source;
    this.target = target;

    setSourceAnchor(new AnchorTop());
    sourceAnchor.setShape(source);

    setTargetAnchor(new AnchorBottom());
    targetAnchor.setShape(target);

    setRouter(new Router.NoRouter());

    drawWay(tag);
    owner.add(this);
  }

  public SVGEdge(SVGDiagramElement owner) {
    super(owner.tag.addElement("path"));
    waypoints.clear();
  }

  protected void drawPath(double[] xy, Element e) {
    for (int k = 0; k < xy.length; k += 2)
      waypoints.add(new Point(xy[k], xy[k + 1]));

    drawWay(e);
  }

  private void drawWay(Element e) {
    StringBuilder sb = new StringBuilder();

    Point p0 = waypoints.get(0);
    sb.append(format("M %d %d ", (int) p0.x, (int) p0.y));

    for (int k = 1; k < waypoints.size(); k++) {
      Point p = waypoints.get(k);
      sb.append(format("L %d %d ", (int) p.x, (int) p.y));
    }

    e.addAttribute("d", sb.toString());
    e.addAttribute("fill", "none");
    e.addAttribute("opacity", "1");
  }

  @Override
  public void setSourceAnchor(Anchor anchor) {
    sourceAnchor = anchor;
    sourceAnchor.setShape(getSource());
  }

  @Override
  public void setTargetAnchor(Anchor anchor) {
    targetAnchor = anchor;
    targetAnchor.setShape(getTarget());
  }

  @Override
  public void setRouter(Router router) {
    this.router = router;
    router.setEdge(this);
  }

  @Override
  public void setRouterHV() {
    setRouter(new RouterHV());
    router.setEdge(this);
  }

  @Override
  public void setRouterVH() {
    setRouter(new RouterVH());
    router.setEdge(this);
  }

  @Override
  public void setRouterHVH() {
    setRouter(new RouterHVH());
    router.setEdge(this);
  }

  @Override
  public void setRouterVHV() {
    setRouter(new RouterVHV());
    router.setEdge(this);
  }

  @Override
  public void layout() {
    waypoints.set(0, sourceAnchor.get());
    waypoints.set(waypoints.size() - 1, targetAnchor.get());
    router.route();
    drawWay(tag);
  }

  @Override
  public String toString() {
    String kind = getClass().getSimpleName();
    String s = source.getClass().getSimpleName();
    String t = target.getClass().getSimpleName();
    String sa = sourceAnchor.getClass().getSimpleName();
    String ta = targetAnchor.getClass().getSimpleName();

    String way = waypoints
            .stream()
            .map(p -> format("(%d,%d)", (int) p.x, (int) p.y))
            .collect(joining(",", "[", "]"));

    return format("%s[%s]--%s%s->%s[%s]", sa, s, kind, way, ta, t);
  }

  @Override
  public List<Point> getWaypoints() {
    return waypoints;
  }

  @Override
  public void setWaypoints(List<Point> waypoints) {
    this.waypoints = waypoints;
  }

  @Override
  public Shape getSource() {
    return source;
  }

  @Override
  public void setSource(Shape source) {
    this.source = source;
  }

  @Override
  public Shape getTarget() {
    return target;
  }

  @Override
  public void setTarget(Shape target) {
    this.target = target;
  }
}
