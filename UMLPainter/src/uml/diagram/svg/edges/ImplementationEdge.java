package uml.diagram.svg.edges;

import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGEdge;
import uml.diagram.svg.nodes.ClassNode;
import uml.diagram.svg.nodes.ClassifierNode;

public class ImplementationEdge extends SVGEdge {
  {
    setColor("navy");
    setBackground("white");

    tag.addAttribute("stroke-dasharray", "" + 5);
    tag.addAttribute("marker-end", "url(#triangleAdorn)");
  }

  public ImplementationEdge(SVGDiagramElement owner, ClassNode source, ClassifierNode target) {
    super(owner, source, target);
  }
}
