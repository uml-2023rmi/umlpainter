package uml.diagram.svg.edges;

import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGEdge;
import uml.diagram.svg.nodes.ClassifierNode;

public class AssociationEdge extends SVGEdge {
  {
    setColor("navy");
    setBackground("white");
    tag.addAttribute("marker-end", "url(#arrowAdorn)");
  }

  public AssociationEdge(SVGDiagramElement owner, ClassifierNode source, ClassifierNode target) {
    super(owner, source, target);
  }
}
