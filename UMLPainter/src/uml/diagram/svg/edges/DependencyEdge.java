package uml.diagram.svg.edges;

import uml.diagram.Shape;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGEdge;

public class DependencyEdge extends SVGEdge {
  public static final int D = 5;

  {
    setColor("navy");
    setBackground("white");
    tag.addAttribute("stroke-dasharray", "" + D);
    tag.addAttribute("marker-end", "url(#arrowAdorn)");
  }

  public DependencyEdge(SVGDiagramElement owner, Shape source, Shape target) {
    super(owner, source, target);
  }

  public DependencyEdge(SVGDiagramElement owner, double x1, double y1, double x2, double y2) {
    super(owner);
    drawPath(new double[]{x1, y1, x2, y2}, tag);
  }
}  
