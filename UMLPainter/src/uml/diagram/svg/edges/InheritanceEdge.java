package uml.diagram.svg.edges;

import uml.diagram.Shape;
import uml.diagram.svg.SVGDiagramElement;
import uml.diagram.svg.SVGEdge;

public class InheritanceEdge extends SVGEdge {
  {
    setColor("navy");
    setBackground("white");
    tag.addAttribute("marker-end", "url(#triangleAdorn)");
  }

  public InheritanceEdge(SVGDiagramElement owner, Shape source, Shape target) {
    super(owner, source, target);
  }
}
