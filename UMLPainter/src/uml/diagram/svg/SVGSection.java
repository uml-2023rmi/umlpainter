package uml.diagram.svg;

import org.dom4j.Element;
import uml.dd.Bounds;
import uml.diagram.DiagramElement;
import uml.diagram.graphical.Rectangle;
import uml.diagram.layout.VerticalLayouter;
import uml.diagram.svg.graphical.SVGRectangle;

import static java.util.stream.Collectors.joining;

/**
 * Простой узел-прямоугольник с цветными рамкой и полем.
 */
public class SVGSection extends SVGShape {
  final double padding = 4;
  final double spacing = 2;

  protected final Rectangle paper;

  private String color = "none";
  private String background = "none";
  private String textColor = "none";

  {
    VerticalLayouter vl = new VerticalLayouter(this, padding, spacing, true);
    vl.startElement = 1; // Пропускаем 1-й элемент (прямоугольник-фон).
    setLayouter(vl);
  }

  public SVGSection(SVGDiagramElement owner) {
    super(owner.tag.addElement("g"));
    owner.add(this);

    paper = new SVGRectangle(this, new Bounds(100, 100));
    paper.setBackground(background);
    paper.setColor(color);

    setWidth(0);
    setHeight(0);

    owner.layout();
  }

  public SVGSection(Element owner) {
    super(owner.addElement("g"));

    paper = new SVGRectangle(this, new Bounds(0, 0));
    paper.setBackground(background);
    paper.setColor(color);

    setWidth(0);
    setHeight(0);
  }

  public void layout() {
    getLayouter().layout();
    resizePaper();
  }

  protected void resizePaper() {
    paper.setX(getX());
    paper.setY(getY());
    paper.setWidth(getWidth());
    paper.setHeight(getHeight());
  }

  public void setWidth(double d) {
    super.setWidth(d);
    paper.setWidth(d);
  }

  public void setHeight(double h) {
    super.setHeight(h);
    paper.setHeight(h);
  }

  public void setX(double x) {
    super.setX(x);
    paper.setX(x);
    layout();
  }

  public void setY(double y) {
    super.setY(y);
    paper.setY(y);
    layout();
  }

  public String getBackground() {
    return background;
  }

  public void setBackground(String background) {
    this.background = background;
    paper.setBackground(background);
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
    paper.setColor(color);
  }

  public void setTextColor(String textColor) {
    this.textColor = textColor;
  }

  public String toString() {
    String owner = super.toString();

    if (getOwnedElements().isEmpty())
      return owner + "{}";

    String prefix = "\n {  ";
    String separator = ",\n  ";
    String postfix = "\n }\n\n";

    String owned = getOwnedElements()
            .stream()
            .map(DiagramElement::toString)
            .collect(joining(separator, prefix, postfix));
    return owner + owned;
  }
}
