package uml.diagram.svg;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.dom.DOMCDATA;
import uml.diagram.svg.adorns.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class SVG {
  public static final int IWIDTH = 18;
  public static final int IHEIGHT = 18;

  public static final String DTD_SVG =
          "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd";
  public static final Namespace nsXLink =
          new Namespace("xlink", "http://www.w3.org/1999/xlink");

  /**
   * Встроить тег <svg> в документ как корневой элемент.
   *
   * @param document кда встроить тег <svg>
   * @return элемент XML-дерева для svg-ега
   */
  public static Element svgHead(Document document) {
    Element svg = document.addElement("svg", "http://www.w3.org/2000/svg");

    initSVG(svg);

    return svg;
  }

  /**
   * Встроить тег <svg> в HTML5-тег как корневой элемент.
   *
   * @param tag в какой тег встроить.
   * @return элемент XML-дерева для svg-тега
   */
  public static Element svgHead(Element tag) {
    Element svg = tag.addElement("svg", "http://www.w3.org/2000/svg");

    initSVG(svg);

    return svg;
  }

  private static void initSVG(Element svg) {
    svg.addAttribute("width", "2000px");
    svg.addAttribute("height", "2000px");

    svg.add(new Namespace("xlink", "http://www.w3.org/1999/xlink"));

    Element defs = svg.addElement("defs");

    new ArrowAdorn(defs);
    new TriangleAdorn(defs);

    new EnumIcon(defs);
    new ClassIcon(defs);
    new InterfaceIcon(defs);
    new PackageIcon(defs);
    new ComponentIcon(defs);

    Element style = defs.addElement("style");
    style.addAttribute("type", "text/css");

    ByteArrayOutputStream bas = new ByteArrayOutputStream();
    PrintStream out = new PrintStream(bas);
    out.format("%n.classifier { %n");
    out.format("  fill: cyan; %n");
    out.format("  stroke: blue; %n");
    out.format("  stroke-width: 1; %n");
    out.format("} %n");
    out.format("%n.package { %n");
    out.format("  fill: lime; %n");
    out.format("  stroke: green; %n");
    out.format("  stroke-width: 1; %n");
    out.format("} %n");

    String styles = bas.toString();
    out.close();

    style.add(new DOMCDATA(styles));

    svg.addAttribute("version", "1.1");
  }

  public static Element createInterfaceIcon(Element owner, int x, int y) {
    Element c = owner.addElement("circle");

    c.addAttribute("class", "classifier");
    c.addAttribute("cx", "" + (x + IWIDTH / 2));
    c.addAttribute("cy", "" + (y + IHEIGHT / 2));
    c.addAttribute("r", "" + IWIDTH / 2);

    return c;
  }

  public static Element createClassIcon(Element owner, int x, int y) {
    Element g = owner.addElement("g");
    g.addAttribute("class", "classifier");

    Element r = g.addElement("rect");
    r.addAttribute("x", "" + x);
    r.addAttribute("y", "" + y);
    r.addAttribute("width", "" + IWIDTH);
    r.addAttribute("height", "" + IHEIGHT);
    r.addAttribute("stroke-width", "1");

    Element l = g.addElement("line");
    l.addAttribute("x1", "" + x);
    l.addAttribute("y1", "" + (y + IHEIGHT / 3));
    l.addAttribute("x2", "" + (x + IWIDTH));
    l.addAttribute("y2", "" + (y + IHEIGHT / 3));
    l.addAttribute("stroke-width", "1");

    l = g.addElement("line");
    l.addAttribute("x1", "" + x);
    l.addAttribute("y1", "" + (y + 2 * IHEIGHT / 3));
    l.addAttribute("x2", "" + (x + IWIDTH));
    l.addAttribute("y2", "" + (y + 2 * IHEIGHT / 3));
    l.addAttribute("stroke-width", "1");

    return g;
  }

  public static Element createComponentIcon(Element owner, int x, int y) {
    int w4 = IWIDTH / 4;
    int h5 = IHEIGHT / 5;

    Element g = owner.addElement("g");
    g.addAttribute("class", "component");

    Element r;
    r = g.addElement("rect");
    r.addAttribute("x", "" + x + w4);
    r.addAttribute("y", "" + y);
    r.addAttribute("width", "" + (IWIDTH - w4));
    r.addAttribute("height", "" + IHEIGHT);
    r.addAttribute("stroke-width", "1");

    r = g.addElement("rect");
    r.addAttribute("x", "" + x);
    r.addAttribute("y", "" + (y + h5));
    r.addAttribute("width", "" + (2 * w4));
    r.addAttribute("height", "" + h5);
    r.addAttribute("stroke-width", "1");

    r = g.addElement("rect");
    r.addAttribute("x", "" + x);
    r.addAttribute("y", "" + (y + 3 * h5));
    r.addAttribute("width", "" + (2 * w4));
    r.addAttribute("height", "" + h5);
    r.addAttribute("stroke-width", "1");

    return g;
  }

  public static Element createEnumIcon(Element owner, int x, int y) {
    Element g = owner.addElement("g");
    g.addAttribute("class", "classifier");

    Element r = g.addElement("rect");
    r.addAttribute("x", "" + x);
    r.addAttribute("y", "" + y);
    r.addAttribute("width", "" + IWIDTH);
    r.addAttribute("height", "" + IHEIGHT);

    Element l = g.addElement("line");
    l.addAttribute("x1", "" + x);
    l.addAttribute("y1", "" + (y + IHEIGHT / 3));
    l.addAttribute("x2", "" + (x + IWIDTH));
    l.addAttribute("y2", "" + (y + IHEIGHT / 3));

    return g;
  }

  public static Element createPackageIcon(Element owner, int x, int y) {
    Element g = owner.addElement("g");
    g.addAttribute("class", "package");

    Element r = g.addElement("rect");
    r.addAttribute("x", "" + x);
    r.addAttribute("y", "" + y);
    r.addAttribute("width", "" + IWIDTH);
    r.addAttribute("height", "" + IHEIGHT);

    Element l = g.addElement("line");
    l.addAttribute("x1", "" + x);
    l.addAttribute("y1", "" + (y + IHEIGHT / 3));
    l.addAttribute("x2", "" + (x + 2 * IWIDTH / 3));
    l.addAttribute("y2", "" + (y + IHEIGHT / 3));

    l = g.addElement("line");
    l.addAttribute("x1", "" + (x + 2 * IWIDTH / 3));
    l.addAttribute("y1", "" + (y + IHEIGHT / 3));
    l.addAttribute("x2", "" + (x + IWIDTH));
    l.addAttribute("y2", "" + y);

    return g;
  }

  public static Element createText(Element owner, int x, int y, String text) {
    int fontSize = IHEIGHT;

    Element e = owner.addElement("text");

    e.addAttribute("x", "" + x);
    e.addAttribute("y", "" + (y + fontSize));
    e.addAttribute("stroke", "none");
    e.addAttribute("fill", "black");
    e.addAttribute("font-size", "" + fontSize);

    e.addText(text);

    return e;
  }

  public static int getTextWidth(String text) {
    return IWIDTH * text.length();
  }

}
