package uml.diagram.svg;

import org.dom4j.Element;
import uml.dd.Bounds;
import uml.diagram.DiagramElement;

import static java.lang.String.format;

public class SVGShape extends SVGDiagramElement implements uml.diagram.Shape {
  protected Bounds bounds = new Bounds();

  public SVGShape(Element owner) {
    super(owner);

    if (owner instanceof SVGDiagramElement)
      ((DiagramElement) owner).add(this);
  }

  public SVGShape(DiagramElement owner) {
    super(owner);

    if (owner != null)
      owner.add(this);
  }

  protected void setBounds(Bounds bounds) {
    this.bounds = bounds;

    tag.addAttribute("x", "" + (int) bounds.x + "px");
    tag.addAttribute("y", "" + (int) bounds.y + "px");
    tag.addAttribute("width", "" + (int) bounds.width + "px");
    tag.addAttribute("height", "" + (int) bounds.height + "px");
  }

  @Override
  public Bounds getBounds() {
    return bounds;
  }

  @Override
  public void setX(double x) {
    bounds.x = x;
  }

  @Override
  public void setY(double y) {
    bounds.y = y;
  }

  @Override
  public double getX() {
    return bounds.x;
  }

  @Override
  public double getY() {
    return bounds.y;
  }

  @Override
  public double getRight() {
    return bounds.x + bounds.width;
  }

  @Override
  public double getBottom() {
    return bounds.y + bounds.height;
  }

  @Override
  public double getCenterX() {
    return bounds.x + bounds.width / 2;
  }

  @Override
  public double getCenterY() {
    return bounds.y + bounds.height / 2;
  }

  @Override
  public double getLeft() {
    return bounds.x;
  }

  @Override
  public double getTop() {
    return bounds.y;
  }

  @Override
  public void setWidth(double width) {
    bounds.width = width;
  }

  @Override
  public void setHeight(double height) {
    bounds.height = height;
  }

  @Override
  public double getWidth() {
    return bounds.width;
  }

  @Override
  public double getHeight() {
    return bounds.height;
  }

  @Override
  public String toString() {
    String prefix = this.getClass().getSimpleName();
    return
            format("%s(left:%d top:%d right:%d bottom:%d w:%d h:%d)",
                    prefix,
                    (int) getLeft(), (int) getTop(),
                    (int) getRight(), (int) getBottom(),
                    (int) bounds.width, (int) bounds.height);
  }
}
