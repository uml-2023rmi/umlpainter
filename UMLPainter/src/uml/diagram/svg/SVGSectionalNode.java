package uml.diagram.svg;

import uml.diagram.layout.VerticalLayouter;

public class SVGSectionalNode extends SVGSection {
  public SVGSectionalNode(SVGDiagramElement owner) {
    super(owner);

    // Прозрачный узел с секциями.
    VerticalLayouter vl = new VerticalLayouter(this, 0, 0, true);
    vl.startElement = 1; // Пропускаем 1-й элемент (прямоугольник-фон).
    setLayouter(vl);
  }

  public void setX(double x) {
    super.setX(x);
    layout();
  }

  public void setY(double y) {
    super.setY(y);
    layout();
  }
}
