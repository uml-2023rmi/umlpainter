package uml.diagram;

public interface Diagram extends DiagramElement, Shape {
  @Override
  void layout();

  void save(String dir, String file);

  String getName();

  void setName(String name);
}
