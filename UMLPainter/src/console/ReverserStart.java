package console;

import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLFactory;
import samples.SVGinHTML5;
import samples.UML2SVGDemo;
import tutorial.TaskSamples;
import tutorial.TutorialDiagramsGenerator;
import uml.java.reverser.asm.AsmBytecodeReverser;
import uml.relations.UMLRelations;
import uml.report.UML2HTMLReporter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class ReverserStart {
    private static final Logger log = Logger.getLogger(ReverserStart.class.toString());
    private static final String userHome = System.getProperty("user.home");

    private static final String fs = File.separator;

    private static Model model;
    private static final AsmBytecodeReverser reverser;

    private static final LogProgressTracing trace;

    private static final File rootDir;
    private static final String svgDemoPath;
    private static final String rootPath;

    private static final List<String> jarFiles;

    private static final String htmlPath;

    private static final String tutorialPath;

    private static final String taskPath;

    static {
        rootDir = new File(".");
        rootPath = rootDir.getAbsolutePath();

        svgDemoPath = rootPath.replace(".", "generated-svg-diagrams");
        tutorialPath = rootPath.replace(".", "tutorial");
        taskPath = rootPath.replace(".", "tasks/img");

//	htmlPath = rootPath.replace(".", "generated-html5");
        htmlPath = userHome + fs + "generated-html5";

        model = UMLFactory.eINSTANCE.createModel();
        model.setName("Empty Model");

        reverser = new AsmBytecodeReverser();
        trace = new LogProgressTracing(log);

        jarFiles = new ArrayList<>();
    }

    public static void main(String[] args) {
        run();
    }

    public static void run() {
        TutorialDiagramsGenerator.generate(tutorialPath);
        TaskSamples.generate(taskPath);

        UML2SVGDemo.showPrimitives(tutorialPath, "uml-primitives.svg");
        UML2SVGDemo.showPrimitives(svgDemoPath, "uml-primitives.svg");
        SVGinHTML5.generate(svgDemoPath, "svg-in-html5.html");

        String dom4j1_4path = rootPath.replace(".", "lib/dom4j-1.6.1.jar");
        String dom4j1_6path = rootPath.replace(".", "lib/dom4j-1.4.jar");

        jarFiles.clear();
        jarFiles.add(dom4j1_6path);

        Model model1_4 = buildUMLModel("DOM4J1_4", asList(dom4j1_4path));
        Model model1_6 = buildUMLModel("DOM4J1_6", asList(dom4j1_6path));

        genReport(asList(model1_4, model1_6));

//		jarFiles.clear();
//		jarFiles.add(rootPath.replace(".", "lib/asm-7.2.jar"));
//		buildUMLModel("ASM", jarFiles);
    }

    public static Model buildUMLModel(String modelName, List<String> jars) {
        Model model = UMLFactory.eINSTANCE.createModel();
        model.setName(modelName);

        log.info("UML model building is started: " + modelName);
        reverser.reverseJarFileCollection(jars, model, trace);
        log.info("UML model building is complete: " + modelName + "\n");

        UMLRelations.load(model);
        return model;
    }

    private static void genReport(List<Model> models) {
        String title = models
                .stream()
                .map(m -> m.getName())
                .collect(Collectors.joining(", ", "", ""));

//		String title = model.getName();

        log.info("HTML report generation is started for: " + title);
        UML2HTMLReporter.generateReport(models, htmlPath);
        log.info("HTML report generation is complete for: " + title + "\n");
        log.info("HTML report generated to: \n" + htmlPath + "\n");
    }
}
