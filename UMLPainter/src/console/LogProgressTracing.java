package console;


import java.util.logging.Logger;

import uml.java.reverser.ProgressTracing;

public class LogProgressTracing implements ProgressTracing {
	private final Logger log;

	public LogProgressTracing(Logger log) {
		this.log = log;
	}

	@Override
	public void outMessage(String message) {
		log.info(message);
	}

	@Override
	public void begin(String message, int size) {
		log.info(message + " [" + size + "] elements");
	}
	
	@Override
	public void step(int scale) {
	}

	@Override
	public void done() {
		log.info("Is done.");
	}
}
