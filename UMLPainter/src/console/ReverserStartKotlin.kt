package console

import uml.report.DiagramGeneratorRegistry
import uml.report.reporters.NestedPackagesSizesInKotlin

fun main() {
	// Регистрация генераторов диаграмм написанных на Котлин. 
	DiagramGeneratorRegistry.register( NestedPackagesSizesInKotlin() )

	ReverserStart.run()
}
