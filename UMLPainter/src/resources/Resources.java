package resources;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Resources {
	static
	public InputStream getResource(String fileName) {
		return Resources.class.getResourceAsStream(fileName);
	}
	
	/**
	 * Скопировать в генерируемый отчет файлы-ресурсы (изображения, Javascript-файлы, ...). 
	 * Файлы-ресурсы должны быть расположены в одной папке с классом Resources.
	 * 
	 * @param dir
	 *            - папка в генерируемом отчете куда копируются файлы.
	 * @param fileName
	 *            - имя копируемого файла-ресурса.
	 */
	static
	public void copyResource(String dir, String fileName) {
		new File(dir).mkdirs();
		
		InputStream is = Resources.getResource(fileName);
		Path outPath = Paths.get(dir, fileName);
		
		try { Files.copy(is, outPath, StandardCopyOption.REPLACE_EXISTING);	} 
		catch (IOException e) { e.printStackTrace(); }
	}

	/**
	 * Скопировать файлы-ресурсы в папки вложенные в корневую папку.
	 * @param rootDir папка в которую переписываюся ресурсы.
	 */
	static 
	public void copyResources(String rootDir) {
		String cssDir = rootDir + "/css";
		String imgDir = rootDir + "/img";

		new File(rootDir).mkdirs();
		new File(cssDir).mkdirs();
		new File(imgDir).mkdirs();

		copyResource(cssDir, "report.css");

		copyResource(imgDir, "Class.png");
		copyResource(imgDir, "Enum.png");
		copyResource(imgDir, "Interface.png");
		copyResource(imgDir, "Package.png");
	}
}