package util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class FilesUtil {
	static
	public void walkRes(String root, Predicate<String> fileFilter, Consumer<String> action) {
	    Path start = Paths.get(root);
		try {
			Stream<Path> stream = Files.walk(start, Integer.MAX_VALUE, FileVisitOption.FOLLOW_LINKS);
				stream
					.map(Path::toString)
					.filter(fileFilter)
					.forEach(action);
			stream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.format("File not found: %s %n", e);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
