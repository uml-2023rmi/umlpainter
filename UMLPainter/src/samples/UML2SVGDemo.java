package samples;

import uml.diagram.Edge;
import uml.diagram.Shape;
import uml.diagram.svg.SVGDiagram;
import uml.diagram.svg.SVGIconNode;
import uml.diagram.anchors.AnchorBottom;
import uml.diagram.anchors.AnchorTop;
import uml.diagram.svg.edges.AssociationEdge;
import uml.diagram.svg.edges.DependencyEdge;
import uml.diagram.svg.edges.ImplementationEdge;
import uml.diagram.svg.edges.InheritanceEdge;
import uml.diagram.svg.nodes.*;

import java.io.File;

import static java.lang.Math.max;

public class UML2SVGDemo {
  private static final double DX = 30;
  private static final double DY = 30;

  private static final double X = DX;
  private static final double Y = DY;

  public static void showPrimitives(String diagramDir, String fileName) {
    new File(diagramDir).mkdirs();

    SVGDiagram diagram = new SVGDiagram("Примитивы для рисования UML диаграмм");

    double x = X;
    double y = Y;
    double columnWidth = 0;

    // ===================================
    // Узлы-пиктограммы на диаграммах UML.
    // ===================================
    SVGIconNode icon;

    // Класс - пиктограмма.
    icon = new ClassIconNode(diagram, "Class", x, y);
    icon.setX(x);
    icon.setY(y);
    columnWidth = max(columnWidth, icon.getWidth());
    y += (icon.getHeight() + DY);

    // Интерфейс - пиктограмма.
    icon = new InterfaceIconNode(diagram, "Interface", x, y);
    icon.setColor("Purple");
    icon.setBackground("Violet");
    icon.setColor("Purple");
    icon.setBackground("Violet");
    icon.setTextColor("Indigo");
    columnWidth = max(columnWidth, icon.getWidth());
    y += (icon.getHeight() + DY);

    // Перечисление - пиктограмма.
    icon = new EnumerationIconNode(diagram, "Enumeration", x, y);
    icon.setColor("maroon");
    icon.setBackground("yellow");
    icon.setTextColor("maroon");
    columnWidth = max(columnWidth, icon.getWidth());
    y += (icon.getHeight() + DY);

    // Пакет - пиктограмма.
    icon = new PackageIconNode(diagram, "Package", x, y);
    icon.setColor("green");
    icon.setBackground("SpringGreen");
    icon.setTextColor("green");
    columnWidth = max(columnWidth, icon.getWidth());
    y += (icon.getHeight() + DY);

    // Компонента - пиктограмма.
    icon = new ComponentIconNode(diagram, "swt.jar", x, y);
    icon.setColor("Blue");
    icon.setBackground("CornflowerBlue");
    icon.setTextColor("Blue");
    icon.setX(x);
    icon.setY(y);
    columnWidth = max(columnWidth, icon.getWidth());

    // =================================
    // Составные узлы на диаграммах UML.
    // =================================
    //
    // Новая колонка.
    // ==============
    x += (columnWidth + DX);
    y = Y;

    // ----- Составной пакет. -----
    PackageNode pn = new PackageNode(diagram, "eclipse.swt", x, y);

    // Интерфейс вложенный в пакет.
    InterfaceIconNode iMouse = pn.createInterfaceIconNode("MouseListener");
    iMouse.setBackground("Violet");
    iMouse.setColor("Purple");

    // Интерфейс вложенный в пакет.
    InterfaceIconNode iMouseListener = pn.createInterfaceIconNode("KeyboardListener");
    iMouseListener.setBackground("Violet");
    iMouseListener.setColor("Purple");

    // Класс вложенный в пакет.
    SVGIconNode in = pn.createClassIconNode("Event");
    in.setColor("blue");
    in.setTextColor("maroon");
    in.setItalic(true);
    y += (pn.getHeight() + DY);

    // Класс.
    ClassNode cn = new ClassNode(diagram, "PaintEvent", x, y);
    cn.header.setBackground("cyan");
    cn.header.setColor("blue");
    y += 50;

    // Интерфейс.
    InterfaceNode inn = new InterfaceNode(diagram, "PaintEvent", x, y);
    inn.iconNode.setBackground("Violet");
    inn.header.setColor("Indigo");
    inn.header.setBackground("Violet");
    inn.header.setColor("Indigo");
    y += 50;

    // Компонета.
    ComponentNode cmn = new ComponentNode(diagram, "ClassNode.jar", x, y);
    cmn.iconNode.setColor("Blue");
    cmn.iconNode.setTextColor("Blue");
    cmn.iconNode.setBackground("CornflowerBlue");
    cmn.header.setColor("DarkSlateBlue");
    cmn.header.setBackground("CornflowerBlue");

    // =========================================
    // Отношения на диаграммах UML (ребра графа).
    // =========================================
    //
    // Новая колонка.
    // ==============
    x += 200;
    y = Y;

    double L = 80;

    Edge edge;

    // Отношение наследования.
    ClassifierNode subclass = new ClassNode(diagram, "Dog", x, y + L);
    ClassifierNode superclass = new ClassNode(diagram, "Animal", x, y);
    new InheritanceEdge(diagram, subclass, superclass);
    y += 150;

    // Отношение реализации.
    ClassNode implementor = new ClassNode(diagram, "Fish", x, y + L);
    InterfaceNode implemented = new InterfaceNode(diagram, "Swim", x, y);
    implemented.header.setColor("Purple");
    implemented.header.setBackground("Violet");
    implemented.iconNode.setColor("Purple");
    implemented.iconNode.setBackground("Violet");
    new ImplementationEdge(diagram, implementor, implemented);

    //
    // Новая колонка.
    // ==============
    x += 100;
    y = Y;

    // Отношение ассоциации.
    ClassNode owner = new ClassNode(diagram, "Tree", x, y + L);
    ClassNode owned = new ClassNode(diagram, "Leaf", x, y);
    new AssociationEdge(diagram, owner, owned);
    y += 150;

    // Отношение зависимости.
    Shape client = new PackageNode(diagram, "Chess", x, y);
    Shape supplier = new PackageNode(diagram, "Game", x, y + L);
    edge = new DependencyEdge(diagram, client, supplier);
    edge.setSourceAnchor(new AnchorBottom());
    edge.setTargetAnchor(new AnchorTop());

    diagram.save(diagramDir, fileName);
  }
}
