package samples;

import uml.diagram.svg.SVGDiagram;
import uml.diagram.svg.edges.AssociationEdge;
import uml.diagram.svg.nodes.ClassNode;

import java.io.File;

public class RadialLayout {
  private static final double CX = 200;
  private static final double CY = 200;

  public static void showRadialLayout(String diagramDir, String fileName) {
    new File(diagramDir).mkdirs();

    SVGDiagram diagram = new SVGDiagram("Packages nesting with radial layout");

    double xA = CX;
    double yA = CY;
    ClassNode center;
    center = new ClassNode(diagram, "org", xA, yA);
    center.setColor("yellow");
    center.setBackground("yellow");

    ClassNode level1;
    double offset1 = 50;
    double[] xB = {xA + offset1, xA - offset1};
    double[] yB = {yA + offset1, yA - offset1};
    for (double v : xB) {
      for (double value : yB) {
        level1 = new ClassNode(diagram, "java", v, value);
        level1.setColor("yellow");
        level1.setBackground("maroon");
        new AssociationEdge(diagram, center, level1);
      }
    }

    double offset2 = 52;
    double r2 = 180;
    double[] xC = {-10, -10 - offset2, -10 - 2 * offset2, -10 - 3 * offset2,
            10, 10 + offset2, 10 + 2 * offset2, 10 + 3 * offset2};
    ClassNode level2;
    for (double v : xC) {
      double yC1 = Math.sqrt(Math.pow(r2, 2) - Math.pow(v, 2));
      double yC2 = -yC1;
      level2 = new ClassNode(diagram, "swing", xA + v, yA + yC1);
      level2.setColor("yellow");
      level2.setBackground("yellow");

      level2 = new ClassNode(diagram, "swing", xA + v, yA + yC2);
      level2.setColor("yellow");
      level2.setBackground("yellow");

    }

    diagram.save(diagramDir, fileName);

  }

  public static void main(String[] args) {
    RadialLayout.showRadialLayout("./test", "1.svg");
  }
}