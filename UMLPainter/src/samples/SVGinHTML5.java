package samples;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;

import uml.report.ReportUtil;

public class SVGinHTML5 {

	public static void generate(String dir, String fileName) {
		new File(dir).mkdirs();
		
		Document document = DocumentFactory.getInstance().createDocument();
		document.addDocType("html", null, null);
		
		Element html = document.addElement("html");
		Element body = html.addElement("body");

		Element h1 = body.addElement("h1");
		h1.addText("SVG in HTML5");

		Element svg = body.addElement("svg");
		svg.addAttribute("width", "100");
		svg.addAttribute("height", "100");

		Element circle = svg.addElement("circle");
		circle.addAttribute("cx", "50");
		circle.addAttribute("cy", "50");
		circle.addAttribute("r", "40");
		circle.addAttribute("stroke", "green");
		circle.addAttribute("stroke-width", "4");
		circle.addAttribute("fill", "yellow");
		
		ReportUtil.save(document, dir, fileName);
	}
}
